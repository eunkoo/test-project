#!/usr/bin/env bash
export PATH=$(go env GOPATH)/bin:$PATH
swag init -g ./cmd/main/main.go -o ./docs
go build cmd/main/main.go
go run cmd/main/main.go