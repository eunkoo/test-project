#!/usr/bin/env bash

export DB_HOST="prod-rds.c3zn8lnmsclj.ap-northeast-2.rds.amazonaws.com"
export DB_ACCOUNT="web-server"
export DB_PASSWORD='!linstock123#'
export SCHEMA_NAME="linstock_production"
export TABLES="\
linstock_stocks_classification,\
linstock_stocks_dowjones,\
linstock_stocks_etfbigpricerank,\
linstock_stocks_etfdetail,\
linstock_stocks_etfloss1mrank,\
linstock_stocks_etfloss1wrank,\
linstock_stocks_etfloss1yrank,\
linstock_stocks_etfloss3mrank,\
linstock_stocks_etfloss5yrank,\
linstock_stocks_etfloss6mrank,\
linstock_stocks_etfmarketcaprank,\
linstock_stocks_etfnewrank,\
linstock_stocks_etfprofit1mrank,\
linstock_stocks_etfprofit1wrank,\
linstock_stocks_etfprofit1yrank,\
linstock_stocks_etfprofit3mrank,\
linstock_stocks_etfprofit5yrank,\
linstock_stocks_etfprofit6mrank,\
linstock_stocks_etfsmallpricerank,\
linstock_stocks_etfvolume10drank,\
linstock_stocks_etfvolume1drank,\
linstock_stocks_etfvolume1mrank,\
linstock_stocks_nasdaq,\
linstock_stocks_pubetfcategory,\
linstock_stocks_pubetfcategory_symbol,\
linstock_stocks_sp500,\
linstock_common_usstockholiday,\
linstock_common_usstocktime,\
linstock_common_usstockearlyclose,\
linstock_stocks_symbol,\
linstock_stocks_symbolprice,\
linstock_stocks_apiclientkey,\
linstock_stocks_symboldailyprice,\
linstock_stocks_symbolversion,\
linstock_stocks_top100popularsymbols,\
linstock_common_kiwoomcontentfeed,\
linstock_stocks_historicaldividendus,\
linstock_stocks_etfholder,\
linstock_stocks_etfcountryweight,\
linstock_stocks_etfsectorweight,
"

export DB_DSN="mysql://${DB_ACCOUNT}:${DB_PASSWORD}@tcp(${DB_HOST})/${SCHEMA_NAME}"
go run ariga.io/entimport/cmd/entimport -dsn $DB_DSN -tables $TABLES
