#!/usr/bin/env bash

go install entgo.io/ent/cmd/ent@latest
go run -mod=mod entgo.io/ent/cmd/ent init
go run -mod=mod ariga.io/entimport/cmd/entimport -h