// Code generated by ent, DO NOT EDIT.

package ent

import (
	"ezar-b2b-api/ent/linstockstockssp500"
	"fmt"
	"strings"
	"time"

	"entgo.io/ent/dialect/sql"
)

// LinstockStocksSp500 is the model entity for the LinstockStocksSp500 schema.
type LinstockStocksSp500 struct {
	config `json:"-"`
	// ID of the ent.
	ID string `json:"id,omitempty"`
	// Name holds the value of the "name" field.
	Name string `json:"name,omitempty"`
	// Sector holds the value of the "sector" field.
	Sector string `json:"sector,omitempty"`
	// DescriptionKo holds the value of the "description_ko" field.
	DescriptionKo string `json:"description_ko,omitempty"`
	// Avg10Vol holds the value of the "avg10_vol" field.
	Avg10Vol int32 `json:"avg10_vol,omitempty"`
	// Avg30Vol holds the value of the "avg30_vol" field.
	Avg30Vol int32 `json:"avg30_vol,omitempty"`
	// Beta holds the value of the "beta" field.
	Beta float64 `json:"beta,omitempty"`
	// LatestPrice holds the value of the "latest_price" field.
	LatestPrice float64 `json:"latest_price,omitempty"`
	// LatestPriceKo holds the value of the "latest_price_ko" field.
	LatestPriceKo float64 `json:"latest_price_ko,omitempty"`
	// MarketCap holds the value of the "market_cap" field.
	MarketCap float64 `json:"market_cap,omitempty"`
	// MarketCapKo holds the value of the "market_cap_ko" field.
	MarketCapKo float64 `json:"market_cap_ko,omitempty"`
	// ChangePercent holds the value of the "change_percent" field.
	ChangePercent float64 `json:"change_percent,omitempty"`
	// Day30ChangePercent holds the value of the "day30_change_percent" field.
	Day30ChangePercent float64 `json:"day30_change_percent,omitempty"`
	// Month3ChangePercent holds the value of the "month3_change_percent" field.
	Month3ChangePercent float64 `json:"month3_change_percent,omitempty"`
	// Month6ChangePercent holds the value of the "month6_change_percent" field.
	Month6ChangePercent float64 `json:"month6_change_percent,omitempty"`
	// Year1ChangePercent holds the value of the "year1_change_percent" field.
	Year1ChangePercent float64 `json:"year1_change_percent,omitempty"`
	// Year2ChangePercent holds the value of the "year2_change_percent" field.
	Year2ChangePercent float64 `json:"year2_change_percent,omitempty"`
	// Year5ChangePercent holds the value of the "year5_change_percent" field.
	Year5ChangePercent float64 `json:"year5_change_percent,omitempty"`
	// YtdChangePercent holds the value of the "ytd_change_percent" field.
	YtdChangePercent float64 `json:"ytd_change_percent,omitempty"`
	// PeRatio holds the value of the "pe_ratio" field.
	PeRatio float64 `json:"pe_ratio,omitempty"`
	// DividendYield holds the value of the "dividend_yield" field.
	DividendYield float64 `json:"dividend_yield,omitempty"`
	// TotalCash holds the value of the "total_cash" field.
	TotalCash int `json:"total_cash,omitempty"`
	// CurrentDebt holds the value of the "current_debt" field.
	CurrentDebt int `json:"current_debt,omitempty"`
	// Revenue holds the value of the "revenue" field.
	Revenue int `json:"revenue,omitempty"`
	// GrossProfit holds the value of the "gross_profit" field.
	GrossProfit int `json:"gross_profit,omitempty"`
	// TotalRevenue holds the value of the "total_revenue" field.
	TotalRevenue int `json:"total_revenue,omitempty"`
	// RevenuePerShare holds the value of the "revenue_per_share" field.
	RevenuePerShare float64 `json:"revenue_per_share,omitempty"`
	// ProfitMargin holds the value of the "profit_margin" field.
	ProfitMargin float64 `json:"profit_margin,omitempty"`
	// PriceToSales holds the value of the "price_to_sales" field.
	PriceToSales float64 `json:"price_to_sales,omitempty"`
	// PriceToBook holds the value of the "price_to_book" field.
	PriceToBook float64 `json:"price_to_book,omitempty"`
	// ForwardPeRatio holds the value of the "forward_pe_ratio" field.
	ForwardPeRatio float64 `json:"forward_pe_ratio,omitempty"`
	// CreatedAt holds the value of the "created_at" field.
	CreatedAt time.Time `json:"created_at,omitempty"`
	// UpdatedAt holds the value of the "updated_at" field.
	UpdatedAt time.Time `json:"updated_at,omitempty"`
	// DeletedAt holds the value of the "deleted_at" field.
	DeletedAt time.Time `json:"deleted_at,omitempty"`
	// GrossProfitKo holds the value of the "gross_profit_ko" field.
	GrossProfitKo int `json:"gross_profit_ko,omitempty"`
	// NameKo holds the value of the "name_ko" field.
	NameKo string `json:"name_ko,omitempty"`
	// RevenueKo holds the value of the "revenue_ko" field.
	RevenueKo int `json:"revenue_ko,omitempty"`
	// RevenuePerShareKo holds the value of the "revenue_per_share_ko" field.
	RevenuePerShareKo float64 `json:"revenue_per_share_ko,omitempty"`
	// TotalCashKo holds the value of the "total_cash_ko" field.
	TotalCashKo int `json:"total_cash_ko,omitempty"`
}

// scanValues returns the types for scanning values from sql.Rows.
func (*LinstockStocksSp500) scanValues(columns []string) ([]interface{}, error) {
	values := make([]interface{}, len(columns))
	for i := range columns {
		switch columns[i] {
		case linstockstockssp500.FieldBeta, linstockstockssp500.FieldLatestPrice, linstockstockssp500.FieldLatestPriceKo, linstockstockssp500.FieldMarketCap, linstockstockssp500.FieldMarketCapKo, linstockstockssp500.FieldChangePercent, linstockstockssp500.FieldDay30ChangePercent, linstockstockssp500.FieldMonth3ChangePercent, linstockstockssp500.FieldMonth6ChangePercent, linstockstockssp500.FieldYear1ChangePercent, linstockstockssp500.FieldYear2ChangePercent, linstockstockssp500.FieldYear5ChangePercent, linstockstockssp500.FieldYtdChangePercent, linstockstockssp500.FieldPeRatio, linstockstockssp500.FieldDividendYield, linstockstockssp500.FieldRevenuePerShare, linstockstockssp500.FieldProfitMargin, linstockstockssp500.FieldPriceToSales, linstockstockssp500.FieldPriceToBook, linstockstockssp500.FieldForwardPeRatio, linstockstockssp500.FieldRevenuePerShareKo:
			values[i] = new(sql.NullFloat64)
		case linstockstockssp500.FieldAvg10Vol, linstockstockssp500.FieldAvg30Vol, linstockstockssp500.FieldTotalCash, linstockstockssp500.FieldCurrentDebt, linstockstockssp500.FieldRevenue, linstockstockssp500.FieldGrossProfit, linstockstockssp500.FieldTotalRevenue, linstockstockssp500.FieldGrossProfitKo, linstockstockssp500.FieldRevenueKo, linstockstockssp500.FieldTotalCashKo:
			values[i] = new(sql.NullInt64)
		case linstockstockssp500.FieldID, linstockstockssp500.FieldName, linstockstockssp500.FieldSector, linstockstockssp500.FieldDescriptionKo, linstockstockssp500.FieldNameKo:
			values[i] = new(sql.NullString)
		case linstockstockssp500.FieldCreatedAt, linstockstockssp500.FieldUpdatedAt, linstockstockssp500.FieldDeletedAt:
			values[i] = new(sql.NullTime)
		default:
			return nil, fmt.Errorf("unexpected column %q for type LinstockStocksSp500", columns[i])
		}
	}
	return values, nil
}

// assignValues assigns the values that were returned from sql.Rows (after scanning)
// to the LinstockStocksSp500 fields.
func (lss *LinstockStocksSp500) assignValues(columns []string, values []interface{}) error {
	if m, n := len(values), len(columns); m < n {
		return fmt.Errorf("mismatch number of scan values: %d != %d", m, n)
	}
	for i := range columns {
		switch columns[i] {
		case linstockstockssp500.FieldID:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field id", values[i])
			} else if value.Valid {
				lss.ID = value.String
			}
		case linstockstockssp500.FieldName:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field name", values[i])
			} else if value.Valid {
				lss.Name = value.String
			}
		case linstockstockssp500.FieldSector:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field sector", values[i])
			} else if value.Valid {
				lss.Sector = value.String
			}
		case linstockstockssp500.FieldDescriptionKo:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field description_ko", values[i])
			} else if value.Valid {
				lss.DescriptionKo = value.String
			}
		case linstockstockssp500.FieldAvg10Vol:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field avg10_vol", values[i])
			} else if value.Valid {
				lss.Avg10Vol = int32(value.Int64)
			}
		case linstockstockssp500.FieldAvg30Vol:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field avg30_vol", values[i])
			} else if value.Valid {
				lss.Avg30Vol = int32(value.Int64)
			}
		case linstockstockssp500.FieldBeta:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field beta", values[i])
			} else if value.Valid {
				lss.Beta = value.Float64
			}
		case linstockstockssp500.FieldLatestPrice:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field latest_price", values[i])
			} else if value.Valid {
				lss.LatestPrice = value.Float64
			}
		case linstockstockssp500.FieldLatestPriceKo:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field latest_price_ko", values[i])
			} else if value.Valid {
				lss.LatestPriceKo = value.Float64
			}
		case linstockstockssp500.FieldMarketCap:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field market_cap", values[i])
			} else if value.Valid {
				lss.MarketCap = value.Float64
			}
		case linstockstockssp500.FieldMarketCapKo:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field market_cap_ko", values[i])
			} else if value.Valid {
				lss.MarketCapKo = value.Float64
			}
		case linstockstockssp500.FieldChangePercent:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field change_percent", values[i])
			} else if value.Valid {
				lss.ChangePercent = value.Float64
			}
		case linstockstockssp500.FieldDay30ChangePercent:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field day30_change_percent", values[i])
			} else if value.Valid {
				lss.Day30ChangePercent = value.Float64
			}
		case linstockstockssp500.FieldMonth3ChangePercent:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field month3_change_percent", values[i])
			} else if value.Valid {
				lss.Month3ChangePercent = value.Float64
			}
		case linstockstockssp500.FieldMonth6ChangePercent:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field month6_change_percent", values[i])
			} else if value.Valid {
				lss.Month6ChangePercent = value.Float64
			}
		case linstockstockssp500.FieldYear1ChangePercent:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field year1_change_percent", values[i])
			} else if value.Valid {
				lss.Year1ChangePercent = value.Float64
			}
		case linstockstockssp500.FieldYear2ChangePercent:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field year2_change_percent", values[i])
			} else if value.Valid {
				lss.Year2ChangePercent = value.Float64
			}
		case linstockstockssp500.FieldYear5ChangePercent:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field year5_change_percent", values[i])
			} else if value.Valid {
				lss.Year5ChangePercent = value.Float64
			}
		case linstockstockssp500.FieldYtdChangePercent:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field ytd_change_percent", values[i])
			} else if value.Valid {
				lss.YtdChangePercent = value.Float64
			}
		case linstockstockssp500.FieldPeRatio:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field pe_ratio", values[i])
			} else if value.Valid {
				lss.PeRatio = value.Float64
			}
		case linstockstockssp500.FieldDividendYield:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field dividend_yield", values[i])
			} else if value.Valid {
				lss.DividendYield = value.Float64
			}
		case linstockstockssp500.FieldTotalCash:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field total_cash", values[i])
			} else if value.Valid {
				lss.TotalCash = int(value.Int64)
			}
		case linstockstockssp500.FieldCurrentDebt:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field current_debt", values[i])
			} else if value.Valid {
				lss.CurrentDebt = int(value.Int64)
			}
		case linstockstockssp500.FieldRevenue:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field revenue", values[i])
			} else if value.Valid {
				lss.Revenue = int(value.Int64)
			}
		case linstockstockssp500.FieldGrossProfit:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field gross_profit", values[i])
			} else if value.Valid {
				lss.GrossProfit = int(value.Int64)
			}
		case linstockstockssp500.FieldTotalRevenue:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field total_revenue", values[i])
			} else if value.Valid {
				lss.TotalRevenue = int(value.Int64)
			}
		case linstockstockssp500.FieldRevenuePerShare:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field revenue_per_share", values[i])
			} else if value.Valid {
				lss.RevenuePerShare = value.Float64
			}
		case linstockstockssp500.FieldProfitMargin:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field profit_margin", values[i])
			} else if value.Valid {
				lss.ProfitMargin = value.Float64
			}
		case linstockstockssp500.FieldPriceToSales:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field price_to_sales", values[i])
			} else if value.Valid {
				lss.PriceToSales = value.Float64
			}
		case linstockstockssp500.FieldPriceToBook:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field price_to_book", values[i])
			} else if value.Valid {
				lss.PriceToBook = value.Float64
			}
		case linstockstockssp500.FieldForwardPeRatio:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field forward_pe_ratio", values[i])
			} else if value.Valid {
				lss.ForwardPeRatio = value.Float64
			}
		case linstockstockssp500.FieldCreatedAt:
			if value, ok := values[i].(*sql.NullTime); !ok {
				return fmt.Errorf("unexpected type %T for field created_at", values[i])
			} else if value.Valid {
				lss.CreatedAt = value.Time
			}
		case linstockstockssp500.FieldUpdatedAt:
			if value, ok := values[i].(*sql.NullTime); !ok {
				return fmt.Errorf("unexpected type %T for field updated_at", values[i])
			} else if value.Valid {
				lss.UpdatedAt = value.Time
			}
		case linstockstockssp500.FieldDeletedAt:
			if value, ok := values[i].(*sql.NullTime); !ok {
				return fmt.Errorf("unexpected type %T for field deleted_at", values[i])
			} else if value.Valid {
				lss.DeletedAt = value.Time
			}
		case linstockstockssp500.FieldGrossProfitKo:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field gross_profit_ko", values[i])
			} else if value.Valid {
				lss.GrossProfitKo = int(value.Int64)
			}
		case linstockstockssp500.FieldNameKo:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field name_ko", values[i])
			} else if value.Valid {
				lss.NameKo = value.String
			}
		case linstockstockssp500.FieldRevenueKo:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field revenue_ko", values[i])
			} else if value.Valid {
				lss.RevenueKo = int(value.Int64)
			}
		case linstockstockssp500.FieldRevenuePerShareKo:
			if value, ok := values[i].(*sql.NullFloat64); !ok {
				return fmt.Errorf("unexpected type %T for field revenue_per_share_ko", values[i])
			} else if value.Valid {
				lss.RevenuePerShareKo = value.Float64
			}
		case linstockstockssp500.FieldTotalCashKo:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field total_cash_ko", values[i])
			} else if value.Valid {
				lss.TotalCashKo = int(value.Int64)
			}
		}
	}
	return nil
}

// Update returns a builder for updating this LinstockStocksSp500.
// Note that you need to call LinstockStocksSp500.Unwrap() before calling this method if this LinstockStocksSp500
// was returned from a transaction, and the transaction was committed or rolled back.
func (lss *LinstockStocksSp500) Update() *LinstockStocksSp500UpdateOne {
	return (&LinstockStocksSp500Client{config: lss.config}).UpdateOne(lss)
}

// Unwrap unwraps the LinstockStocksSp500 entity that was returned from a transaction after it was closed,
// so that all future queries will be executed through the driver which created the transaction.
func (lss *LinstockStocksSp500) Unwrap() *LinstockStocksSp500 {
	_tx, ok := lss.config.driver.(*txDriver)
	if !ok {
		panic("ent: LinstockStocksSp500 is not a transactional entity")
	}
	lss.config.driver = _tx.drv
	return lss
}

// String implements the fmt.Stringer.
func (lss *LinstockStocksSp500) String() string {
	var builder strings.Builder
	builder.WriteString("LinstockStocksSp500(")
	builder.WriteString(fmt.Sprintf("id=%v, ", lss.ID))
	builder.WriteString("name=")
	builder.WriteString(lss.Name)
	builder.WriteString(", ")
	builder.WriteString("sector=")
	builder.WriteString(lss.Sector)
	builder.WriteString(", ")
	builder.WriteString("description_ko=")
	builder.WriteString(lss.DescriptionKo)
	builder.WriteString(", ")
	builder.WriteString("avg10_vol=")
	builder.WriteString(fmt.Sprintf("%v", lss.Avg10Vol))
	builder.WriteString(", ")
	builder.WriteString("avg30_vol=")
	builder.WriteString(fmt.Sprintf("%v", lss.Avg30Vol))
	builder.WriteString(", ")
	builder.WriteString("beta=")
	builder.WriteString(fmt.Sprintf("%v", lss.Beta))
	builder.WriteString(", ")
	builder.WriteString("latest_price=")
	builder.WriteString(fmt.Sprintf("%v", lss.LatestPrice))
	builder.WriteString(", ")
	builder.WriteString("latest_price_ko=")
	builder.WriteString(fmt.Sprintf("%v", lss.LatestPriceKo))
	builder.WriteString(", ")
	builder.WriteString("market_cap=")
	builder.WriteString(fmt.Sprintf("%v", lss.MarketCap))
	builder.WriteString(", ")
	builder.WriteString("market_cap_ko=")
	builder.WriteString(fmt.Sprintf("%v", lss.MarketCapKo))
	builder.WriteString(", ")
	builder.WriteString("change_percent=")
	builder.WriteString(fmt.Sprintf("%v", lss.ChangePercent))
	builder.WriteString(", ")
	builder.WriteString("day30_change_percent=")
	builder.WriteString(fmt.Sprintf("%v", lss.Day30ChangePercent))
	builder.WriteString(", ")
	builder.WriteString("month3_change_percent=")
	builder.WriteString(fmt.Sprintf("%v", lss.Month3ChangePercent))
	builder.WriteString(", ")
	builder.WriteString("month6_change_percent=")
	builder.WriteString(fmt.Sprintf("%v", lss.Month6ChangePercent))
	builder.WriteString(", ")
	builder.WriteString("year1_change_percent=")
	builder.WriteString(fmt.Sprintf("%v", lss.Year1ChangePercent))
	builder.WriteString(", ")
	builder.WriteString("year2_change_percent=")
	builder.WriteString(fmt.Sprintf("%v", lss.Year2ChangePercent))
	builder.WriteString(", ")
	builder.WriteString("year5_change_percent=")
	builder.WriteString(fmt.Sprintf("%v", lss.Year5ChangePercent))
	builder.WriteString(", ")
	builder.WriteString("ytd_change_percent=")
	builder.WriteString(fmt.Sprintf("%v", lss.YtdChangePercent))
	builder.WriteString(", ")
	builder.WriteString("pe_ratio=")
	builder.WriteString(fmt.Sprintf("%v", lss.PeRatio))
	builder.WriteString(", ")
	builder.WriteString("dividend_yield=")
	builder.WriteString(fmt.Sprintf("%v", lss.DividendYield))
	builder.WriteString(", ")
	builder.WriteString("total_cash=")
	builder.WriteString(fmt.Sprintf("%v", lss.TotalCash))
	builder.WriteString(", ")
	builder.WriteString("current_debt=")
	builder.WriteString(fmt.Sprintf("%v", lss.CurrentDebt))
	builder.WriteString(", ")
	builder.WriteString("revenue=")
	builder.WriteString(fmt.Sprintf("%v", lss.Revenue))
	builder.WriteString(", ")
	builder.WriteString("gross_profit=")
	builder.WriteString(fmt.Sprintf("%v", lss.GrossProfit))
	builder.WriteString(", ")
	builder.WriteString("total_revenue=")
	builder.WriteString(fmt.Sprintf("%v", lss.TotalRevenue))
	builder.WriteString(", ")
	builder.WriteString("revenue_per_share=")
	builder.WriteString(fmt.Sprintf("%v", lss.RevenuePerShare))
	builder.WriteString(", ")
	builder.WriteString("profit_margin=")
	builder.WriteString(fmt.Sprintf("%v", lss.ProfitMargin))
	builder.WriteString(", ")
	builder.WriteString("price_to_sales=")
	builder.WriteString(fmt.Sprintf("%v", lss.PriceToSales))
	builder.WriteString(", ")
	builder.WriteString("price_to_book=")
	builder.WriteString(fmt.Sprintf("%v", lss.PriceToBook))
	builder.WriteString(", ")
	builder.WriteString("forward_pe_ratio=")
	builder.WriteString(fmt.Sprintf("%v", lss.ForwardPeRatio))
	builder.WriteString(", ")
	builder.WriteString("created_at=")
	builder.WriteString(lss.CreatedAt.Format(time.ANSIC))
	builder.WriteString(", ")
	builder.WriteString("updated_at=")
	builder.WriteString(lss.UpdatedAt.Format(time.ANSIC))
	builder.WriteString(", ")
	builder.WriteString("deleted_at=")
	builder.WriteString(lss.DeletedAt.Format(time.ANSIC))
	builder.WriteString(", ")
	builder.WriteString("gross_profit_ko=")
	builder.WriteString(fmt.Sprintf("%v", lss.GrossProfitKo))
	builder.WriteString(", ")
	builder.WriteString("name_ko=")
	builder.WriteString(lss.NameKo)
	builder.WriteString(", ")
	builder.WriteString("revenue_ko=")
	builder.WriteString(fmt.Sprintf("%v", lss.RevenueKo))
	builder.WriteString(", ")
	builder.WriteString("revenue_per_share_ko=")
	builder.WriteString(fmt.Sprintf("%v", lss.RevenuePerShareKo))
	builder.WriteString(", ")
	builder.WriteString("total_cash_ko=")
	builder.WriteString(fmt.Sprintf("%v", lss.TotalCashKo))
	builder.WriteByte(')')
	return builder.String()
}

// LinstockStocksSp500s is a parsable slice of LinstockStocksSp500.
type LinstockStocksSp500s []*LinstockStocksSp500

func (lss LinstockStocksSp500s) config(cfg config) {
	for _i := range lss {
		lss[_i].config = cfg
	}
}
