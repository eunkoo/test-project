// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"ezar-b2b-api/ent/linstockstocksnasdaq"
	"ezar-b2b-api/ent/predicate"
	"fmt"
	"math"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// LinstockStocksNasdaqQuery is the builder for querying LinstockStocksNasdaq entities.
type LinstockStocksNasdaqQuery struct {
	config
	limit      *int
	offset     *int
	unique     *bool
	order      []OrderFunc
	fields     []string
	predicates []predicate.LinstockStocksNasdaq
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Where adds a new predicate for the LinstockStocksNasdaqQuery builder.
func (lsnq *LinstockStocksNasdaqQuery) Where(ps ...predicate.LinstockStocksNasdaq) *LinstockStocksNasdaqQuery {
	lsnq.predicates = append(lsnq.predicates, ps...)
	return lsnq
}

// Limit adds a limit step to the query.
func (lsnq *LinstockStocksNasdaqQuery) Limit(limit int) *LinstockStocksNasdaqQuery {
	lsnq.limit = &limit
	return lsnq
}

// Offset adds an offset step to the query.
func (lsnq *LinstockStocksNasdaqQuery) Offset(offset int) *LinstockStocksNasdaqQuery {
	lsnq.offset = &offset
	return lsnq
}

// Unique configures the query builder to filter duplicate records on query.
// By default, unique is set to true, and can be disabled using this method.
func (lsnq *LinstockStocksNasdaqQuery) Unique(unique bool) *LinstockStocksNasdaqQuery {
	lsnq.unique = &unique
	return lsnq
}

// Order adds an order step to the query.
func (lsnq *LinstockStocksNasdaqQuery) Order(o ...OrderFunc) *LinstockStocksNasdaqQuery {
	lsnq.order = append(lsnq.order, o...)
	return lsnq
}

// First returns the first LinstockStocksNasdaq entity from the query.
// Returns a *NotFoundError when no LinstockStocksNasdaq was found.
func (lsnq *LinstockStocksNasdaqQuery) First(ctx context.Context) (*LinstockStocksNasdaq, error) {
	nodes, err := lsnq.Limit(1).All(ctx)
	if err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nil, &NotFoundError{linstockstocksnasdaq.Label}
	}
	return nodes[0], nil
}

// FirstX is like First, but panics if an error occurs.
func (lsnq *LinstockStocksNasdaqQuery) FirstX(ctx context.Context) *LinstockStocksNasdaq {
	node, err := lsnq.First(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return node
}

// FirstID returns the first LinstockStocksNasdaq ID from the query.
// Returns a *NotFoundError when no LinstockStocksNasdaq ID was found.
func (lsnq *LinstockStocksNasdaqQuery) FirstID(ctx context.Context) (id string, err error) {
	var ids []string
	if ids, err = lsnq.Limit(1).IDs(ctx); err != nil {
		return
	}
	if len(ids) == 0 {
		err = &NotFoundError{linstockstocksnasdaq.Label}
		return
	}
	return ids[0], nil
}

// FirstIDX is like FirstID, but panics if an error occurs.
func (lsnq *LinstockStocksNasdaqQuery) FirstIDX(ctx context.Context) string {
	id, err := lsnq.FirstID(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return id
}

// Only returns a single LinstockStocksNasdaq entity found by the query, ensuring it only returns one.
// Returns a *NotSingularError when more than one LinstockStocksNasdaq entity is found.
// Returns a *NotFoundError when no LinstockStocksNasdaq entities are found.
func (lsnq *LinstockStocksNasdaqQuery) Only(ctx context.Context) (*LinstockStocksNasdaq, error) {
	nodes, err := lsnq.Limit(2).All(ctx)
	if err != nil {
		return nil, err
	}
	switch len(nodes) {
	case 1:
		return nodes[0], nil
	case 0:
		return nil, &NotFoundError{linstockstocksnasdaq.Label}
	default:
		return nil, &NotSingularError{linstockstocksnasdaq.Label}
	}
}

// OnlyX is like Only, but panics if an error occurs.
func (lsnq *LinstockStocksNasdaqQuery) OnlyX(ctx context.Context) *LinstockStocksNasdaq {
	node, err := lsnq.Only(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// OnlyID is like Only, but returns the only LinstockStocksNasdaq ID in the query.
// Returns a *NotSingularError when more than one LinstockStocksNasdaq ID is found.
// Returns a *NotFoundError when no entities are found.
func (lsnq *LinstockStocksNasdaqQuery) OnlyID(ctx context.Context) (id string, err error) {
	var ids []string
	if ids, err = lsnq.Limit(2).IDs(ctx); err != nil {
		return
	}
	switch len(ids) {
	case 1:
		id = ids[0]
	case 0:
		err = &NotFoundError{linstockstocksnasdaq.Label}
	default:
		err = &NotSingularError{linstockstocksnasdaq.Label}
	}
	return
}

// OnlyIDX is like OnlyID, but panics if an error occurs.
func (lsnq *LinstockStocksNasdaqQuery) OnlyIDX(ctx context.Context) string {
	id, err := lsnq.OnlyID(ctx)
	if err != nil {
		panic(err)
	}
	return id
}

// All executes the query and returns a list of LinstockStocksNasdaqs.
func (lsnq *LinstockStocksNasdaqQuery) All(ctx context.Context) ([]*LinstockStocksNasdaq, error) {
	if err := lsnq.prepareQuery(ctx); err != nil {
		return nil, err
	}
	return lsnq.sqlAll(ctx)
}

// AllX is like All, but panics if an error occurs.
func (lsnq *LinstockStocksNasdaqQuery) AllX(ctx context.Context) []*LinstockStocksNasdaq {
	nodes, err := lsnq.All(ctx)
	if err != nil {
		panic(err)
	}
	return nodes
}

// IDs executes the query and returns a list of LinstockStocksNasdaq IDs.
func (lsnq *LinstockStocksNasdaqQuery) IDs(ctx context.Context) ([]string, error) {
	var ids []string
	if err := lsnq.Select(linstockstocksnasdaq.FieldID).Scan(ctx, &ids); err != nil {
		return nil, err
	}
	return ids, nil
}

// IDsX is like IDs, but panics if an error occurs.
func (lsnq *LinstockStocksNasdaqQuery) IDsX(ctx context.Context) []string {
	ids, err := lsnq.IDs(ctx)
	if err != nil {
		panic(err)
	}
	return ids
}

// Count returns the count of the given query.
func (lsnq *LinstockStocksNasdaqQuery) Count(ctx context.Context) (int, error) {
	if err := lsnq.prepareQuery(ctx); err != nil {
		return 0, err
	}
	return lsnq.sqlCount(ctx)
}

// CountX is like Count, but panics if an error occurs.
func (lsnq *LinstockStocksNasdaqQuery) CountX(ctx context.Context) int {
	count, err := lsnq.Count(ctx)
	if err != nil {
		panic(err)
	}
	return count
}

// Exist returns true if the query has elements in the graph.
func (lsnq *LinstockStocksNasdaqQuery) Exist(ctx context.Context) (bool, error) {
	if err := lsnq.prepareQuery(ctx); err != nil {
		return false, err
	}
	return lsnq.sqlExist(ctx)
}

// ExistX is like Exist, but panics if an error occurs.
func (lsnq *LinstockStocksNasdaqQuery) ExistX(ctx context.Context) bool {
	exist, err := lsnq.Exist(ctx)
	if err != nil {
		panic(err)
	}
	return exist
}

// Clone returns a duplicate of the LinstockStocksNasdaqQuery builder, including all associated steps. It can be
// used to prepare common query builders and use them differently after the clone is made.
func (lsnq *LinstockStocksNasdaqQuery) Clone() *LinstockStocksNasdaqQuery {
	if lsnq == nil {
		return nil
	}
	return &LinstockStocksNasdaqQuery{
		config:     lsnq.config,
		limit:      lsnq.limit,
		offset:     lsnq.offset,
		order:      append([]OrderFunc{}, lsnq.order...),
		predicates: append([]predicate.LinstockStocksNasdaq{}, lsnq.predicates...),
		// clone intermediate query.
		sql:    lsnq.sql.Clone(),
		path:   lsnq.path,
		unique: lsnq.unique,
	}
}

// GroupBy is used to group vertices by one or more fields/columns.
// It is often used with aggregate functions, like: count, max, mean, min, sum.
//
// Example:
//
//	var v []struct {
//		Name string `json:"name,omitempty"`
//		Count int `json:"count,omitempty"`
//	}
//
//	client.LinstockStocksNasdaq.Query().
//		GroupBy(linstockstocksnasdaq.FieldName).
//		Aggregate(ent.Count()).
//		Scan(ctx, &v)
func (lsnq *LinstockStocksNasdaqQuery) GroupBy(field string, fields ...string) *LinstockStocksNasdaqGroupBy {
	grbuild := &LinstockStocksNasdaqGroupBy{config: lsnq.config}
	grbuild.fields = append([]string{field}, fields...)
	grbuild.path = func(ctx context.Context) (prev *sql.Selector, err error) {
		if err := lsnq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		return lsnq.sqlQuery(ctx), nil
	}
	grbuild.label = linstockstocksnasdaq.Label
	grbuild.flds, grbuild.scan = &grbuild.fields, grbuild.Scan
	return grbuild
}

// Select allows the selection one or more fields/columns for the given query,
// instead of selecting all fields in the entity.
//
// Example:
//
//	var v []struct {
//		Name string `json:"name,omitempty"`
//	}
//
//	client.LinstockStocksNasdaq.Query().
//		Select(linstockstocksnasdaq.FieldName).
//		Scan(ctx, &v)
func (lsnq *LinstockStocksNasdaqQuery) Select(fields ...string) *LinstockStocksNasdaqSelect {
	lsnq.fields = append(lsnq.fields, fields...)
	selbuild := &LinstockStocksNasdaqSelect{LinstockStocksNasdaqQuery: lsnq}
	selbuild.label = linstockstocksnasdaq.Label
	selbuild.flds, selbuild.scan = &lsnq.fields, selbuild.Scan
	return selbuild
}

func (lsnq *LinstockStocksNasdaqQuery) prepareQuery(ctx context.Context) error {
	for _, f := range lsnq.fields {
		if !linstockstocksnasdaq.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
		}
	}
	if lsnq.path != nil {
		prev, err := lsnq.path(ctx)
		if err != nil {
			return err
		}
		lsnq.sql = prev
	}
	return nil
}

func (lsnq *LinstockStocksNasdaqQuery) sqlAll(ctx context.Context, hooks ...queryHook) ([]*LinstockStocksNasdaq, error) {
	var (
		nodes = []*LinstockStocksNasdaq{}
		_spec = lsnq.querySpec()
	)
	_spec.ScanValues = func(columns []string) ([]interface{}, error) {
		return (*LinstockStocksNasdaq).scanValues(nil, columns)
	}
	_spec.Assign = func(columns []string, values []interface{}) error {
		node := &LinstockStocksNasdaq{config: lsnq.config}
		nodes = append(nodes, node)
		return node.assignValues(columns, values)
	}
	for i := range hooks {
		hooks[i](ctx, _spec)
	}
	if err := sqlgraph.QueryNodes(ctx, lsnq.driver, _spec); err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nodes, nil
	}
	return nodes, nil
}

func (lsnq *LinstockStocksNasdaqQuery) sqlCount(ctx context.Context) (int, error) {
	_spec := lsnq.querySpec()
	_spec.Node.Columns = lsnq.fields
	if len(lsnq.fields) > 0 {
		_spec.Unique = lsnq.unique != nil && *lsnq.unique
	}
	return sqlgraph.CountNodes(ctx, lsnq.driver, _spec)
}

func (lsnq *LinstockStocksNasdaqQuery) sqlExist(ctx context.Context) (bool, error) {
	n, err := lsnq.sqlCount(ctx)
	if err != nil {
		return false, fmt.Errorf("ent: check existence: %w", err)
	}
	return n > 0, nil
}

func (lsnq *LinstockStocksNasdaqQuery) querySpec() *sqlgraph.QuerySpec {
	_spec := &sqlgraph.QuerySpec{
		Node: &sqlgraph.NodeSpec{
			Table:   linstockstocksnasdaq.Table,
			Columns: linstockstocksnasdaq.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeString,
				Column: linstockstocksnasdaq.FieldID,
			},
		},
		From:   lsnq.sql,
		Unique: true,
	}
	if unique := lsnq.unique; unique != nil {
		_spec.Unique = *unique
	}
	if fields := lsnq.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, linstockstocksnasdaq.FieldID)
		for i := range fields {
			if fields[i] != linstockstocksnasdaq.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, fields[i])
			}
		}
	}
	if ps := lsnq.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if limit := lsnq.limit; limit != nil {
		_spec.Limit = *limit
	}
	if offset := lsnq.offset; offset != nil {
		_spec.Offset = *offset
	}
	if ps := lsnq.order; len(ps) > 0 {
		_spec.Order = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	return _spec
}

func (lsnq *LinstockStocksNasdaqQuery) sqlQuery(ctx context.Context) *sql.Selector {
	builder := sql.Dialect(lsnq.driver.Dialect())
	t1 := builder.Table(linstockstocksnasdaq.Table)
	columns := lsnq.fields
	if len(columns) == 0 {
		columns = linstockstocksnasdaq.Columns
	}
	selector := builder.Select(t1.Columns(columns...)...).From(t1)
	if lsnq.sql != nil {
		selector = lsnq.sql
		selector.Select(selector.Columns(columns...)...)
	}
	if lsnq.unique != nil && *lsnq.unique {
		selector.Distinct()
	}
	for _, p := range lsnq.predicates {
		p(selector)
	}
	for _, p := range lsnq.order {
		p(selector)
	}
	if offset := lsnq.offset; offset != nil {
		// limit is mandatory for offset clause. We start
		// with default value, and override it below if needed.
		selector.Offset(*offset).Limit(math.MaxInt32)
	}
	if limit := lsnq.limit; limit != nil {
		selector.Limit(*limit)
	}
	return selector
}

// LinstockStocksNasdaqGroupBy is the group-by builder for LinstockStocksNasdaq entities.
type LinstockStocksNasdaqGroupBy struct {
	config
	selector
	fields []string
	fns    []AggregateFunc
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Aggregate adds the given aggregation functions to the group-by query.
func (lsngb *LinstockStocksNasdaqGroupBy) Aggregate(fns ...AggregateFunc) *LinstockStocksNasdaqGroupBy {
	lsngb.fns = append(lsngb.fns, fns...)
	return lsngb
}

// Scan applies the group-by query and scans the result into the given value.
func (lsngb *LinstockStocksNasdaqGroupBy) Scan(ctx context.Context, v interface{}) error {
	query, err := lsngb.path(ctx)
	if err != nil {
		return err
	}
	lsngb.sql = query
	return lsngb.sqlScan(ctx, v)
}

func (lsngb *LinstockStocksNasdaqGroupBy) sqlScan(ctx context.Context, v interface{}) error {
	for _, f := range lsngb.fields {
		if !linstockstocksnasdaq.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("invalid field %q for group-by", f)}
		}
	}
	selector := lsngb.sqlQuery()
	if err := selector.Err(); err != nil {
		return err
	}
	rows := &sql.Rows{}
	query, args := selector.Query()
	if err := lsngb.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

func (lsngb *LinstockStocksNasdaqGroupBy) sqlQuery() *sql.Selector {
	selector := lsngb.sql.Select()
	aggregation := make([]string, 0, len(lsngb.fns))
	for _, fn := range lsngb.fns {
		aggregation = append(aggregation, fn(selector))
	}
	// If no columns were selected in a custom aggregation function, the default
	// selection is the fields used for "group-by", and the aggregation functions.
	if len(selector.SelectedColumns()) == 0 {
		columns := make([]string, 0, len(lsngb.fields)+len(lsngb.fns))
		for _, f := range lsngb.fields {
			columns = append(columns, selector.C(f))
		}
		columns = append(columns, aggregation...)
		selector.Select(columns...)
	}
	return selector.GroupBy(selector.Columns(lsngb.fields...)...)
}

// LinstockStocksNasdaqSelect is the builder for selecting fields of LinstockStocksNasdaq entities.
type LinstockStocksNasdaqSelect struct {
	*LinstockStocksNasdaqQuery
	selector
	// intermediate query (i.e. traversal path).
	sql *sql.Selector
}

// Scan applies the selector query and scans the result into the given value.
func (lsns *LinstockStocksNasdaqSelect) Scan(ctx context.Context, v interface{}) error {
	if err := lsns.prepareQuery(ctx); err != nil {
		return err
	}
	lsns.sql = lsns.LinstockStocksNasdaqQuery.sqlQuery(ctx)
	return lsns.sqlScan(ctx, v)
}

func (lsns *LinstockStocksNasdaqSelect) sqlScan(ctx context.Context, v interface{}) error {
	rows := &sql.Rows{}
	query, args := lsns.sql.Query()
	if err := lsns.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}
