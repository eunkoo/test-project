// Code generated by ent, DO NOT EDIT.

package linstockstocksapiclientkey

import (
	"ezar-b2b-api/ent/predicate"
	"time"

	"entgo.io/ent/dialect/sql"
)

// ID filters vertices based on their ID field.
func ID(id int32) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDEQ applies the EQ predicate on the ID field.
func IDEQ(id int32) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDNEQ applies the NEQ predicate on the ID field.
func IDNEQ(id int32) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldID), id))
	})
}

// IDIn applies the In predicate on the ID field.
func IDIn(ids ...int32) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		v := make([]interface{}, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.In(s.C(FieldID), v...))
	})
}

// IDNotIn applies the NotIn predicate on the ID field.
func IDNotIn(ids ...int32) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		v := make([]interface{}, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.NotIn(s.C(FieldID), v...))
	})
}

// IDGT applies the GT predicate on the ID field.
func IDGT(id int32) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldID), id))
	})
}

// IDGTE applies the GTE predicate on the ID field.
func IDGTE(id int32) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldID), id))
	})
}

// IDLT applies the LT predicate on the ID field.
func IDLT(id int32) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldID), id))
	})
}

// IDLTE applies the LTE predicate on the ID field.
func IDLTE(id int32) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldID), id))
	})
}

// ClientEmail applies equality check predicate on the "client_email" field. It's identical to ClientEmailEQ.
func ClientEmail(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldClientEmail), v))
	})
}

// APIKey applies equality check predicate on the "api_key" field. It's identical to APIKeyEQ.
func APIKey(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldAPIKey), v))
	})
}

// IsExpired applies equality check predicate on the "is_expired" field. It's identical to IsExpiredEQ.
func IsExpired(v bool) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldIsExpired), v))
	})
}

// ExpireAt applies equality check predicate on the "expire_at" field. It's identical to ExpireAtEQ.
func ExpireAt(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldExpireAt), v))
	})
}

// CreatedAt applies equality check predicate on the "created_at" field. It's identical to CreatedAtEQ.
func CreatedAt(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldCreatedAt), v))
	})
}

// UpdatedAt applies equality check predicate on the "updated_at" field. It's identical to UpdatedAtEQ.
func UpdatedAt(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldUpdatedAt), v))
	})
}

// DeletedAt applies equality check predicate on the "deleted_at" field. It's identical to DeletedAtEQ.
func DeletedAt(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldDeletedAt), v))
	})
}

// ClientPassword applies equality check predicate on the "client_password" field. It's identical to ClientPasswordEQ.
func ClientPassword(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldClientPassword), v))
	})
}

// CompanyName applies equality check predicate on the "company_name" field. It's identical to CompanyNameEQ.
func CompanyName(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldCompanyName), v))
	})
}

// IsAdmin applies equality check predicate on the "is_admin" field. It's identical to IsAdminEQ.
func IsAdmin(v bool) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldIsAdmin), v))
	})
}

// ResponseFormat applies equality check predicate on the "response_format" field. It's identical to ResponseFormatEQ.
func ResponseFormat(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldResponseFormat), v))
	})
}

// ClientEmailEQ applies the EQ predicate on the "client_email" field.
func ClientEmailEQ(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldClientEmail), v))
	})
}

// ClientEmailNEQ applies the NEQ predicate on the "client_email" field.
func ClientEmailNEQ(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldClientEmail), v))
	})
}

// ClientEmailIn applies the In predicate on the "client_email" field.
func ClientEmailIn(vs ...string) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldClientEmail), v...))
	})
}

// ClientEmailNotIn applies the NotIn predicate on the "client_email" field.
func ClientEmailNotIn(vs ...string) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldClientEmail), v...))
	})
}

// ClientEmailGT applies the GT predicate on the "client_email" field.
func ClientEmailGT(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldClientEmail), v))
	})
}

// ClientEmailGTE applies the GTE predicate on the "client_email" field.
func ClientEmailGTE(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldClientEmail), v))
	})
}

// ClientEmailLT applies the LT predicate on the "client_email" field.
func ClientEmailLT(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldClientEmail), v))
	})
}

// ClientEmailLTE applies the LTE predicate on the "client_email" field.
func ClientEmailLTE(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldClientEmail), v))
	})
}

// ClientEmailContains applies the Contains predicate on the "client_email" field.
func ClientEmailContains(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldClientEmail), v))
	})
}

// ClientEmailHasPrefix applies the HasPrefix predicate on the "client_email" field.
func ClientEmailHasPrefix(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldClientEmail), v))
	})
}

// ClientEmailHasSuffix applies the HasSuffix predicate on the "client_email" field.
func ClientEmailHasSuffix(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldClientEmail), v))
	})
}

// ClientEmailEqualFold applies the EqualFold predicate on the "client_email" field.
func ClientEmailEqualFold(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldClientEmail), v))
	})
}

// ClientEmailContainsFold applies the ContainsFold predicate on the "client_email" field.
func ClientEmailContainsFold(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldClientEmail), v))
	})
}

// APIKeyEQ applies the EQ predicate on the "api_key" field.
func APIKeyEQ(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldAPIKey), v))
	})
}

// APIKeyNEQ applies the NEQ predicate on the "api_key" field.
func APIKeyNEQ(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldAPIKey), v))
	})
}

// APIKeyIn applies the In predicate on the "api_key" field.
func APIKeyIn(vs ...string) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldAPIKey), v...))
	})
}

// APIKeyNotIn applies the NotIn predicate on the "api_key" field.
func APIKeyNotIn(vs ...string) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldAPIKey), v...))
	})
}

// APIKeyGT applies the GT predicate on the "api_key" field.
func APIKeyGT(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldAPIKey), v))
	})
}

// APIKeyGTE applies the GTE predicate on the "api_key" field.
func APIKeyGTE(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldAPIKey), v))
	})
}

// APIKeyLT applies the LT predicate on the "api_key" field.
func APIKeyLT(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldAPIKey), v))
	})
}

// APIKeyLTE applies the LTE predicate on the "api_key" field.
func APIKeyLTE(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldAPIKey), v))
	})
}

// APIKeyContains applies the Contains predicate on the "api_key" field.
func APIKeyContains(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldAPIKey), v))
	})
}

// APIKeyHasPrefix applies the HasPrefix predicate on the "api_key" field.
func APIKeyHasPrefix(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldAPIKey), v))
	})
}

// APIKeyHasSuffix applies the HasSuffix predicate on the "api_key" field.
func APIKeyHasSuffix(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldAPIKey), v))
	})
}

// APIKeyEqualFold applies the EqualFold predicate on the "api_key" field.
func APIKeyEqualFold(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldAPIKey), v))
	})
}

// APIKeyContainsFold applies the ContainsFold predicate on the "api_key" field.
func APIKeyContainsFold(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldAPIKey), v))
	})
}

// IsExpiredEQ applies the EQ predicate on the "is_expired" field.
func IsExpiredEQ(v bool) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldIsExpired), v))
	})
}

// IsExpiredNEQ applies the NEQ predicate on the "is_expired" field.
func IsExpiredNEQ(v bool) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldIsExpired), v))
	})
}

// ExpireAtEQ applies the EQ predicate on the "expire_at" field.
func ExpireAtEQ(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldExpireAt), v))
	})
}

// ExpireAtNEQ applies the NEQ predicate on the "expire_at" field.
func ExpireAtNEQ(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldExpireAt), v))
	})
}

// ExpireAtIn applies the In predicate on the "expire_at" field.
func ExpireAtIn(vs ...time.Time) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldExpireAt), v...))
	})
}

// ExpireAtNotIn applies the NotIn predicate on the "expire_at" field.
func ExpireAtNotIn(vs ...time.Time) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldExpireAt), v...))
	})
}

// ExpireAtGT applies the GT predicate on the "expire_at" field.
func ExpireAtGT(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldExpireAt), v))
	})
}

// ExpireAtGTE applies the GTE predicate on the "expire_at" field.
func ExpireAtGTE(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldExpireAt), v))
	})
}

// ExpireAtLT applies the LT predicate on the "expire_at" field.
func ExpireAtLT(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldExpireAt), v))
	})
}

// ExpireAtLTE applies the LTE predicate on the "expire_at" field.
func ExpireAtLTE(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldExpireAt), v))
	})
}

// ExpireAtIsNil applies the IsNil predicate on the "expire_at" field.
func ExpireAtIsNil() predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldExpireAt)))
	})
}

// ExpireAtNotNil applies the NotNil predicate on the "expire_at" field.
func ExpireAtNotNil() predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldExpireAt)))
	})
}

// CreatedAtEQ applies the EQ predicate on the "created_at" field.
func CreatedAtEQ(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtNEQ applies the NEQ predicate on the "created_at" field.
func CreatedAtNEQ(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtIn applies the In predicate on the "created_at" field.
func CreatedAtIn(vs ...time.Time) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldCreatedAt), v...))
	})
}

// CreatedAtNotIn applies the NotIn predicate on the "created_at" field.
func CreatedAtNotIn(vs ...time.Time) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldCreatedAt), v...))
	})
}

// CreatedAtGT applies the GT predicate on the "created_at" field.
func CreatedAtGT(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtGTE applies the GTE predicate on the "created_at" field.
func CreatedAtGTE(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtLT applies the LT predicate on the "created_at" field.
func CreatedAtLT(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtLTE applies the LTE predicate on the "created_at" field.
func CreatedAtLTE(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtIsNil applies the IsNil predicate on the "created_at" field.
func CreatedAtIsNil() predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldCreatedAt)))
	})
}

// CreatedAtNotNil applies the NotNil predicate on the "created_at" field.
func CreatedAtNotNil() predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldCreatedAt)))
	})
}

// UpdatedAtEQ applies the EQ predicate on the "updated_at" field.
func UpdatedAtEQ(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldUpdatedAt), v))
	})
}

// UpdatedAtNEQ applies the NEQ predicate on the "updated_at" field.
func UpdatedAtNEQ(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldUpdatedAt), v))
	})
}

// UpdatedAtIn applies the In predicate on the "updated_at" field.
func UpdatedAtIn(vs ...time.Time) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldUpdatedAt), v...))
	})
}

// UpdatedAtNotIn applies the NotIn predicate on the "updated_at" field.
func UpdatedAtNotIn(vs ...time.Time) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldUpdatedAt), v...))
	})
}

// UpdatedAtGT applies the GT predicate on the "updated_at" field.
func UpdatedAtGT(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldUpdatedAt), v))
	})
}

// UpdatedAtGTE applies the GTE predicate on the "updated_at" field.
func UpdatedAtGTE(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldUpdatedAt), v))
	})
}

// UpdatedAtLT applies the LT predicate on the "updated_at" field.
func UpdatedAtLT(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldUpdatedAt), v))
	})
}

// UpdatedAtLTE applies the LTE predicate on the "updated_at" field.
func UpdatedAtLTE(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldUpdatedAt), v))
	})
}

// UpdatedAtIsNil applies the IsNil predicate on the "updated_at" field.
func UpdatedAtIsNil() predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldUpdatedAt)))
	})
}

// UpdatedAtNotNil applies the NotNil predicate on the "updated_at" field.
func UpdatedAtNotNil() predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldUpdatedAt)))
	})
}

// DeletedAtEQ applies the EQ predicate on the "deleted_at" field.
func DeletedAtEQ(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldDeletedAt), v))
	})
}

// DeletedAtNEQ applies the NEQ predicate on the "deleted_at" field.
func DeletedAtNEQ(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldDeletedAt), v))
	})
}

// DeletedAtIn applies the In predicate on the "deleted_at" field.
func DeletedAtIn(vs ...time.Time) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldDeletedAt), v...))
	})
}

// DeletedAtNotIn applies the NotIn predicate on the "deleted_at" field.
func DeletedAtNotIn(vs ...time.Time) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldDeletedAt), v...))
	})
}

// DeletedAtGT applies the GT predicate on the "deleted_at" field.
func DeletedAtGT(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldDeletedAt), v))
	})
}

// DeletedAtGTE applies the GTE predicate on the "deleted_at" field.
func DeletedAtGTE(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldDeletedAt), v))
	})
}

// DeletedAtLT applies the LT predicate on the "deleted_at" field.
func DeletedAtLT(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldDeletedAt), v))
	})
}

// DeletedAtLTE applies the LTE predicate on the "deleted_at" field.
func DeletedAtLTE(v time.Time) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldDeletedAt), v))
	})
}

// DeletedAtIsNil applies the IsNil predicate on the "deleted_at" field.
func DeletedAtIsNil() predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldDeletedAt)))
	})
}

// DeletedAtNotNil applies the NotNil predicate on the "deleted_at" field.
func DeletedAtNotNil() predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldDeletedAt)))
	})
}

// ClientPasswordEQ applies the EQ predicate on the "client_password" field.
func ClientPasswordEQ(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldClientPassword), v))
	})
}

// ClientPasswordNEQ applies the NEQ predicate on the "client_password" field.
func ClientPasswordNEQ(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldClientPassword), v))
	})
}

// ClientPasswordIn applies the In predicate on the "client_password" field.
func ClientPasswordIn(vs ...string) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldClientPassword), v...))
	})
}

// ClientPasswordNotIn applies the NotIn predicate on the "client_password" field.
func ClientPasswordNotIn(vs ...string) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldClientPassword), v...))
	})
}

// ClientPasswordGT applies the GT predicate on the "client_password" field.
func ClientPasswordGT(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldClientPassword), v))
	})
}

// ClientPasswordGTE applies the GTE predicate on the "client_password" field.
func ClientPasswordGTE(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldClientPassword), v))
	})
}

// ClientPasswordLT applies the LT predicate on the "client_password" field.
func ClientPasswordLT(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldClientPassword), v))
	})
}

// ClientPasswordLTE applies the LTE predicate on the "client_password" field.
func ClientPasswordLTE(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldClientPassword), v))
	})
}

// ClientPasswordContains applies the Contains predicate on the "client_password" field.
func ClientPasswordContains(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldClientPassword), v))
	})
}

// ClientPasswordHasPrefix applies the HasPrefix predicate on the "client_password" field.
func ClientPasswordHasPrefix(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldClientPassword), v))
	})
}

// ClientPasswordHasSuffix applies the HasSuffix predicate on the "client_password" field.
func ClientPasswordHasSuffix(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldClientPassword), v))
	})
}

// ClientPasswordEqualFold applies the EqualFold predicate on the "client_password" field.
func ClientPasswordEqualFold(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldClientPassword), v))
	})
}

// ClientPasswordContainsFold applies the ContainsFold predicate on the "client_password" field.
func ClientPasswordContainsFold(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldClientPassword), v))
	})
}

// CompanyNameEQ applies the EQ predicate on the "company_name" field.
func CompanyNameEQ(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldCompanyName), v))
	})
}

// CompanyNameNEQ applies the NEQ predicate on the "company_name" field.
func CompanyNameNEQ(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldCompanyName), v))
	})
}

// CompanyNameIn applies the In predicate on the "company_name" field.
func CompanyNameIn(vs ...string) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldCompanyName), v...))
	})
}

// CompanyNameNotIn applies the NotIn predicate on the "company_name" field.
func CompanyNameNotIn(vs ...string) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldCompanyName), v...))
	})
}

// CompanyNameGT applies the GT predicate on the "company_name" field.
func CompanyNameGT(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldCompanyName), v))
	})
}

// CompanyNameGTE applies the GTE predicate on the "company_name" field.
func CompanyNameGTE(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldCompanyName), v))
	})
}

// CompanyNameLT applies the LT predicate on the "company_name" field.
func CompanyNameLT(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldCompanyName), v))
	})
}

// CompanyNameLTE applies the LTE predicate on the "company_name" field.
func CompanyNameLTE(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldCompanyName), v))
	})
}

// CompanyNameContains applies the Contains predicate on the "company_name" field.
func CompanyNameContains(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldCompanyName), v))
	})
}

// CompanyNameHasPrefix applies the HasPrefix predicate on the "company_name" field.
func CompanyNameHasPrefix(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldCompanyName), v))
	})
}

// CompanyNameHasSuffix applies the HasSuffix predicate on the "company_name" field.
func CompanyNameHasSuffix(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldCompanyName), v))
	})
}

// CompanyNameEqualFold applies the EqualFold predicate on the "company_name" field.
func CompanyNameEqualFold(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldCompanyName), v))
	})
}

// CompanyNameContainsFold applies the ContainsFold predicate on the "company_name" field.
func CompanyNameContainsFold(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldCompanyName), v))
	})
}

// IsAdminEQ applies the EQ predicate on the "is_admin" field.
func IsAdminEQ(v bool) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldIsAdmin), v))
	})
}

// IsAdminNEQ applies the NEQ predicate on the "is_admin" field.
func IsAdminNEQ(v bool) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldIsAdmin), v))
	})
}

// ResponseFormatEQ applies the EQ predicate on the "response_format" field.
func ResponseFormatEQ(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldResponseFormat), v))
	})
}

// ResponseFormatNEQ applies the NEQ predicate on the "response_format" field.
func ResponseFormatNEQ(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldResponseFormat), v))
	})
}

// ResponseFormatIn applies the In predicate on the "response_format" field.
func ResponseFormatIn(vs ...string) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldResponseFormat), v...))
	})
}

// ResponseFormatNotIn applies the NotIn predicate on the "response_format" field.
func ResponseFormatNotIn(vs ...string) predicate.LinstockStocksApiclientkey {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldResponseFormat), v...))
	})
}

// ResponseFormatGT applies the GT predicate on the "response_format" field.
func ResponseFormatGT(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldResponseFormat), v))
	})
}

// ResponseFormatGTE applies the GTE predicate on the "response_format" field.
func ResponseFormatGTE(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldResponseFormat), v))
	})
}

// ResponseFormatLT applies the LT predicate on the "response_format" field.
func ResponseFormatLT(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldResponseFormat), v))
	})
}

// ResponseFormatLTE applies the LTE predicate on the "response_format" field.
func ResponseFormatLTE(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldResponseFormat), v))
	})
}

// ResponseFormatContains applies the Contains predicate on the "response_format" field.
func ResponseFormatContains(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldResponseFormat), v))
	})
}

// ResponseFormatHasPrefix applies the HasPrefix predicate on the "response_format" field.
func ResponseFormatHasPrefix(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldResponseFormat), v))
	})
}

// ResponseFormatHasSuffix applies the HasSuffix predicate on the "response_format" field.
func ResponseFormatHasSuffix(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldResponseFormat), v))
	})
}

// ResponseFormatEqualFold applies the EqualFold predicate on the "response_format" field.
func ResponseFormatEqualFold(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldResponseFormat), v))
	})
}

// ResponseFormatContainsFold applies the ContainsFold predicate on the "response_format" field.
func ResponseFormatContainsFold(v string) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldResponseFormat), v))
	})
}

// And groups predicates with the AND operator between them.
func And(predicates ...predicate.LinstockStocksApiclientkey) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for _, p := range predicates {
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Or groups predicates with the OR operator between them.
func Or(predicates ...predicate.LinstockStocksApiclientkey) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for i, p := range predicates {
			if i > 0 {
				s1.Or()
			}
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Not applies the not operator on the given predicate.
func Not(p predicate.LinstockStocksApiclientkey) predicate.LinstockStocksApiclientkey {
	return predicate.LinstockStocksApiclientkey(func(s *sql.Selector) {
		p(s.Not())
	})
}
