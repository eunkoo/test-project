// Code generated by ent, DO NOT EDIT.

package linstockstockstop100popularsymbol

const (
	// Label holds the string label denoting the linstockstockstop100popularsymbol type in the database.
	Label = "linstock_stocks_top100popularsymbol"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldSymbolID holds the string denoting the symbol_id field in the database.
	FieldSymbolID = "symbol_id"
	// FieldUpdatedAt holds the string denoting the updated_at field in the database.
	FieldUpdatedAt = "updated_at"
	// EdgeLinstockStocksSymbol holds the string denoting the linstock_stocks_symbol edge name in mutations.
	EdgeLinstockStocksSymbol = "linstock_stocks_symbol"
	// Table holds the table name of the linstockstockstop100popularsymbol in the database.
	Table = "linstock_stocks_top100popularsymbols"
	// LinstockStocksSymbolTable is the table that holds the linstock_stocks_symbol relation/edge.
	LinstockStocksSymbolTable = "linstock_stocks_top100popularsymbols"
	// LinstockStocksSymbolInverseTable is the table name for the LinstockStocksSymbol entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstockssymbol" package.
	LinstockStocksSymbolInverseTable = "linstock_stocks_symbol"
	// LinstockStocksSymbolColumn is the table column denoting the linstock_stocks_symbol relation/edge.
	LinstockStocksSymbolColumn = "symbol_id"
)

// Columns holds all SQL columns for linstockstockstop100popularsymbol fields.
var Columns = []string{
	FieldID,
	FieldSymbolID,
	FieldUpdatedAt,
}

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	return false
}
