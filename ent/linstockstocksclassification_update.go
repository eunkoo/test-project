// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"ezar-b2b-api/ent/linstockstocksclassification"
	"ezar-b2b-api/ent/predicate"
	"fmt"
	"time"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// LinstockStocksClassificationUpdate is the builder for updating LinstockStocksClassification entities.
type LinstockStocksClassificationUpdate struct {
	config
	hooks    []Hook
	mutation *LinstockStocksClassificationMutation
}

// Where appends a list predicates to the LinstockStocksClassificationUpdate builder.
func (lscu *LinstockStocksClassificationUpdate) Where(ps ...predicate.LinstockStocksClassification) *LinstockStocksClassificationUpdate {
	lscu.mutation.Where(ps...)
	return lscu
}

// SetCreatedAt sets the "created_at" field.
func (lscu *LinstockStocksClassificationUpdate) SetCreatedAt(t time.Time) *LinstockStocksClassificationUpdate {
	lscu.mutation.SetCreatedAt(t)
	return lscu
}

// SetUpdatedAt sets the "updated_at" field.
func (lscu *LinstockStocksClassificationUpdate) SetUpdatedAt(t time.Time) *LinstockStocksClassificationUpdate {
	lscu.mutation.SetUpdatedAt(t)
	return lscu
}

// SetNillableUpdatedAt sets the "updated_at" field if the given value is not nil.
func (lscu *LinstockStocksClassificationUpdate) SetNillableUpdatedAt(t *time.Time) *LinstockStocksClassificationUpdate {
	if t != nil {
		lscu.SetUpdatedAt(*t)
	}
	return lscu
}

// ClearUpdatedAt clears the value of the "updated_at" field.
func (lscu *LinstockStocksClassificationUpdate) ClearUpdatedAt() *LinstockStocksClassificationUpdate {
	lscu.mutation.ClearUpdatedAt()
	return lscu
}

// SetDeletedAt sets the "deleted_at" field.
func (lscu *LinstockStocksClassificationUpdate) SetDeletedAt(t time.Time) *LinstockStocksClassificationUpdate {
	lscu.mutation.SetDeletedAt(t)
	return lscu
}

// SetNillableDeletedAt sets the "deleted_at" field if the given value is not nil.
func (lscu *LinstockStocksClassificationUpdate) SetNillableDeletedAt(t *time.Time) *LinstockStocksClassificationUpdate {
	if t != nil {
		lscu.SetDeletedAt(*t)
	}
	return lscu
}

// ClearDeletedAt clears the value of the "deleted_at" field.
func (lscu *LinstockStocksClassificationUpdate) ClearDeletedAt() *LinstockStocksClassificationUpdate {
	lscu.mutation.ClearDeletedAt()
	return lscu
}

// SetTitle sets the "title" field.
func (lscu *LinstockStocksClassificationUpdate) SetTitle(s string) *LinstockStocksClassificationUpdate {
	lscu.mutation.SetTitle(s)
	return lscu
}

// SetDescription sets the "description" field.
func (lscu *LinstockStocksClassificationUpdate) SetDescription(s string) *LinstockStocksClassificationUpdate {
	lscu.mutation.SetDescription(s)
	return lscu
}

// SetNillableDescription sets the "description" field if the given value is not nil.
func (lscu *LinstockStocksClassificationUpdate) SetNillableDescription(s *string) *LinstockStocksClassificationUpdate {
	if s != nil {
		lscu.SetDescription(*s)
	}
	return lscu
}

// ClearDescription clears the value of the "description" field.
func (lscu *LinstockStocksClassificationUpdate) ClearDescription() *LinstockStocksClassificationUpdate {
	lscu.mutation.ClearDescription()
	return lscu
}

// SetDescriptionKo sets the "description_ko" field.
func (lscu *LinstockStocksClassificationUpdate) SetDescriptionKo(s string) *LinstockStocksClassificationUpdate {
	lscu.mutation.SetDescriptionKo(s)
	return lscu
}

// SetNillableDescriptionKo sets the "description_ko" field if the given value is not nil.
func (lscu *LinstockStocksClassificationUpdate) SetNillableDescriptionKo(s *string) *LinstockStocksClassificationUpdate {
	if s != nil {
		lscu.SetDescriptionKo(*s)
	}
	return lscu
}

// ClearDescriptionKo clears the value of the "description_ko" field.
func (lscu *LinstockStocksClassificationUpdate) ClearDescriptionKo() *LinstockStocksClassificationUpdate {
	lscu.mutation.ClearDescriptionKo()
	return lscu
}

// SetTitleKo sets the "title_ko" field.
func (lscu *LinstockStocksClassificationUpdate) SetTitleKo(s string) *LinstockStocksClassificationUpdate {
	lscu.mutation.SetTitleKo(s)
	return lscu
}

// SetIsRecommand sets the "is_recommand" field.
func (lscu *LinstockStocksClassificationUpdate) SetIsRecommand(b bool) *LinstockStocksClassificationUpdate {
	lscu.mutation.SetIsRecommand(b)
	return lscu
}

// Mutation returns the LinstockStocksClassificationMutation object of the builder.
func (lscu *LinstockStocksClassificationUpdate) Mutation() *LinstockStocksClassificationMutation {
	return lscu.mutation
}

// Save executes the query and returns the number of nodes affected by the update operation.
func (lscu *LinstockStocksClassificationUpdate) Save(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(lscu.hooks) == 0 {
		affected, err = lscu.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*LinstockStocksClassificationMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			lscu.mutation = mutation
			affected, err = lscu.sqlSave(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(lscu.hooks) - 1; i >= 0; i-- {
			if lscu.hooks[i] == nil {
				return 0, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = lscu.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, lscu.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// SaveX is like Save, but panics if an error occurs.
func (lscu *LinstockStocksClassificationUpdate) SaveX(ctx context.Context) int {
	affected, err := lscu.Save(ctx)
	if err != nil {
		panic(err)
	}
	return affected
}

// Exec executes the query.
func (lscu *LinstockStocksClassificationUpdate) Exec(ctx context.Context) error {
	_, err := lscu.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (lscu *LinstockStocksClassificationUpdate) ExecX(ctx context.Context) {
	if err := lscu.Exec(ctx); err != nil {
		panic(err)
	}
}

func (lscu *LinstockStocksClassificationUpdate) sqlSave(ctx context.Context) (n int, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   linstockstocksclassification.Table,
			Columns: linstockstocksclassification.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt32,
				Column: linstockstocksclassification.FieldID,
			},
		},
	}
	if ps := lscu.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := lscu.mutation.CreatedAt(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksclassification.FieldCreatedAt,
		})
	}
	if value, ok := lscu.mutation.UpdatedAt(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksclassification.FieldUpdatedAt,
		})
	}
	if lscu.mutation.UpdatedAtCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Column: linstockstocksclassification.FieldUpdatedAt,
		})
	}
	if value, ok := lscu.mutation.DeletedAt(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksclassification.FieldDeletedAt,
		})
	}
	if lscu.mutation.DeletedAtCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Column: linstockstocksclassification.FieldDeletedAt,
		})
	}
	if value, ok := lscu.mutation.Title(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: linstockstocksclassification.FieldTitle,
		})
	}
	if value, ok := lscu.mutation.Description(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: linstockstocksclassification.FieldDescription,
		})
	}
	if lscu.mutation.DescriptionCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Column: linstockstocksclassification.FieldDescription,
		})
	}
	if value, ok := lscu.mutation.DescriptionKo(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: linstockstocksclassification.FieldDescriptionKo,
		})
	}
	if lscu.mutation.DescriptionKoCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Column: linstockstocksclassification.FieldDescriptionKo,
		})
	}
	if value, ok := lscu.mutation.TitleKo(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: linstockstocksclassification.FieldTitleKo,
		})
	}
	if value, ok := lscu.mutation.IsRecommand(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeBool,
			Value:  value,
			Column: linstockstocksclassification.FieldIsRecommand,
		})
	}
	if n, err = sqlgraph.UpdateNodes(ctx, lscu.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{linstockstocksclassification.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return 0, err
	}
	return n, nil
}

// LinstockStocksClassificationUpdateOne is the builder for updating a single LinstockStocksClassification entity.
type LinstockStocksClassificationUpdateOne struct {
	config
	fields   []string
	hooks    []Hook
	mutation *LinstockStocksClassificationMutation
}

// SetCreatedAt sets the "created_at" field.
func (lscuo *LinstockStocksClassificationUpdateOne) SetCreatedAt(t time.Time) *LinstockStocksClassificationUpdateOne {
	lscuo.mutation.SetCreatedAt(t)
	return lscuo
}

// SetUpdatedAt sets the "updated_at" field.
func (lscuo *LinstockStocksClassificationUpdateOne) SetUpdatedAt(t time.Time) *LinstockStocksClassificationUpdateOne {
	lscuo.mutation.SetUpdatedAt(t)
	return lscuo
}

// SetNillableUpdatedAt sets the "updated_at" field if the given value is not nil.
func (lscuo *LinstockStocksClassificationUpdateOne) SetNillableUpdatedAt(t *time.Time) *LinstockStocksClassificationUpdateOne {
	if t != nil {
		lscuo.SetUpdatedAt(*t)
	}
	return lscuo
}

// ClearUpdatedAt clears the value of the "updated_at" field.
func (lscuo *LinstockStocksClassificationUpdateOne) ClearUpdatedAt() *LinstockStocksClassificationUpdateOne {
	lscuo.mutation.ClearUpdatedAt()
	return lscuo
}

// SetDeletedAt sets the "deleted_at" field.
func (lscuo *LinstockStocksClassificationUpdateOne) SetDeletedAt(t time.Time) *LinstockStocksClassificationUpdateOne {
	lscuo.mutation.SetDeletedAt(t)
	return lscuo
}

// SetNillableDeletedAt sets the "deleted_at" field if the given value is not nil.
func (lscuo *LinstockStocksClassificationUpdateOne) SetNillableDeletedAt(t *time.Time) *LinstockStocksClassificationUpdateOne {
	if t != nil {
		lscuo.SetDeletedAt(*t)
	}
	return lscuo
}

// ClearDeletedAt clears the value of the "deleted_at" field.
func (lscuo *LinstockStocksClassificationUpdateOne) ClearDeletedAt() *LinstockStocksClassificationUpdateOne {
	lscuo.mutation.ClearDeletedAt()
	return lscuo
}

// SetTitle sets the "title" field.
func (lscuo *LinstockStocksClassificationUpdateOne) SetTitle(s string) *LinstockStocksClassificationUpdateOne {
	lscuo.mutation.SetTitle(s)
	return lscuo
}

// SetDescription sets the "description" field.
func (lscuo *LinstockStocksClassificationUpdateOne) SetDescription(s string) *LinstockStocksClassificationUpdateOne {
	lscuo.mutation.SetDescription(s)
	return lscuo
}

// SetNillableDescription sets the "description" field if the given value is not nil.
func (lscuo *LinstockStocksClassificationUpdateOne) SetNillableDescription(s *string) *LinstockStocksClassificationUpdateOne {
	if s != nil {
		lscuo.SetDescription(*s)
	}
	return lscuo
}

// ClearDescription clears the value of the "description" field.
func (lscuo *LinstockStocksClassificationUpdateOne) ClearDescription() *LinstockStocksClassificationUpdateOne {
	lscuo.mutation.ClearDescription()
	return lscuo
}

// SetDescriptionKo sets the "description_ko" field.
func (lscuo *LinstockStocksClassificationUpdateOne) SetDescriptionKo(s string) *LinstockStocksClassificationUpdateOne {
	lscuo.mutation.SetDescriptionKo(s)
	return lscuo
}

// SetNillableDescriptionKo sets the "description_ko" field if the given value is not nil.
func (lscuo *LinstockStocksClassificationUpdateOne) SetNillableDescriptionKo(s *string) *LinstockStocksClassificationUpdateOne {
	if s != nil {
		lscuo.SetDescriptionKo(*s)
	}
	return lscuo
}

// ClearDescriptionKo clears the value of the "description_ko" field.
func (lscuo *LinstockStocksClassificationUpdateOne) ClearDescriptionKo() *LinstockStocksClassificationUpdateOne {
	lscuo.mutation.ClearDescriptionKo()
	return lscuo
}

// SetTitleKo sets the "title_ko" field.
func (lscuo *LinstockStocksClassificationUpdateOne) SetTitleKo(s string) *LinstockStocksClassificationUpdateOne {
	lscuo.mutation.SetTitleKo(s)
	return lscuo
}

// SetIsRecommand sets the "is_recommand" field.
func (lscuo *LinstockStocksClassificationUpdateOne) SetIsRecommand(b bool) *LinstockStocksClassificationUpdateOne {
	lscuo.mutation.SetIsRecommand(b)
	return lscuo
}

// Mutation returns the LinstockStocksClassificationMutation object of the builder.
func (lscuo *LinstockStocksClassificationUpdateOne) Mutation() *LinstockStocksClassificationMutation {
	return lscuo.mutation
}

// Select allows selecting one or more fields (columns) of the returned entity.
// The default is selecting all fields defined in the entity schema.
func (lscuo *LinstockStocksClassificationUpdateOne) Select(field string, fields ...string) *LinstockStocksClassificationUpdateOne {
	lscuo.fields = append([]string{field}, fields...)
	return lscuo
}

// Save executes the query and returns the updated LinstockStocksClassification entity.
func (lscuo *LinstockStocksClassificationUpdateOne) Save(ctx context.Context) (*LinstockStocksClassification, error) {
	var (
		err  error
		node *LinstockStocksClassification
	)
	if len(lscuo.hooks) == 0 {
		node, err = lscuo.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*LinstockStocksClassificationMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			lscuo.mutation = mutation
			node, err = lscuo.sqlSave(ctx)
			mutation.done = true
			return node, err
		})
		for i := len(lscuo.hooks) - 1; i >= 0; i-- {
			if lscuo.hooks[i] == nil {
				return nil, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = lscuo.hooks[i](mut)
		}
		v, err := mut.Mutate(ctx, lscuo.mutation)
		if err != nil {
			return nil, err
		}
		nv, ok := v.(*LinstockStocksClassification)
		if !ok {
			return nil, fmt.Errorf("unexpected node type %T returned from LinstockStocksClassificationMutation", v)
		}
		node = nv
	}
	return node, err
}

// SaveX is like Save, but panics if an error occurs.
func (lscuo *LinstockStocksClassificationUpdateOne) SaveX(ctx context.Context) *LinstockStocksClassification {
	node, err := lscuo.Save(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// Exec executes the query on the entity.
func (lscuo *LinstockStocksClassificationUpdateOne) Exec(ctx context.Context) error {
	_, err := lscuo.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (lscuo *LinstockStocksClassificationUpdateOne) ExecX(ctx context.Context) {
	if err := lscuo.Exec(ctx); err != nil {
		panic(err)
	}
}

func (lscuo *LinstockStocksClassificationUpdateOne) sqlSave(ctx context.Context) (_node *LinstockStocksClassification, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   linstockstocksclassification.Table,
			Columns: linstockstocksclassification.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt32,
				Column: linstockstocksclassification.FieldID,
			},
		},
	}
	id, ok := lscuo.mutation.ID()
	if !ok {
		return nil, &ValidationError{Name: "id", err: errors.New(`ent: missing "LinstockStocksClassification.id" for update`)}
	}
	_spec.Node.ID.Value = id
	if fields := lscuo.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, linstockstocksclassification.FieldID)
		for _, f := range fields {
			if !linstockstocksclassification.ValidColumn(f) {
				return nil, &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
			}
			if f != linstockstocksclassification.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, f)
			}
		}
	}
	if ps := lscuo.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := lscuo.mutation.CreatedAt(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksclassification.FieldCreatedAt,
		})
	}
	if value, ok := lscuo.mutation.UpdatedAt(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksclassification.FieldUpdatedAt,
		})
	}
	if lscuo.mutation.UpdatedAtCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Column: linstockstocksclassification.FieldUpdatedAt,
		})
	}
	if value, ok := lscuo.mutation.DeletedAt(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksclassification.FieldDeletedAt,
		})
	}
	if lscuo.mutation.DeletedAtCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Column: linstockstocksclassification.FieldDeletedAt,
		})
	}
	if value, ok := lscuo.mutation.Title(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: linstockstocksclassification.FieldTitle,
		})
	}
	if value, ok := lscuo.mutation.Description(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: linstockstocksclassification.FieldDescription,
		})
	}
	if lscuo.mutation.DescriptionCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Column: linstockstocksclassification.FieldDescription,
		})
	}
	if value, ok := lscuo.mutation.DescriptionKo(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: linstockstocksclassification.FieldDescriptionKo,
		})
	}
	if lscuo.mutation.DescriptionKoCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Column: linstockstocksclassification.FieldDescriptionKo,
		})
	}
	if value, ok := lscuo.mutation.TitleKo(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: linstockstocksclassification.FieldTitleKo,
		})
	}
	if value, ok := lscuo.mutation.IsRecommand(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeBool,
			Value:  value,
			Column: linstockstocksclassification.FieldIsRecommand,
		})
	}
	_node = &LinstockStocksClassification{config: lscuo.config}
	_spec.Assign = _node.assignValues
	_spec.ScanValues = _node.scanValues
	if err = sqlgraph.UpdateNode(ctx, lscuo.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{linstockstocksclassification.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	return _node, nil
}
