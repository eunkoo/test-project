// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"database/sql/driver"
	"ezar-b2b-api/ent/linstockstockspubetfcategory"
	"ezar-b2b-api/ent/linstockstockspubetfcategorysymbol"
	"ezar-b2b-api/ent/predicate"
	"fmt"
	"math"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// LinstockStocksPubetfcategoryQuery is the builder for querying LinstockStocksPubetfcategory entities.
type LinstockStocksPubetfcategoryQuery struct {
	config
	limit                                   *int
	offset                                  *int
	unique                                  *bool
	order                                   []OrderFunc
	fields                                  []string
	predicates                              []predicate.LinstockStocksPubetfcategory
	withLinstockStocksPubetfcategorySymbols *LinstockStocksPubetfcategorySymbolQuery
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Where adds a new predicate for the LinstockStocksPubetfcategoryQuery builder.
func (lspq *LinstockStocksPubetfcategoryQuery) Where(ps ...predicate.LinstockStocksPubetfcategory) *LinstockStocksPubetfcategoryQuery {
	lspq.predicates = append(lspq.predicates, ps...)
	return lspq
}

// Limit adds a limit step to the query.
func (lspq *LinstockStocksPubetfcategoryQuery) Limit(limit int) *LinstockStocksPubetfcategoryQuery {
	lspq.limit = &limit
	return lspq
}

// Offset adds an offset step to the query.
func (lspq *LinstockStocksPubetfcategoryQuery) Offset(offset int) *LinstockStocksPubetfcategoryQuery {
	lspq.offset = &offset
	return lspq
}

// Unique configures the query builder to filter duplicate records on query.
// By default, unique is set to true, and can be disabled using this method.
func (lspq *LinstockStocksPubetfcategoryQuery) Unique(unique bool) *LinstockStocksPubetfcategoryQuery {
	lspq.unique = &unique
	return lspq
}

// Order adds an order step to the query.
func (lspq *LinstockStocksPubetfcategoryQuery) Order(o ...OrderFunc) *LinstockStocksPubetfcategoryQuery {
	lspq.order = append(lspq.order, o...)
	return lspq
}

// QueryLinstockStocksPubetfcategorySymbols chains the current query on the "linstock_stocks_pubetfcategory_symbols" edge.
func (lspq *LinstockStocksPubetfcategoryQuery) QueryLinstockStocksPubetfcategorySymbols() *LinstockStocksPubetfcategorySymbolQuery {
	query := &LinstockStocksPubetfcategorySymbolQuery{config: lspq.config}
	query.path = func(ctx context.Context) (fromU *sql.Selector, err error) {
		if err := lspq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		selector := lspq.sqlQuery(ctx)
		if err := selector.Err(); err != nil {
			return nil, err
		}
		step := sqlgraph.NewStep(
			sqlgraph.From(linstockstockspubetfcategory.Table, linstockstockspubetfcategory.FieldID, selector),
			sqlgraph.To(linstockstockspubetfcategorysymbol.Table, linstockstockspubetfcategorysymbol.FieldID),
			sqlgraph.Edge(sqlgraph.O2M, false, linstockstockspubetfcategory.LinstockStocksPubetfcategorySymbolsTable, linstockstockspubetfcategory.LinstockStocksPubetfcategorySymbolsColumn),
		)
		fromU = sqlgraph.SetNeighbors(lspq.driver.Dialect(), step)
		return fromU, nil
	}
	return query
}

// First returns the first LinstockStocksPubetfcategory entity from the query.
// Returns a *NotFoundError when no LinstockStocksPubetfcategory was found.
func (lspq *LinstockStocksPubetfcategoryQuery) First(ctx context.Context) (*LinstockStocksPubetfcategory, error) {
	nodes, err := lspq.Limit(1).All(ctx)
	if err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nil, &NotFoundError{linstockstockspubetfcategory.Label}
	}
	return nodes[0], nil
}

// FirstX is like First, but panics if an error occurs.
func (lspq *LinstockStocksPubetfcategoryQuery) FirstX(ctx context.Context) *LinstockStocksPubetfcategory {
	node, err := lspq.First(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return node
}

// FirstID returns the first LinstockStocksPubetfcategory ID from the query.
// Returns a *NotFoundError when no LinstockStocksPubetfcategory ID was found.
func (lspq *LinstockStocksPubetfcategoryQuery) FirstID(ctx context.Context) (id int32, err error) {
	var ids []int32
	if ids, err = lspq.Limit(1).IDs(ctx); err != nil {
		return
	}
	if len(ids) == 0 {
		err = &NotFoundError{linstockstockspubetfcategory.Label}
		return
	}
	return ids[0], nil
}

// FirstIDX is like FirstID, but panics if an error occurs.
func (lspq *LinstockStocksPubetfcategoryQuery) FirstIDX(ctx context.Context) int32 {
	id, err := lspq.FirstID(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return id
}

// Only returns a single LinstockStocksPubetfcategory entity found by the query, ensuring it only returns one.
// Returns a *NotSingularError when more than one LinstockStocksPubetfcategory entity is found.
// Returns a *NotFoundError when no LinstockStocksPubetfcategory entities are found.
func (lspq *LinstockStocksPubetfcategoryQuery) Only(ctx context.Context) (*LinstockStocksPubetfcategory, error) {
	nodes, err := lspq.Limit(2).All(ctx)
	if err != nil {
		return nil, err
	}
	switch len(nodes) {
	case 1:
		return nodes[0], nil
	case 0:
		return nil, &NotFoundError{linstockstockspubetfcategory.Label}
	default:
		return nil, &NotSingularError{linstockstockspubetfcategory.Label}
	}
}

// OnlyX is like Only, but panics if an error occurs.
func (lspq *LinstockStocksPubetfcategoryQuery) OnlyX(ctx context.Context) *LinstockStocksPubetfcategory {
	node, err := lspq.Only(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// OnlyID is like Only, but returns the only LinstockStocksPubetfcategory ID in the query.
// Returns a *NotSingularError when more than one LinstockStocksPubetfcategory ID is found.
// Returns a *NotFoundError when no entities are found.
func (lspq *LinstockStocksPubetfcategoryQuery) OnlyID(ctx context.Context) (id int32, err error) {
	var ids []int32
	if ids, err = lspq.Limit(2).IDs(ctx); err != nil {
		return
	}
	switch len(ids) {
	case 1:
		id = ids[0]
	case 0:
		err = &NotFoundError{linstockstockspubetfcategory.Label}
	default:
		err = &NotSingularError{linstockstockspubetfcategory.Label}
	}
	return
}

// OnlyIDX is like OnlyID, but panics if an error occurs.
func (lspq *LinstockStocksPubetfcategoryQuery) OnlyIDX(ctx context.Context) int32 {
	id, err := lspq.OnlyID(ctx)
	if err != nil {
		panic(err)
	}
	return id
}

// All executes the query and returns a list of LinstockStocksPubetfcategories.
func (lspq *LinstockStocksPubetfcategoryQuery) All(ctx context.Context) ([]*LinstockStocksPubetfcategory, error) {
	if err := lspq.prepareQuery(ctx); err != nil {
		return nil, err
	}
	return lspq.sqlAll(ctx)
}

// AllX is like All, but panics if an error occurs.
func (lspq *LinstockStocksPubetfcategoryQuery) AllX(ctx context.Context) []*LinstockStocksPubetfcategory {
	nodes, err := lspq.All(ctx)
	if err != nil {
		panic(err)
	}
	return nodes
}

// IDs executes the query and returns a list of LinstockStocksPubetfcategory IDs.
func (lspq *LinstockStocksPubetfcategoryQuery) IDs(ctx context.Context) ([]int32, error) {
	var ids []int32
	if err := lspq.Select(linstockstockspubetfcategory.FieldID).Scan(ctx, &ids); err != nil {
		return nil, err
	}
	return ids, nil
}

// IDsX is like IDs, but panics if an error occurs.
func (lspq *LinstockStocksPubetfcategoryQuery) IDsX(ctx context.Context) []int32 {
	ids, err := lspq.IDs(ctx)
	if err != nil {
		panic(err)
	}
	return ids
}

// Count returns the count of the given query.
func (lspq *LinstockStocksPubetfcategoryQuery) Count(ctx context.Context) (int, error) {
	if err := lspq.prepareQuery(ctx); err != nil {
		return 0, err
	}
	return lspq.sqlCount(ctx)
}

// CountX is like Count, but panics if an error occurs.
func (lspq *LinstockStocksPubetfcategoryQuery) CountX(ctx context.Context) int {
	count, err := lspq.Count(ctx)
	if err != nil {
		panic(err)
	}
	return count
}

// Exist returns true if the query has elements in the graph.
func (lspq *LinstockStocksPubetfcategoryQuery) Exist(ctx context.Context) (bool, error) {
	if err := lspq.prepareQuery(ctx); err != nil {
		return false, err
	}
	return lspq.sqlExist(ctx)
}

// ExistX is like Exist, but panics if an error occurs.
func (lspq *LinstockStocksPubetfcategoryQuery) ExistX(ctx context.Context) bool {
	exist, err := lspq.Exist(ctx)
	if err != nil {
		panic(err)
	}
	return exist
}

// Clone returns a duplicate of the LinstockStocksPubetfcategoryQuery builder, including all associated steps. It can be
// used to prepare common query builders and use them differently after the clone is made.
func (lspq *LinstockStocksPubetfcategoryQuery) Clone() *LinstockStocksPubetfcategoryQuery {
	if lspq == nil {
		return nil
	}
	return &LinstockStocksPubetfcategoryQuery{
		config:                                  lspq.config,
		limit:                                   lspq.limit,
		offset:                                  lspq.offset,
		order:                                   append([]OrderFunc{}, lspq.order...),
		predicates:                              append([]predicate.LinstockStocksPubetfcategory{}, lspq.predicates...),
		withLinstockStocksPubetfcategorySymbols: lspq.withLinstockStocksPubetfcategorySymbols.Clone(),
		// clone intermediate query.
		sql:    lspq.sql.Clone(),
		path:   lspq.path,
		unique: lspq.unique,
	}
}

// WithLinstockStocksPubetfcategorySymbols tells the query-builder to eager-load the nodes that are connected to
// the "linstock_stocks_pubetfcategory_symbols" edge. The optional arguments are used to configure the query builder of the edge.
func (lspq *LinstockStocksPubetfcategoryQuery) WithLinstockStocksPubetfcategorySymbols(opts ...func(*LinstockStocksPubetfcategorySymbolQuery)) *LinstockStocksPubetfcategoryQuery {
	query := &LinstockStocksPubetfcategorySymbolQuery{config: lspq.config}
	for _, opt := range opts {
		opt(query)
	}
	lspq.withLinstockStocksPubetfcategorySymbols = query
	return lspq
}

// GroupBy is used to group vertices by one or more fields/columns.
// It is often used with aggregate functions, like: count, max, mean, min, sum.
//
// Example:
//
//	var v []struct {
//		Title string `json:"title,omitempty"`
//		Count int `json:"count,omitempty"`
//	}
//
//	client.LinstockStocksPubetfcategory.Query().
//		GroupBy(linstockstockspubetfcategory.FieldTitle).
//		Aggregate(ent.Count()).
//		Scan(ctx, &v)
func (lspq *LinstockStocksPubetfcategoryQuery) GroupBy(field string, fields ...string) *LinstockStocksPubetfcategoryGroupBy {
	grbuild := &LinstockStocksPubetfcategoryGroupBy{config: lspq.config}
	grbuild.fields = append([]string{field}, fields...)
	grbuild.path = func(ctx context.Context) (prev *sql.Selector, err error) {
		if err := lspq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		return lspq.sqlQuery(ctx), nil
	}
	grbuild.label = linstockstockspubetfcategory.Label
	grbuild.flds, grbuild.scan = &grbuild.fields, grbuild.Scan
	return grbuild
}

// Select allows the selection one or more fields/columns for the given query,
// instead of selecting all fields in the entity.
//
// Example:
//
//	var v []struct {
//		Title string `json:"title,omitempty"`
//	}
//
//	client.LinstockStocksPubetfcategory.Query().
//		Select(linstockstockspubetfcategory.FieldTitle).
//		Scan(ctx, &v)
func (lspq *LinstockStocksPubetfcategoryQuery) Select(fields ...string) *LinstockStocksPubetfcategorySelect {
	lspq.fields = append(lspq.fields, fields...)
	selbuild := &LinstockStocksPubetfcategorySelect{LinstockStocksPubetfcategoryQuery: lspq}
	selbuild.label = linstockstockspubetfcategory.Label
	selbuild.flds, selbuild.scan = &lspq.fields, selbuild.Scan
	return selbuild
}

func (lspq *LinstockStocksPubetfcategoryQuery) prepareQuery(ctx context.Context) error {
	for _, f := range lspq.fields {
		if !linstockstockspubetfcategory.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
		}
	}
	if lspq.path != nil {
		prev, err := lspq.path(ctx)
		if err != nil {
			return err
		}
		lspq.sql = prev
	}
	return nil
}

func (lspq *LinstockStocksPubetfcategoryQuery) sqlAll(ctx context.Context, hooks ...queryHook) ([]*LinstockStocksPubetfcategory, error) {
	var (
		nodes       = []*LinstockStocksPubetfcategory{}
		_spec       = lspq.querySpec()
		loadedTypes = [1]bool{
			lspq.withLinstockStocksPubetfcategorySymbols != nil,
		}
	)
	_spec.ScanValues = func(columns []string) ([]interface{}, error) {
		return (*LinstockStocksPubetfcategory).scanValues(nil, columns)
	}
	_spec.Assign = func(columns []string, values []interface{}) error {
		node := &LinstockStocksPubetfcategory{config: lspq.config}
		nodes = append(nodes, node)
		node.Edges.loadedTypes = loadedTypes
		return node.assignValues(columns, values)
	}
	for i := range hooks {
		hooks[i](ctx, _spec)
	}
	if err := sqlgraph.QueryNodes(ctx, lspq.driver, _spec); err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nodes, nil
	}
	if query := lspq.withLinstockStocksPubetfcategorySymbols; query != nil {
		if err := lspq.loadLinstockStocksPubetfcategorySymbols(ctx, query, nodes,
			func(n *LinstockStocksPubetfcategory) {
				n.Edges.LinstockStocksPubetfcategorySymbols = []*LinstockStocksPubetfcategorySymbol{}
			},
			func(n *LinstockStocksPubetfcategory, e *LinstockStocksPubetfcategorySymbol) {
				n.Edges.LinstockStocksPubetfcategorySymbols = append(n.Edges.LinstockStocksPubetfcategorySymbols, e)
			}); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

func (lspq *LinstockStocksPubetfcategoryQuery) loadLinstockStocksPubetfcategorySymbols(ctx context.Context, query *LinstockStocksPubetfcategorySymbolQuery, nodes []*LinstockStocksPubetfcategory, init func(*LinstockStocksPubetfcategory), assign func(*LinstockStocksPubetfcategory, *LinstockStocksPubetfcategorySymbol)) error {
	fks := make([]driver.Value, 0, len(nodes))
	nodeids := make(map[int32]*LinstockStocksPubetfcategory)
	for i := range nodes {
		fks = append(fks, nodes[i].ID)
		nodeids[nodes[i].ID] = nodes[i]
		if init != nil {
			init(nodes[i])
		}
	}
	query.Where(predicate.LinstockStocksPubetfcategorySymbol(func(s *sql.Selector) {
		s.Where(sql.InValues(linstockstockspubetfcategory.LinstockStocksPubetfcategorySymbolsColumn, fks...))
	}))
	neighbors, err := query.All(ctx)
	if err != nil {
		return err
	}
	for _, n := range neighbors {
		fk := n.PubetfcategoryID
		node, ok := nodeids[fk]
		if !ok {
			return fmt.Errorf(`unexpected foreign-key "pubetfcategory_id" returned %v for node %v`, fk, n.ID)
		}
		assign(node, n)
	}
	return nil
}

func (lspq *LinstockStocksPubetfcategoryQuery) sqlCount(ctx context.Context) (int, error) {
	_spec := lspq.querySpec()
	_spec.Node.Columns = lspq.fields
	if len(lspq.fields) > 0 {
		_spec.Unique = lspq.unique != nil && *lspq.unique
	}
	return sqlgraph.CountNodes(ctx, lspq.driver, _spec)
}

func (lspq *LinstockStocksPubetfcategoryQuery) sqlExist(ctx context.Context) (bool, error) {
	n, err := lspq.sqlCount(ctx)
	if err != nil {
		return false, fmt.Errorf("ent: check existence: %w", err)
	}
	return n > 0, nil
}

func (lspq *LinstockStocksPubetfcategoryQuery) querySpec() *sqlgraph.QuerySpec {
	_spec := &sqlgraph.QuerySpec{
		Node: &sqlgraph.NodeSpec{
			Table:   linstockstockspubetfcategory.Table,
			Columns: linstockstockspubetfcategory.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt32,
				Column: linstockstockspubetfcategory.FieldID,
			},
		},
		From:   lspq.sql,
		Unique: true,
	}
	if unique := lspq.unique; unique != nil {
		_spec.Unique = *unique
	}
	if fields := lspq.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, linstockstockspubetfcategory.FieldID)
		for i := range fields {
			if fields[i] != linstockstockspubetfcategory.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, fields[i])
			}
		}
	}
	if ps := lspq.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if limit := lspq.limit; limit != nil {
		_spec.Limit = *limit
	}
	if offset := lspq.offset; offset != nil {
		_spec.Offset = *offset
	}
	if ps := lspq.order; len(ps) > 0 {
		_spec.Order = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	return _spec
}

func (lspq *LinstockStocksPubetfcategoryQuery) sqlQuery(ctx context.Context) *sql.Selector {
	builder := sql.Dialect(lspq.driver.Dialect())
	t1 := builder.Table(linstockstockspubetfcategory.Table)
	columns := lspq.fields
	if len(columns) == 0 {
		columns = linstockstockspubetfcategory.Columns
	}
	selector := builder.Select(t1.Columns(columns...)...).From(t1)
	if lspq.sql != nil {
		selector = lspq.sql
		selector.Select(selector.Columns(columns...)...)
	}
	if lspq.unique != nil && *lspq.unique {
		selector.Distinct()
	}
	for _, p := range lspq.predicates {
		p(selector)
	}
	for _, p := range lspq.order {
		p(selector)
	}
	if offset := lspq.offset; offset != nil {
		// limit is mandatory for offset clause. We start
		// with default value, and override it below if needed.
		selector.Offset(*offset).Limit(math.MaxInt32)
	}
	if limit := lspq.limit; limit != nil {
		selector.Limit(*limit)
	}
	return selector
}

// LinstockStocksPubetfcategoryGroupBy is the group-by builder for LinstockStocksPubetfcategory entities.
type LinstockStocksPubetfcategoryGroupBy struct {
	config
	selector
	fields []string
	fns    []AggregateFunc
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Aggregate adds the given aggregation functions to the group-by query.
func (lspgb *LinstockStocksPubetfcategoryGroupBy) Aggregate(fns ...AggregateFunc) *LinstockStocksPubetfcategoryGroupBy {
	lspgb.fns = append(lspgb.fns, fns...)
	return lspgb
}

// Scan applies the group-by query and scans the result into the given value.
func (lspgb *LinstockStocksPubetfcategoryGroupBy) Scan(ctx context.Context, v interface{}) error {
	query, err := lspgb.path(ctx)
	if err != nil {
		return err
	}
	lspgb.sql = query
	return lspgb.sqlScan(ctx, v)
}

func (lspgb *LinstockStocksPubetfcategoryGroupBy) sqlScan(ctx context.Context, v interface{}) error {
	for _, f := range lspgb.fields {
		if !linstockstockspubetfcategory.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("invalid field %q for group-by", f)}
		}
	}
	selector := lspgb.sqlQuery()
	if err := selector.Err(); err != nil {
		return err
	}
	rows := &sql.Rows{}
	query, args := selector.Query()
	if err := lspgb.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

func (lspgb *LinstockStocksPubetfcategoryGroupBy) sqlQuery() *sql.Selector {
	selector := lspgb.sql.Select()
	aggregation := make([]string, 0, len(lspgb.fns))
	for _, fn := range lspgb.fns {
		aggregation = append(aggregation, fn(selector))
	}
	// If no columns were selected in a custom aggregation function, the default
	// selection is the fields used for "group-by", and the aggregation functions.
	if len(selector.SelectedColumns()) == 0 {
		columns := make([]string, 0, len(lspgb.fields)+len(lspgb.fns))
		for _, f := range lspgb.fields {
			columns = append(columns, selector.C(f))
		}
		columns = append(columns, aggregation...)
		selector.Select(columns...)
	}
	return selector.GroupBy(selector.Columns(lspgb.fields...)...)
}

// LinstockStocksPubetfcategorySelect is the builder for selecting fields of LinstockStocksPubetfcategory entities.
type LinstockStocksPubetfcategorySelect struct {
	*LinstockStocksPubetfcategoryQuery
	selector
	// intermediate query (i.e. traversal path).
	sql *sql.Selector
}

// Scan applies the selector query and scans the result into the given value.
func (lsps *LinstockStocksPubetfcategorySelect) Scan(ctx context.Context, v interface{}) error {
	if err := lsps.prepareQuery(ctx); err != nil {
		return err
	}
	lsps.sql = lsps.LinstockStocksPubetfcategoryQuery.sqlQuery(ctx)
	return lsps.sqlScan(ctx, v)
}

func (lsps *LinstockStocksPubetfcategorySelect) sqlScan(ctx context.Context, v interface{}) error {
	rows := &sql.Rows{}
	query, args := lsps.sql.Query()
	if err := lsps.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}
