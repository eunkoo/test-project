// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"ezar-b2b-api/ent/linstockstockssymboldailyprice"
	"ezar-b2b-api/ent/predicate"
	"fmt"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// LinstockStocksSymboldailypriceDelete is the builder for deleting a LinstockStocksSymboldailyprice entity.
type LinstockStocksSymboldailypriceDelete struct {
	config
	hooks    []Hook
	mutation *LinstockStocksSymboldailypriceMutation
}

// Where appends a list predicates to the LinstockStocksSymboldailypriceDelete builder.
func (lssd *LinstockStocksSymboldailypriceDelete) Where(ps ...predicate.LinstockStocksSymboldailyprice) *LinstockStocksSymboldailypriceDelete {
	lssd.mutation.Where(ps...)
	return lssd
}

// Exec executes the deletion query and returns how many vertices were deleted.
func (lssd *LinstockStocksSymboldailypriceDelete) Exec(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(lssd.hooks) == 0 {
		affected, err = lssd.sqlExec(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*LinstockStocksSymboldailypriceMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			lssd.mutation = mutation
			affected, err = lssd.sqlExec(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(lssd.hooks) - 1; i >= 0; i-- {
			if lssd.hooks[i] == nil {
				return 0, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = lssd.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, lssd.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// ExecX is like Exec, but panics if an error occurs.
func (lssd *LinstockStocksSymboldailypriceDelete) ExecX(ctx context.Context) int {
	n, err := lssd.Exec(ctx)
	if err != nil {
		panic(err)
	}
	return n
}

func (lssd *LinstockStocksSymboldailypriceDelete) sqlExec(ctx context.Context) (int, error) {
	_spec := &sqlgraph.DeleteSpec{
		Node: &sqlgraph.NodeSpec{
			Table: linstockstockssymboldailyprice.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt32,
				Column: linstockstockssymboldailyprice.FieldID,
			},
		},
	}
	if ps := lssd.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	affected, err := sqlgraph.DeleteNodes(ctx, lssd.driver, _spec)
	if err != nil && sqlgraph.IsConstraintError(err) {
		err = &ConstraintError{msg: err.Error(), wrap: err}
	}
	return affected, err
}

// LinstockStocksSymboldailypriceDeleteOne is the builder for deleting a single LinstockStocksSymboldailyprice entity.
type LinstockStocksSymboldailypriceDeleteOne struct {
	lssd *LinstockStocksSymboldailypriceDelete
}

// Exec executes the deletion query.
func (lssdo *LinstockStocksSymboldailypriceDeleteOne) Exec(ctx context.Context) error {
	n, err := lssdo.lssd.Exec(ctx)
	switch {
	case err != nil:
		return err
	case n == 0:
		return &NotFoundError{linstockstockssymboldailyprice.Label}
	default:
		return nil
	}
}

// ExecX is like Exec, but panics if an error occurs.
func (lssdo *LinstockStocksSymboldailypriceDeleteOne) ExecX(ctx context.Context) {
	lssdo.lssd.ExecX(ctx)
}
