// Code generated by ent, DO NOT EDIT.

package ent

import (
	"ezar-b2b-api/ent/linstockstocksetfholder"
	"fmt"
	"strings"
	"time"

	"entgo.io/ent/dialect/sql"
)

// LinstockStocksEtfholder is the model entity for the LinstockStocksEtfholder schema.
type LinstockStocksEtfholder struct {
	config `json:"-"`
	// ID of the ent.
	ID string `json:"id,omitempty"`
	// EtfHolders holds the value of the "etf_holders" field.
	EtfHolders string `json:"etf_holders,omitempty"`
	// CreatedAt holds the value of the "created_at" field.
	CreatedAt time.Time `json:"created_at,omitempty"`
}

// scanValues returns the types for scanning values from sql.Rows.
func (*LinstockStocksEtfholder) scanValues(columns []string) ([]interface{}, error) {
	values := make([]interface{}, len(columns))
	for i := range columns {
		switch columns[i] {
		case linstockstocksetfholder.FieldID, linstockstocksetfholder.FieldEtfHolders:
			values[i] = new(sql.NullString)
		case linstockstocksetfholder.FieldCreatedAt:
			values[i] = new(sql.NullTime)
		default:
			return nil, fmt.Errorf("unexpected column %q for type LinstockStocksEtfholder", columns[i])
		}
	}
	return values, nil
}

// assignValues assigns the values that were returned from sql.Rows (after scanning)
// to the LinstockStocksEtfholder fields.
func (lse *LinstockStocksEtfholder) assignValues(columns []string, values []interface{}) error {
	if m, n := len(values), len(columns); m < n {
		return fmt.Errorf("mismatch number of scan values: %d != %d", m, n)
	}
	for i := range columns {
		switch columns[i] {
		case linstockstocksetfholder.FieldID:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field id", values[i])
			} else if value.Valid {
				lse.ID = value.String
			}
		case linstockstocksetfholder.FieldEtfHolders:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field etf_holders", values[i])
			} else if value.Valid {
				lse.EtfHolders = value.String
			}
		case linstockstocksetfholder.FieldCreatedAt:
			if value, ok := values[i].(*sql.NullTime); !ok {
				return fmt.Errorf("unexpected type %T for field created_at", values[i])
			} else if value.Valid {
				lse.CreatedAt = value.Time
			}
		}
	}
	return nil
}

// Update returns a builder for updating this LinstockStocksEtfholder.
// Note that you need to call LinstockStocksEtfholder.Unwrap() before calling this method if this LinstockStocksEtfholder
// was returned from a transaction, and the transaction was committed or rolled back.
func (lse *LinstockStocksEtfholder) Update() *LinstockStocksEtfholderUpdateOne {
	return (&LinstockStocksEtfholderClient{config: lse.config}).UpdateOne(lse)
}

// Unwrap unwraps the LinstockStocksEtfholder entity that was returned from a transaction after it was closed,
// so that all future queries will be executed through the driver which created the transaction.
func (lse *LinstockStocksEtfholder) Unwrap() *LinstockStocksEtfholder {
	_tx, ok := lse.config.driver.(*txDriver)
	if !ok {
		panic("ent: LinstockStocksEtfholder is not a transactional entity")
	}
	lse.config.driver = _tx.drv
	return lse
}

// String implements the fmt.Stringer.
func (lse *LinstockStocksEtfholder) String() string {
	var builder strings.Builder
	builder.WriteString("LinstockStocksEtfholder(")
	builder.WriteString(fmt.Sprintf("id=%v, ", lse.ID))
	builder.WriteString("etf_holders=")
	builder.WriteString(lse.EtfHolders)
	builder.WriteString(", ")
	builder.WriteString("created_at=")
	builder.WriteString(lse.CreatedAt.Format(time.ANSIC))
	builder.WriteByte(')')
	return builder.String()
}

// LinstockStocksEtfholders is a parsable slice of LinstockStocksEtfholder.
type LinstockStocksEtfholders []*LinstockStocksEtfholder

func (lse LinstockStocksEtfholders) config(cfg config) {
	for _i := range lse {
		lse[_i].config = cfg
	}
}
