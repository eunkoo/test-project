// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"ezar-b2b-api/ent/linstockstocksetfprofit3mrank"
	"ezar-b2b-api/ent/predicate"
	"fmt"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// LinstockStocksEtfprofit3mrankDelete is the builder for deleting a LinstockStocksEtfprofit3mrank entity.
type LinstockStocksEtfprofit3mrankDelete struct {
	config
	hooks    []Hook
	mutation *LinstockStocksEtfprofit3mrankMutation
}

// Where appends a list predicates to the LinstockStocksEtfprofit3mrankDelete builder.
func (lsed *LinstockStocksEtfprofit3mrankDelete) Where(ps ...predicate.LinstockStocksEtfprofit3mrank) *LinstockStocksEtfprofit3mrankDelete {
	lsed.mutation.Where(ps...)
	return lsed
}

// Exec executes the deletion query and returns how many vertices were deleted.
func (lsed *LinstockStocksEtfprofit3mrankDelete) Exec(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(lsed.hooks) == 0 {
		affected, err = lsed.sqlExec(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*LinstockStocksEtfprofit3mrankMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			lsed.mutation = mutation
			affected, err = lsed.sqlExec(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(lsed.hooks) - 1; i >= 0; i-- {
			if lsed.hooks[i] == nil {
				return 0, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = lsed.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, lsed.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// ExecX is like Exec, but panics if an error occurs.
func (lsed *LinstockStocksEtfprofit3mrankDelete) ExecX(ctx context.Context) int {
	n, err := lsed.Exec(ctx)
	if err != nil {
		panic(err)
	}
	return n
}

func (lsed *LinstockStocksEtfprofit3mrankDelete) sqlExec(ctx context.Context) (int, error) {
	_spec := &sqlgraph.DeleteSpec{
		Node: &sqlgraph.NodeSpec{
			Table: linstockstocksetfprofit3mrank.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt32,
				Column: linstockstocksetfprofit3mrank.FieldID,
			},
		},
	}
	if ps := lsed.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	affected, err := sqlgraph.DeleteNodes(ctx, lsed.driver, _spec)
	if err != nil && sqlgraph.IsConstraintError(err) {
		err = &ConstraintError{msg: err.Error(), wrap: err}
	}
	return affected, err
}

// LinstockStocksEtfprofit3mrankDeleteOne is the builder for deleting a single LinstockStocksEtfprofit3mrank entity.
type LinstockStocksEtfprofit3mrankDeleteOne struct {
	lsed *LinstockStocksEtfprofit3mrankDelete
}

// Exec executes the deletion query.
func (lsedo *LinstockStocksEtfprofit3mrankDeleteOne) Exec(ctx context.Context) error {
	n, err := lsedo.lsed.Exec(ctx)
	switch {
	case err != nil:
		return err
	case n == 0:
		return &NotFoundError{linstockstocksetfprofit3mrank.Label}
	default:
		return nil
	}
}

// ExecX is like Exec, but panics if an error occurs.
func (lsedo *LinstockStocksEtfprofit3mrankDeleteOne) ExecX(ctx context.Context) {
	lsedo.lsed.ExecX(ctx)
}
