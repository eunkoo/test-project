// Code generated by ent, DO NOT EDIT.

package linstockstockssymbol

const (
	// Label holds the string label denoting the linstockstockssymbol type in the database.
	Label = "linstock_stocks_symbol"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldSymbol holds the string denoting the symbol field in the database.
	FieldSymbol = "symbol"
	// FieldName holds the string denoting the name field in the database.
	FieldName = "name"
	// FieldCreatedAt holds the string denoting the created_at field in the database.
	FieldCreatedAt = "created_at"
	// FieldUpdatedAt holds the string denoting the updated_at field in the database.
	FieldUpdatedAt = "updated_at"
	// FieldDeletedAt holds the string denoting the deleted_at field in the database.
	FieldDeletedAt = "deleted_at"
	// FieldLogoImg holds the string denoting the logo_img field in the database.
	FieldLogoImg = "logo_img"
	// FieldOrderRank holds the string denoting the order_rank field in the database.
	FieldOrderRank = "order_rank"
	// FieldTag holds the string denoting the tag field in the database.
	FieldTag = "tag"
	// FieldDescriptionKo holds the string denoting the description_ko field in the database.
	FieldDescriptionKo = "description_ko"
	// FieldCusip holds the string denoting the cusip field in the database.
	FieldCusip = "cusip"
	// FieldIsin holds the string denoting the isin field in the database.
	FieldIsin = "isin"
	// FieldNameKo holds the string denoting the name_ko field in the database.
	FieldNameKo = "name_ko"
	// FieldViews holds the string denoting the views field in the database.
	FieldViews = "views"
	// FieldDomicile holds the string denoting the domicile field in the database.
	FieldDomicile = "domicile"
	// FieldChangePercent holds the string denoting the change_percent field in the database.
	FieldChangePercent = "change_percent"
	// FieldPrice holds the string denoting the price field in the database.
	FieldPrice = "price"
	// FieldPreviousPrice holds the string denoting the previous_price field in the database.
	FieldPreviousPrice = "previous_price"
	// FieldClose holds the string denoting the close field in the database.
	FieldClose = "close"
	// FieldExChangePercent holds the string denoting the ex_change_percent field in the database.
	FieldExChangePercent = "ex_change_percent"
	// FieldExPrice holds the string denoting the ex_price field in the database.
	FieldExPrice = "ex_price"
	// FieldExchange holds the string denoting the exchange field in the database.
	FieldExchange = "exchange"
	// FieldSecType holds the string denoting the sec_type field in the database.
	FieldSecType = "sec_type"
	// FieldStatus holds the string denoting the status field in the database.
	FieldStatus = "status"
	// FieldLegalType holds the string denoting the legal_type field in the database.
	FieldLegalType = "legal_type"
	// FieldNameKoShort holds the string denoting the name_ko_short field in the database.
	FieldNameKoShort = "name_ko_short"
	// FieldPriceSource holds the string denoting the price_source field in the database.
	FieldPriceSource = "price_source"
	// EdgeLinstockStocksEtfbigpriceranks holds the string denoting the linstock_stocks_etfbigpriceranks edge name in mutations.
	EdgeLinstockStocksEtfbigpriceranks = "linstock_stocks_etfbigpriceranks"
	// EdgeLinstockStocksEtfdetail holds the string denoting the linstock_stocks_etfdetail edge name in mutations.
	EdgeLinstockStocksEtfdetail = "linstock_stocks_etfdetail"
	// EdgeLinstockStocksEtfloss1mranks holds the string denoting the linstock_stocks_etfloss1mranks edge name in mutations.
	EdgeLinstockStocksEtfloss1mranks = "linstock_stocks_etfloss1mranks"
	// EdgeLinstockStocksEtfloss1wranks holds the string denoting the linstock_stocks_etfloss1wranks edge name in mutations.
	EdgeLinstockStocksEtfloss1wranks = "linstock_stocks_etfloss1wranks"
	// EdgeLinstockStocksEtfloss1yranks holds the string denoting the linstock_stocks_etfloss1yranks edge name in mutations.
	EdgeLinstockStocksEtfloss1yranks = "linstock_stocks_etfloss1yranks"
	// EdgeLinstockStocksEtfloss3mranks holds the string denoting the linstock_stocks_etfloss3mranks edge name in mutations.
	EdgeLinstockStocksEtfloss3mranks = "linstock_stocks_etfloss3mranks"
	// EdgeLinstockStocksEtfloss5yranks holds the string denoting the linstock_stocks_etfloss5yranks edge name in mutations.
	EdgeLinstockStocksEtfloss5yranks = "linstock_stocks_etfloss5yranks"
	// EdgeLinstockStocksEtfloss6mranks holds the string denoting the linstock_stocks_etfloss6mranks edge name in mutations.
	EdgeLinstockStocksEtfloss6mranks = "linstock_stocks_etfloss6mranks"
	// EdgeLinstockStocksEtfmarketcapranks holds the string denoting the linstock_stocks_etfmarketcapranks edge name in mutations.
	EdgeLinstockStocksEtfmarketcapranks = "linstock_stocks_etfmarketcapranks"
	// EdgeLinstockStocksEtfnewranks holds the string denoting the linstock_stocks_etfnewranks edge name in mutations.
	EdgeLinstockStocksEtfnewranks = "linstock_stocks_etfnewranks"
	// EdgeLinstockStocksEtfprofit1mranks holds the string denoting the linstock_stocks_etfprofit1mranks edge name in mutations.
	EdgeLinstockStocksEtfprofit1mranks = "linstock_stocks_etfprofit1mranks"
	// EdgeLinstockStocksEtfprofit1wranks holds the string denoting the linstock_stocks_etfprofit1wranks edge name in mutations.
	EdgeLinstockStocksEtfprofit1wranks = "linstock_stocks_etfprofit1wranks"
	// EdgeLinstockStocksEtfprofit1yranks holds the string denoting the linstock_stocks_etfprofit1yranks edge name in mutations.
	EdgeLinstockStocksEtfprofit1yranks = "linstock_stocks_etfprofit1yranks"
	// EdgeLinstockStocksEtfprofit3mranks holds the string denoting the linstock_stocks_etfprofit3mranks edge name in mutations.
	EdgeLinstockStocksEtfprofit3mranks = "linstock_stocks_etfprofit3mranks"
	// EdgeLinstockStocksEtfprofit5yranks holds the string denoting the linstock_stocks_etfprofit5yranks edge name in mutations.
	EdgeLinstockStocksEtfprofit5yranks = "linstock_stocks_etfprofit5yranks"
	// EdgeLinstockStocksEtfprofit6mranks holds the string denoting the linstock_stocks_etfprofit6mranks edge name in mutations.
	EdgeLinstockStocksEtfprofit6mranks = "linstock_stocks_etfprofit6mranks"
	// EdgeLinstockStocksEtfsmallpriceranks holds the string denoting the linstock_stocks_etfsmallpriceranks edge name in mutations.
	EdgeLinstockStocksEtfsmallpriceranks = "linstock_stocks_etfsmallpriceranks"
	// EdgeLinstockStocksEtfvolume10dranks holds the string denoting the linstock_stocks_etfvolume10dranks edge name in mutations.
	EdgeLinstockStocksEtfvolume10dranks = "linstock_stocks_etfvolume10dranks"
	// EdgeLinstockStocksEtfvolume1dranks holds the string denoting the linstock_stocks_etfvolume1dranks edge name in mutations.
	EdgeLinstockStocksEtfvolume1dranks = "linstock_stocks_etfvolume1dranks"
	// EdgeLinstockStocksEtfvolume1mranks holds the string denoting the linstock_stocks_etfvolume1mranks edge name in mutations.
	EdgeLinstockStocksEtfvolume1mranks = "linstock_stocks_etfvolume1mranks"
	// EdgeLinstockStocksPubetfcategorySymbols holds the string denoting the linstock_stocks_pubetfcategory_symbols edge name in mutations.
	EdgeLinstockStocksPubetfcategorySymbols = "linstock_stocks_pubetfcategory_symbols"
	// EdgeLinstockStocksTop100popularsymbols holds the string denoting the linstock_stocks_top100popularsymbols edge name in mutations.
	EdgeLinstockStocksTop100popularsymbols = "linstock_stocks_top100popularsymbols"
	// Table holds the table name of the linstockstockssymbol in the database.
	Table = "linstock_stocks_symbol"
	// LinstockStocksEtfbigpriceranksTable is the table that holds the linstock_stocks_etfbigpriceranks relation/edge.
	LinstockStocksEtfbigpriceranksTable = "linstock_stocks_etfbigpricerank"
	// LinstockStocksEtfbigpriceranksInverseTable is the table name for the LinstockStocksEtfbigpricerank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfbigpricerank" package.
	LinstockStocksEtfbigpriceranksInverseTable = "linstock_stocks_etfbigpricerank"
	// LinstockStocksEtfbigpriceranksColumn is the table column denoting the linstock_stocks_etfbigpriceranks relation/edge.
	LinstockStocksEtfbigpriceranksColumn = "symbol_id"
	// LinstockStocksEtfdetailTable is the table that holds the linstock_stocks_etfdetail relation/edge.
	LinstockStocksEtfdetailTable = "linstock_stocks_etfdetail"
	// LinstockStocksEtfdetailInverseTable is the table name for the LinstockStocksEtfdetail entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfdetail" package.
	LinstockStocksEtfdetailInverseTable = "linstock_stocks_etfdetail"
	// LinstockStocksEtfdetailColumn is the table column denoting the linstock_stocks_etfdetail relation/edge.
	LinstockStocksEtfdetailColumn = "symbol_id"
	// LinstockStocksEtfloss1mranksTable is the table that holds the linstock_stocks_etfloss1mranks relation/edge.
	LinstockStocksEtfloss1mranksTable = "linstock_stocks_etfloss1mrank"
	// LinstockStocksEtfloss1mranksInverseTable is the table name for the LinstockStocksEtfloss1mrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfloss1mrank" package.
	LinstockStocksEtfloss1mranksInverseTable = "linstock_stocks_etfloss1mrank"
	// LinstockStocksEtfloss1mranksColumn is the table column denoting the linstock_stocks_etfloss1mranks relation/edge.
	LinstockStocksEtfloss1mranksColumn = "symbol_id"
	// LinstockStocksEtfloss1wranksTable is the table that holds the linstock_stocks_etfloss1wranks relation/edge.
	LinstockStocksEtfloss1wranksTable = "linstock_stocks_etfloss1wrank"
	// LinstockStocksEtfloss1wranksInverseTable is the table name for the LinstockStocksEtfloss1wrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfloss1wrank" package.
	LinstockStocksEtfloss1wranksInverseTable = "linstock_stocks_etfloss1wrank"
	// LinstockStocksEtfloss1wranksColumn is the table column denoting the linstock_stocks_etfloss1wranks relation/edge.
	LinstockStocksEtfloss1wranksColumn = "symbol_id"
	// LinstockStocksEtfloss1yranksTable is the table that holds the linstock_stocks_etfloss1yranks relation/edge.
	LinstockStocksEtfloss1yranksTable = "linstock_stocks_etfloss1yrank"
	// LinstockStocksEtfloss1yranksInverseTable is the table name for the LinstockStocksEtfloss1yrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfloss1yrank" package.
	LinstockStocksEtfloss1yranksInverseTable = "linstock_stocks_etfloss1yrank"
	// LinstockStocksEtfloss1yranksColumn is the table column denoting the linstock_stocks_etfloss1yranks relation/edge.
	LinstockStocksEtfloss1yranksColumn = "symbol_id"
	// LinstockStocksEtfloss3mranksTable is the table that holds the linstock_stocks_etfloss3mranks relation/edge.
	LinstockStocksEtfloss3mranksTable = "linstock_stocks_etfloss3mrank"
	// LinstockStocksEtfloss3mranksInverseTable is the table name for the LinstockStocksEtfloss3mrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfloss3mrank" package.
	LinstockStocksEtfloss3mranksInverseTable = "linstock_stocks_etfloss3mrank"
	// LinstockStocksEtfloss3mranksColumn is the table column denoting the linstock_stocks_etfloss3mranks relation/edge.
	LinstockStocksEtfloss3mranksColumn = "symbol_id"
	// LinstockStocksEtfloss5yranksTable is the table that holds the linstock_stocks_etfloss5yranks relation/edge.
	LinstockStocksEtfloss5yranksTable = "linstock_stocks_etfloss5yrank"
	// LinstockStocksEtfloss5yranksInverseTable is the table name for the LinstockStocksEtfloss5yrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfloss5yrank" package.
	LinstockStocksEtfloss5yranksInverseTable = "linstock_stocks_etfloss5yrank"
	// LinstockStocksEtfloss5yranksColumn is the table column denoting the linstock_stocks_etfloss5yranks relation/edge.
	LinstockStocksEtfloss5yranksColumn = "symbol_id"
	// LinstockStocksEtfloss6mranksTable is the table that holds the linstock_stocks_etfloss6mranks relation/edge.
	LinstockStocksEtfloss6mranksTable = "linstock_stocks_etfloss6mrank"
	// LinstockStocksEtfloss6mranksInverseTable is the table name for the LinstockStocksEtfloss6mrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfloss6mrank" package.
	LinstockStocksEtfloss6mranksInverseTable = "linstock_stocks_etfloss6mrank"
	// LinstockStocksEtfloss6mranksColumn is the table column denoting the linstock_stocks_etfloss6mranks relation/edge.
	LinstockStocksEtfloss6mranksColumn = "symbol_id"
	// LinstockStocksEtfmarketcapranksTable is the table that holds the linstock_stocks_etfmarketcapranks relation/edge.
	LinstockStocksEtfmarketcapranksTable = "linstock_stocks_etfmarketcaprank"
	// LinstockStocksEtfmarketcapranksInverseTable is the table name for the LinstockStocksEtfmarketcaprank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfmarketcaprank" package.
	LinstockStocksEtfmarketcapranksInverseTable = "linstock_stocks_etfmarketcaprank"
	// LinstockStocksEtfmarketcapranksColumn is the table column denoting the linstock_stocks_etfmarketcapranks relation/edge.
	LinstockStocksEtfmarketcapranksColumn = "symbol_id"
	// LinstockStocksEtfnewranksTable is the table that holds the linstock_stocks_etfnewranks relation/edge.
	LinstockStocksEtfnewranksTable = "linstock_stocks_etfnewrank"
	// LinstockStocksEtfnewranksInverseTable is the table name for the LinstockStocksEtfnewrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfnewrank" package.
	LinstockStocksEtfnewranksInverseTable = "linstock_stocks_etfnewrank"
	// LinstockStocksEtfnewranksColumn is the table column denoting the linstock_stocks_etfnewranks relation/edge.
	LinstockStocksEtfnewranksColumn = "symbol_id"
	// LinstockStocksEtfprofit1mranksTable is the table that holds the linstock_stocks_etfprofit1mranks relation/edge.
	LinstockStocksEtfprofit1mranksTable = "linstock_stocks_etfprofit1mrank"
	// LinstockStocksEtfprofit1mranksInverseTable is the table name for the LinstockStocksEtfprofit1mrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfprofit1mrank" package.
	LinstockStocksEtfprofit1mranksInverseTable = "linstock_stocks_etfprofit1mrank"
	// LinstockStocksEtfprofit1mranksColumn is the table column denoting the linstock_stocks_etfprofit1mranks relation/edge.
	LinstockStocksEtfprofit1mranksColumn = "symbol_id"
	// LinstockStocksEtfprofit1wranksTable is the table that holds the linstock_stocks_etfprofit1wranks relation/edge.
	LinstockStocksEtfprofit1wranksTable = "linstock_stocks_etfprofit1wrank"
	// LinstockStocksEtfprofit1wranksInverseTable is the table name for the LinstockStocksEtfprofit1wrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfprofit1wrank" package.
	LinstockStocksEtfprofit1wranksInverseTable = "linstock_stocks_etfprofit1wrank"
	// LinstockStocksEtfprofit1wranksColumn is the table column denoting the linstock_stocks_etfprofit1wranks relation/edge.
	LinstockStocksEtfprofit1wranksColumn = "symbol_id"
	// LinstockStocksEtfprofit1yranksTable is the table that holds the linstock_stocks_etfprofit1yranks relation/edge.
	LinstockStocksEtfprofit1yranksTable = "linstock_stocks_etfprofit1yrank"
	// LinstockStocksEtfprofit1yranksInverseTable is the table name for the LinstockStocksEtfprofit1yrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfprofit1yrank" package.
	LinstockStocksEtfprofit1yranksInverseTable = "linstock_stocks_etfprofit1yrank"
	// LinstockStocksEtfprofit1yranksColumn is the table column denoting the linstock_stocks_etfprofit1yranks relation/edge.
	LinstockStocksEtfprofit1yranksColumn = "symbol_id"
	// LinstockStocksEtfprofit3mranksTable is the table that holds the linstock_stocks_etfprofit3mranks relation/edge.
	LinstockStocksEtfprofit3mranksTable = "linstock_stocks_etfprofit3mrank"
	// LinstockStocksEtfprofit3mranksInverseTable is the table name for the LinstockStocksEtfprofit3mrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfprofit3mrank" package.
	LinstockStocksEtfprofit3mranksInverseTable = "linstock_stocks_etfprofit3mrank"
	// LinstockStocksEtfprofit3mranksColumn is the table column denoting the linstock_stocks_etfprofit3mranks relation/edge.
	LinstockStocksEtfprofit3mranksColumn = "symbol_id"
	// LinstockStocksEtfprofit5yranksTable is the table that holds the linstock_stocks_etfprofit5yranks relation/edge.
	LinstockStocksEtfprofit5yranksTable = "linstock_stocks_etfprofit5yrank"
	// LinstockStocksEtfprofit5yranksInverseTable is the table name for the LinstockStocksEtfprofit5yrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfprofit5yrank" package.
	LinstockStocksEtfprofit5yranksInverseTable = "linstock_stocks_etfprofit5yrank"
	// LinstockStocksEtfprofit5yranksColumn is the table column denoting the linstock_stocks_etfprofit5yranks relation/edge.
	LinstockStocksEtfprofit5yranksColumn = "symbol_id"
	// LinstockStocksEtfprofit6mranksTable is the table that holds the linstock_stocks_etfprofit6mranks relation/edge.
	LinstockStocksEtfprofit6mranksTable = "linstock_stocks_etfprofit6mrank"
	// LinstockStocksEtfprofit6mranksInverseTable is the table name for the LinstockStocksEtfprofit6mrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfprofit6mrank" package.
	LinstockStocksEtfprofit6mranksInverseTable = "linstock_stocks_etfprofit6mrank"
	// LinstockStocksEtfprofit6mranksColumn is the table column denoting the linstock_stocks_etfprofit6mranks relation/edge.
	LinstockStocksEtfprofit6mranksColumn = "symbol_id"
	// LinstockStocksEtfsmallpriceranksTable is the table that holds the linstock_stocks_etfsmallpriceranks relation/edge.
	LinstockStocksEtfsmallpriceranksTable = "linstock_stocks_etfsmallpricerank"
	// LinstockStocksEtfsmallpriceranksInverseTable is the table name for the LinstockStocksEtfsmallpricerank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfsmallpricerank" package.
	LinstockStocksEtfsmallpriceranksInverseTable = "linstock_stocks_etfsmallpricerank"
	// LinstockStocksEtfsmallpriceranksColumn is the table column denoting the linstock_stocks_etfsmallpriceranks relation/edge.
	LinstockStocksEtfsmallpriceranksColumn = "symbol_id"
	// LinstockStocksEtfvolume10dranksTable is the table that holds the linstock_stocks_etfvolume10dranks relation/edge.
	LinstockStocksEtfvolume10dranksTable = "linstock_stocks_etfvolume10drank"
	// LinstockStocksEtfvolume10dranksInverseTable is the table name for the LinstockStocksEtfvolume10drank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfvolume10drank" package.
	LinstockStocksEtfvolume10dranksInverseTable = "linstock_stocks_etfvolume10drank"
	// LinstockStocksEtfvolume10dranksColumn is the table column denoting the linstock_stocks_etfvolume10dranks relation/edge.
	LinstockStocksEtfvolume10dranksColumn = "symbol_id"
	// LinstockStocksEtfvolume1dranksTable is the table that holds the linstock_stocks_etfvolume1dranks relation/edge.
	LinstockStocksEtfvolume1dranksTable = "linstock_stocks_etfvolume1drank"
	// LinstockStocksEtfvolume1dranksInverseTable is the table name for the LinstockStocksEtfvolume1drank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfvolume1drank" package.
	LinstockStocksEtfvolume1dranksInverseTable = "linstock_stocks_etfvolume1drank"
	// LinstockStocksEtfvolume1dranksColumn is the table column denoting the linstock_stocks_etfvolume1dranks relation/edge.
	LinstockStocksEtfvolume1dranksColumn = "symbol_id"
	// LinstockStocksEtfvolume1mranksTable is the table that holds the linstock_stocks_etfvolume1mranks relation/edge.
	LinstockStocksEtfvolume1mranksTable = "linstock_stocks_etfvolume1mrank"
	// LinstockStocksEtfvolume1mranksInverseTable is the table name for the LinstockStocksEtfvolume1mrank entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstocksetfvolume1mrank" package.
	LinstockStocksEtfvolume1mranksInverseTable = "linstock_stocks_etfvolume1mrank"
	// LinstockStocksEtfvolume1mranksColumn is the table column denoting the linstock_stocks_etfvolume1mranks relation/edge.
	LinstockStocksEtfvolume1mranksColumn = "symbol_id"
	// LinstockStocksPubetfcategorySymbolsTable is the table that holds the linstock_stocks_pubetfcategory_symbols relation/edge.
	LinstockStocksPubetfcategorySymbolsTable = "linstock_stocks_pubetfcategory_symbol"
	// LinstockStocksPubetfcategorySymbolsInverseTable is the table name for the LinstockStocksPubetfcategorySymbol entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstockspubetfcategorysymbol" package.
	LinstockStocksPubetfcategorySymbolsInverseTable = "linstock_stocks_pubetfcategory_symbol"
	// LinstockStocksPubetfcategorySymbolsColumn is the table column denoting the linstock_stocks_pubetfcategory_symbols relation/edge.
	LinstockStocksPubetfcategorySymbolsColumn = "symbol_id"
	// LinstockStocksTop100popularsymbolsTable is the table that holds the linstock_stocks_top100popularsymbols relation/edge.
	LinstockStocksTop100popularsymbolsTable = "linstock_stocks_top100popularsymbols"
	// LinstockStocksTop100popularsymbolsInverseTable is the table name for the LinstockStocksTop100popularsymbol entity.
	// It exists in this package in order to avoid circular dependency with the "linstockstockstop100popularsymbol" package.
	LinstockStocksTop100popularsymbolsInverseTable = "linstock_stocks_top100popularsymbols"
	// LinstockStocksTop100popularsymbolsColumn is the table column denoting the linstock_stocks_top100popularsymbols relation/edge.
	LinstockStocksTop100popularsymbolsColumn = "symbol_id"
)

// Columns holds all SQL columns for linstockstockssymbol fields.
var Columns = []string{
	FieldID,
	FieldSymbol,
	FieldName,
	FieldCreatedAt,
	FieldUpdatedAt,
	FieldDeletedAt,
	FieldLogoImg,
	FieldOrderRank,
	FieldTag,
	FieldDescriptionKo,
	FieldCusip,
	FieldIsin,
	FieldNameKo,
	FieldViews,
	FieldDomicile,
	FieldChangePercent,
	FieldPrice,
	FieldPreviousPrice,
	FieldClose,
	FieldExChangePercent,
	FieldExPrice,
	FieldExchange,
	FieldSecType,
	FieldStatus,
	FieldLegalType,
	FieldNameKoShort,
	FieldPriceSource,
}

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	return false
}
