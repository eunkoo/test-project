// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"ezar-b2b-api/ent/linstockstockssp500"
	"ezar-b2b-api/ent/predicate"
	"fmt"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// LinstockStocksSp500Delete is the builder for deleting a LinstockStocksSp500 entity.
type LinstockStocksSp500Delete struct {
	config
	hooks    []Hook
	mutation *LinstockStocksSp500Mutation
}

// Where appends a list predicates to the LinstockStocksSp500Delete builder.
func (lss *LinstockStocksSp500Delete) Where(ps ...predicate.LinstockStocksSp500) *LinstockStocksSp500Delete {
	lss.mutation.Where(ps...)
	return lss
}

// Exec executes the deletion query and returns how many vertices were deleted.
func (lss *LinstockStocksSp500Delete) Exec(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(lss.hooks) == 0 {
		affected, err = lss.sqlExec(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*LinstockStocksSp500Mutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			lss.mutation = mutation
			affected, err = lss.sqlExec(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(lss.hooks) - 1; i >= 0; i-- {
			if lss.hooks[i] == nil {
				return 0, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = lss.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, lss.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// ExecX is like Exec, but panics if an error occurs.
func (lss *LinstockStocksSp500Delete) ExecX(ctx context.Context) int {
	n, err := lss.Exec(ctx)
	if err != nil {
		panic(err)
	}
	return n
}

func (lss *LinstockStocksSp500Delete) sqlExec(ctx context.Context) (int, error) {
	_spec := &sqlgraph.DeleteSpec{
		Node: &sqlgraph.NodeSpec{
			Table: linstockstockssp500.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeString,
				Column: linstockstockssp500.FieldID,
			},
		},
	}
	if ps := lss.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	affected, err := sqlgraph.DeleteNodes(ctx, lss.driver, _spec)
	if err != nil && sqlgraph.IsConstraintError(err) {
		err = &ConstraintError{msg: err.Error(), wrap: err}
	}
	return affected, err
}

// LinstockStocksSp500DeleteOne is the builder for deleting a single LinstockStocksSp500 entity.
type LinstockStocksSp500DeleteOne struct {
	lss *LinstockStocksSp500Delete
}

// Exec executes the deletion query.
func (lsso *LinstockStocksSp500DeleteOne) Exec(ctx context.Context) error {
	n, err := lsso.lss.Exec(ctx)
	switch {
	case err != nil:
		return err
	case n == 0:
		return &NotFoundError{linstockstockssp500.Label}
	default:
		return nil
	}
}

// ExecX is like Exec, but panics if an error occurs.
func (lsso *LinstockStocksSp500DeleteOne) ExecX(ctx context.Context) {
	lsso.lss.ExecX(ctx)
}
