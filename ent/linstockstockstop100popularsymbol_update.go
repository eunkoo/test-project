// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"ezar-b2b-api/ent/linstockstockssymbol"
	"ezar-b2b-api/ent/linstockstockstop100popularsymbol"
	"ezar-b2b-api/ent/predicate"
	"fmt"
	"time"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// LinstockStocksTop100popularsymbolUpdate is the builder for updating LinstockStocksTop100popularsymbol entities.
type LinstockStocksTop100popularsymbolUpdate struct {
	config
	hooks    []Hook
	mutation *LinstockStocksTop100popularsymbolMutation
}

// Where appends a list predicates to the LinstockStocksTop100popularsymbolUpdate builder.
func (lstu *LinstockStocksTop100popularsymbolUpdate) Where(ps ...predicate.LinstockStocksTop100popularsymbol) *LinstockStocksTop100popularsymbolUpdate {
	lstu.mutation.Where(ps...)
	return lstu
}

// SetSymbolID sets the "symbol_id" field.
func (lstu *LinstockStocksTop100popularsymbolUpdate) SetSymbolID(i int32) *LinstockStocksTop100popularsymbolUpdate {
	lstu.mutation.SetSymbolID(i)
	return lstu
}

// SetNillableSymbolID sets the "symbol_id" field if the given value is not nil.
func (lstu *LinstockStocksTop100popularsymbolUpdate) SetNillableSymbolID(i *int32) *LinstockStocksTop100popularsymbolUpdate {
	if i != nil {
		lstu.SetSymbolID(*i)
	}
	return lstu
}

// ClearSymbolID clears the value of the "symbol_id" field.
func (lstu *LinstockStocksTop100popularsymbolUpdate) ClearSymbolID() *LinstockStocksTop100popularsymbolUpdate {
	lstu.mutation.ClearSymbolID()
	return lstu
}

// SetUpdatedAt sets the "updated_at" field.
func (lstu *LinstockStocksTop100popularsymbolUpdate) SetUpdatedAt(t time.Time) *LinstockStocksTop100popularsymbolUpdate {
	lstu.mutation.SetUpdatedAt(t)
	return lstu
}

// SetNillableUpdatedAt sets the "updated_at" field if the given value is not nil.
func (lstu *LinstockStocksTop100popularsymbolUpdate) SetNillableUpdatedAt(t *time.Time) *LinstockStocksTop100popularsymbolUpdate {
	if t != nil {
		lstu.SetUpdatedAt(*t)
	}
	return lstu
}

// ClearUpdatedAt clears the value of the "updated_at" field.
func (lstu *LinstockStocksTop100popularsymbolUpdate) ClearUpdatedAt() *LinstockStocksTop100popularsymbolUpdate {
	lstu.mutation.ClearUpdatedAt()
	return lstu
}

// SetLinstockStocksSymbolID sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity by ID.
func (lstu *LinstockStocksTop100popularsymbolUpdate) SetLinstockStocksSymbolID(id int32) *LinstockStocksTop100popularsymbolUpdate {
	lstu.mutation.SetLinstockStocksSymbolID(id)
	return lstu
}

// SetNillableLinstockStocksSymbolID sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity by ID if the given value is not nil.
func (lstu *LinstockStocksTop100popularsymbolUpdate) SetNillableLinstockStocksSymbolID(id *int32) *LinstockStocksTop100popularsymbolUpdate {
	if id != nil {
		lstu = lstu.SetLinstockStocksSymbolID(*id)
	}
	return lstu
}

// SetLinstockStocksSymbol sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity.
func (lstu *LinstockStocksTop100popularsymbolUpdate) SetLinstockStocksSymbol(l *LinstockStocksSymbol) *LinstockStocksTop100popularsymbolUpdate {
	return lstu.SetLinstockStocksSymbolID(l.ID)
}

// Mutation returns the LinstockStocksTop100popularsymbolMutation object of the builder.
func (lstu *LinstockStocksTop100popularsymbolUpdate) Mutation() *LinstockStocksTop100popularsymbolMutation {
	return lstu.mutation
}

// ClearLinstockStocksSymbol clears the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity.
func (lstu *LinstockStocksTop100popularsymbolUpdate) ClearLinstockStocksSymbol() *LinstockStocksTop100popularsymbolUpdate {
	lstu.mutation.ClearLinstockStocksSymbol()
	return lstu
}

// Save executes the query and returns the number of nodes affected by the update operation.
func (lstu *LinstockStocksTop100popularsymbolUpdate) Save(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(lstu.hooks) == 0 {
		affected, err = lstu.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*LinstockStocksTop100popularsymbolMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			lstu.mutation = mutation
			affected, err = lstu.sqlSave(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(lstu.hooks) - 1; i >= 0; i-- {
			if lstu.hooks[i] == nil {
				return 0, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = lstu.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, lstu.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// SaveX is like Save, but panics if an error occurs.
func (lstu *LinstockStocksTop100popularsymbolUpdate) SaveX(ctx context.Context) int {
	affected, err := lstu.Save(ctx)
	if err != nil {
		panic(err)
	}
	return affected
}

// Exec executes the query.
func (lstu *LinstockStocksTop100popularsymbolUpdate) Exec(ctx context.Context) error {
	_, err := lstu.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (lstu *LinstockStocksTop100popularsymbolUpdate) ExecX(ctx context.Context) {
	if err := lstu.Exec(ctx); err != nil {
		panic(err)
	}
}

func (lstu *LinstockStocksTop100popularsymbolUpdate) sqlSave(ctx context.Context) (n int, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   linstockstockstop100popularsymbol.Table,
			Columns: linstockstockstop100popularsymbol.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt32,
				Column: linstockstockstop100popularsymbol.FieldID,
			},
		},
	}
	if ps := lstu.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := lstu.mutation.UpdatedAt(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstockstop100popularsymbol.FieldUpdatedAt,
		})
	}
	if lstu.mutation.UpdatedAtCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Column: linstockstockstop100popularsymbol.FieldUpdatedAt,
		})
	}
	if lstu.mutation.LinstockStocksSymbolCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   linstockstockstop100popularsymbol.LinstockStocksSymbolTable,
			Columns: []string{linstockstockstop100popularsymbol.LinstockStocksSymbolColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt32,
					Column: linstockstockssymbol.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := lstu.mutation.LinstockStocksSymbolIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   linstockstockstop100popularsymbol.LinstockStocksSymbolTable,
			Columns: []string{linstockstockstop100popularsymbol.LinstockStocksSymbolColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt32,
					Column: linstockstockssymbol.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	if n, err = sqlgraph.UpdateNodes(ctx, lstu.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{linstockstockstop100popularsymbol.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return 0, err
	}
	return n, nil
}

// LinstockStocksTop100popularsymbolUpdateOne is the builder for updating a single LinstockStocksTop100popularsymbol entity.
type LinstockStocksTop100popularsymbolUpdateOne struct {
	config
	fields   []string
	hooks    []Hook
	mutation *LinstockStocksTop100popularsymbolMutation
}

// SetSymbolID sets the "symbol_id" field.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) SetSymbolID(i int32) *LinstockStocksTop100popularsymbolUpdateOne {
	lstuo.mutation.SetSymbolID(i)
	return lstuo
}

// SetNillableSymbolID sets the "symbol_id" field if the given value is not nil.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) SetNillableSymbolID(i *int32) *LinstockStocksTop100popularsymbolUpdateOne {
	if i != nil {
		lstuo.SetSymbolID(*i)
	}
	return lstuo
}

// ClearSymbolID clears the value of the "symbol_id" field.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) ClearSymbolID() *LinstockStocksTop100popularsymbolUpdateOne {
	lstuo.mutation.ClearSymbolID()
	return lstuo
}

// SetUpdatedAt sets the "updated_at" field.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) SetUpdatedAt(t time.Time) *LinstockStocksTop100popularsymbolUpdateOne {
	lstuo.mutation.SetUpdatedAt(t)
	return lstuo
}

// SetNillableUpdatedAt sets the "updated_at" field if the given value is not nil.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) SetNillableUpdatedAt(t *time.Time) *LinstockStocksTop100popularsymbolUpdateOne {
	if t != nil {
		lstuo.SetUpdatedAt(*t)
	}
	return lstuo
}

// ClearUpdatedAt clears the value of the "updated_at" field.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) ClearUpdatedAt() *LinstockStocksTop100popularsymbolUpdateOne {
	lstuo.mutation.ClearUpdatedAt()
	return lstuo
}

// SetLinstockStocksSymbolID sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity by ID.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) SetLinstockStocksSymbolID(id int32) *LinstockStocksTop100popularsymbolUpdateOne {
	lstuo.mutation.SetLinstockStocksSymbolID(id)
	return lstuo
}

// SetNillableLinstockStocksSymbolID sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity by ID if the given value is not nil.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) SetNillableLinstockStocksSymbolID(id *int32) *LinstockStocksTop100popularsymbolUpdateOne {
	if id != nil {
		lstuo = lstuo.SetLinstockStocksSymbolID(*id)
	}
	return lstuo
}

// SetLinstockStocksSymbol sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) SetLinstockStocksSymbol(l *LinstockStocksSymbol) *LinstockStocksTop100popularsymbolUpdateOne {
	return lstuo.SetLinstockStocksSymbolID(l.ID)
}

// Mutation returns the LinstockStocksTop100popularsymbolMutation object of the builder.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) Mutation() *LinstockStocksTop100popularsymbolMutation {
	return lstuo.mutation
}

// ClearLinstockStocksSymbol clears the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) ClearLinstockStocksSymbol() *LinstockStocksTop100popularsymbolUpdateOne {
	lstuo.mutation.ClearLinstockStocksSymbol()
	return lstuo
}

// Select allows selecting one or more fields (columns) of the returned entity.
// The default is selecting all fields defined in the entity schema.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) Select(field string, fields ...string) *LinstockStocksTop100popularsymbolUpdateOne {
	lstuo.fields = append([]string{field}, fields...)
	return lstuo
}

// Save executes the query and returns the updated LinstockStocksTop100popularsymbol entity.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) Save(ctx context.Context) (*LinstockStocksTop100popularsymbol, error) {
	var (
		err  error
		node *LinstockStocksTop100popularsymbol
	)
	if len(lstuo.hooks) == 0 {
		node, err = lstuo.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*LinstockStocksTop100popularsymbolMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			lstuo.mutation = mutation
			node, err = lstuo.sqlSave(ctx)
			mutation.done = true
			return node, err
		})
		for i := len(lstuo.hooks) - 1; i >= 0; i-- {
			if lstuo.hooks[i] == nil {
				return nil, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = lstuo.hooks[i](mut)
		}
		v, err := mut.Mutate(ctx, lstuo.mutation)
		if err != nil {
			return nil, err
		}
		nv, ok := v.(*LinstockStocksTop100popularsymbol)
		if !ok {
			return nil, fmt.Errorf("unexpected node type %T returned from LinstockStocksTop100popularsymbolMutation", v)
		}
		node = nv
	}
	return node, err
}

// SaveX is like Save, but panics if an error occurs.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) SaveX(ctx context.Context) *LinstockStocksTop100popularsymbol {
	node, err := lstuo.Save(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// Exec executes the query on the entity.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) Exec(ctx context.Context) error {
	_, err := lstuo.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) ExecX(ctx context.Context) {
	if err := lstuo.Exec(ctx); err != nil {
		panic(err)
	}
}

func (lstuo *LinstockStocksTop100popularsymbolUpdateOne) sqlSave(ctx context.Context) (_node *LinstockStocksTop100popularsymbol, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   linstockstockstop100popularsymbol.Table,
			Columns: linstockstockstop100popularsymbol.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt32,
				Column: linstockstockstop100popularsymbol.FieldID,
			},
		},
	}
	id, ok := lstuo.mutation.ID()
	if !ok {
		return nil, &ValidationError{Name: "id", err: errors.New(`ent: missing "LinstockStocksTop100popularsymbol.id" for update`)}
	}
	_spec.Node.ID.Value = id
	if fields := lstuo.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, linstockstockstop100popularsymbol.FieldID)
		for _, f := range fields {
			if !linstockstockstop100popularsymbol.ValidColumn(f) {
				return nil, &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
			}
			if f != linstockstockstop100popularsymbol.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, f)
			}
		}
	}
	if ps := lstuo.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := lstuo.mutation.UpdatedAt(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstockstop100popularsymbol.FieldUpdatedAt,
		})
	}
	if lstuo.mutation.UpdatedAtCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Column: linstockstockstop100popularsymbol.FieldUpdatedAt,
		})
	}
	if lstuo.mutation.LinstockStocksSymbolCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   linstockstockstop100popularsymbol.LinstockStocksSymbolTable,
			Columns: []string{linstockstockstop100popularsymbol.LinstockStocksSymbolColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt32,
					Column: linstockstockssymbol.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := lstuo.mutation.LinstockStocksSymbolIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   linstockstockstop100popularsymbol.LinstockStocksSymbolTable,
			Columns: []string{linstockstockstop100popularsymbol.LinstockStocksSymbolColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt32,
					Column: linstockstockssymbol.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	_node = &LinstockStocksTop100popularsymbol{config: lstuo.config}
	_spec.Assign = _node.assignValues
	_spec.ScanValues = _node.scanValues
	if err = sqlgraph.UpdateNode(ctx, lstuo.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{linstockstockstop100popularsymbol.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	return _node, nil
}
