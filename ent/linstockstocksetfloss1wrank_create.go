// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"ezar-b2b-api/ent/linstockstocksetfloss1wrank"
	"ezar-b2b-api/ent/linstockstockssymbol"
	"fmt"
	"time"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// LinstockStocksEtfloss1wrankCreate is the builder for creating a LinstockStocksEtfloss1wrank entity.
type LinstockStocksEtfloss1wrankCreate struct {
	config
	mutation *LinstockStocksEtfloss1wrankMutation
	hooks    []Hook
}

// SetRank sets the "rank" field.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetRank(i int32) *LinstockStocksEtfloss1wrankCreate {
	lsec.mutation.SetRank(i)
	return lsec
}

// SetCreatedAt sets the "created_at" field.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetCreatedAt(t time.Time) *LinstockStocksEtfloss1wrankCreate {
	lsec.mutation.SetCreatedAt(t)
	return lsec
}

// SetNillableCreatedAt sets the "created_at" field if the given value is not nil.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetNillableCreatedAt(t *time.Time) *LinstockStocksEtfloss1wrankCreate {
	if t != nil {
		lsec.SetCreatedAt(*t)
	}
	return lsec
}

// SetUpdatedAt sets the "updated_at" field.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetUpdatedAt(t time.Time) *LinstockStocksEtfloss1wrankCreate {
	lsec.mutation.SetUpdatedAt(t)
	return lsec
}

// SetNillableUpdatedAt sets the "updated_at" field if the given value is not nil.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetNillableUpdatedAt(t *time.Time) *LinstockStocksEtfloss1wrankCreate {
	if t != nil {
		lsec.SetUpdatedAt(*t)
	}
	return lsec
}

// SetDeletedAt sets the "deleted_at" field.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetDeletedAt(t time.Time) *LinstockStocksEtfloss1wrankCreate {
	lsec.mutation.SetDeletedAt(t)
	return lsec
}

// SetNillableDeletedAt sets the "deleted_at" field if the given value is not nil.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetNillableDeletedAt(t *time.Time) *LinstockStocksEtfloss1wrankCreate {
	if t != nil {
		lsec.SetDeletedAt(*t)
	}
	return lsec
}

// SetValue sets the "value" field.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetValue(f float64) *LinstockStocksEtfloss1wrankCreate {
	lsec.mutation.SetValue(f)
	return lsec
}

// SetSymbolID sets the "symbol_id" field.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetSymbolID(i int32) *LinstockStocksEtfloss1wrankCreate {
	lsec.mutation.SetSymbolID(i)
	return lsec
}

// SetNillableSymbolID sets the "symbol_id" field if the given value is not nil.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetNillableSymbolID(i *int32) *LinstockStocksEtfloss1wrankCreate {
	if i != nil {
		lsec.SetSymbolID(*i)
	}
	return lsec
}

// SetID sets the "id" field.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetID(i int32) *LinstockStocksEtfloss1wrankCreate {
	lsec.mutation.SetID(i)
	return lsec
}

// SetLinstockStocksSymbolID sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity by ID.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetLinstockStocksSymbolID(id int32) *LinstockStocksEtfloss1wrankCreate {
	lsec.mutation.SetLinstockStocksSymbolID(id)
	return lsec
}

// SetNillableLinstockStocksSymbolID sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity by ID if the given value is not nil.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetNillableLinstockStocksSymbolID(id *int32) *LinstockStocksEtfloss1wrankCreate {
	if id != nil {
		lsec = lsec.SetLinstockStocksSymbolID(*id)
	}
	return lsec
}

// SetLinstockStocksSymbol sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity.
func (lsec *LinstockStocksEtfloss1wrankCreate) SetLinstockStocksSymbol(l *LinstockStocksSymbol) *LinstockStocksEtfloss1wrankCreate {
	return lsec.SetLinstockStocksSymbolID(l.ID)
}

// Mutation returns the LinstockStocksEtfloss1wrankMutation object of the builder.
func (lsec *LinstockStocksEtfloss1wrankCreate) Mutation() *LinstockStocksEtfloss1wrankMutation {
	return lsec.mutation
}

// Save creates the LinstockStocksEtfloss1wrank in the database.
func (lsec *LinstockStocksEtfloss1wrankCreate) Save(ctx context.Context) (*LinstockStocksEtfloss1wrank, error) {
	var (
		err  error
		node *LinstockStocksEtfloss1wrank
	)
	if len(lsec.hooks) == 0 {
		if err = lsec.check(); err != nil {
			return nil, err
		}
		node, err = lsec.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*LinstockStocksEtfloss1wrankMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = lsec.check(); err != nil {
				return nil, err
			}
			lsec.mutation = mutation
			if node, err = lsec.sqlSave(ctx); err != nil {
				return nil, err
			}
			mutation.id = &node.ID
			mutation.done = true
			return node, err
		})
		for i := len(lsec.hooks) - 1; i >= 0; i-- {
			if lsec.hooks[i] == nil {
				return nil, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = lsec.hooks[i](mut)
		}
		v, err := mut.Mutate(ctx, lsec.mutation)
		if err != nil {
			return nil, err
		}
		nv, ok := v.(*LinstockStocksEtfloss1wrank)
		if !ok {
			return nil, fmt.Errorf("unexpected node type %T returned from LinstockStocksEtfloss1wrankMutation", v)
		}
		node = nv
	}
	return node, err
}

// SaveX calls Save and panics if Save returns an error.
func (lsec *LinstockStocksEtfloss1wrankCreate) SaveX(ctx context.Context) *LinstockStocksEtfloss1wrank {
	v, err := lsec.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (lsec *LinstockStocksEtfloss1wrankCreate) Exec(ctx context.Context) error {
	_, err := lsec.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (lsec *LinstockStocksEtfloss1wrankCreate) ExecX(ctx context.Context) {
	if err := lsec.Exec(ctx); err != nil {
		panic(err)
	}
}

// check runs all checks and user-defined validators on the builder.
func (lsec *LinstockStocksEtfloss1wrankCreate) check() error {
	if _, ok := lsec.mutation.Rank(); !ok {
		return &ValidationError{Name: "rank", err: errors.New(`ent: missing required field "LinstockStocksEtfloss1wrank.rank"`)}
	}
	if _, ok := lsec.mutation.Value(); !ok {
		return &ValidationError{Name: "value", err: errors.New(`ent: missing required field "LinstockStocksEtfloss1wrank.value"`)}
	}
	return nil
}

func (lsec *LinstockStocksEtfloss1wrankCreate) sqlSave(ctx context.Context) (*LinstockStocksEtfloss1wrank, error) {
	_node, _spec := lsec.createSpec()
	if err := sqlgraph.CreateNode(ctx, lsec.driver, _spec); err != nil {
		if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	if _spec.ID.Value != _node.ID {
		id := _spec.ID.Value.(int64)
		_node.ID = int32(id)
	}
	return _node, nil
}

func (lsec *LinstockStocksEtfloss1wrankCreate) createSpec() (*LinstockStocksEtfloss1wrank, *sqlgraph.CreateSpec) {
	var (
		_node = &LinstockStocksEtfloss1wrank{config: lsec.config}
		_spec = &sqlgraph.CreateSpec{
			Table: linstockstocksetfloss1wrank.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt32,
				Column: linstockstocksetfloss1wrank.FieldID,
			},
		}
	)
	if id, ok := lsec.mutation.ID(); ok {
		_node.ID = id
		_spec.ID.Value = id
	}
	if value, ok := lsec.mutation.Rank(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeInt32,
			Value:  value,
			Column: linstockstocksetfloss1wrank.FieldRank,
		})
		_node.Rank = value
	}
	if value, ok := lsec.mutation.CreatedAt(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksetfloss1wrank.FieldCreatedAt,
		})
		_node.CreatedAt = value
	}
	if value, ok := lsec.mutation.UpdatedAt(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksetfloss1wrank.FieldUpdatedAt,
		})
		_node.UpdatedAt = value
	}
	if value, ok := lsec.mutation.DeletedAt(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksetfloss1wrank.FieldDeletedAt,
		})
		_node.DeletedAt = value
	}
	if value, ok := lsec.mutation.Value(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeFloat64,
			Value:  value,
			Column: linstockstocksetfloss1wrank.FieldValue,
		})
		_node.Value = value
	}
	if nodes := lsec.mutation.LinstockStocksSymbolIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   linstockstocksetfloss1wrank.LinstockStocksSymbolTable,
			Columns: []string{linstockstocksetfloss1wrank.LinstockStocksSymbolColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt32,
					Column: linstockstockssymbol.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_node.SymbolID = nodes[0]
		_spec.Edges = append(_spec.Edges, edge)
	}
	return _node, _spec
}

// LinstockStocksEtfloss1wrankCreateBulk is the builder for creating many LinstockStocksEtfloss1wrank entities in bulk.
type LinstockStocksEtfloss1wrankCreateBulk struct {
	config
	builders []*LinstockStocksEtfloss1wrankCreate
}

// Save creates the LinstockStocksEtfloss1wrank entities in the database.
func (lsecb *LinstockStocksEtfloss1wrankCreateBulk) Save(ctx context.Context) ([]*LinstockStocksEtfloss1wrank, error) {
	specs := make([]*sqlgraph.CreateSpec, len(lsecb.builders))
	nodes := make([]*LinstockStocksEtfloss1wrank, len(lsecb.builders))
	mutators := make([]Mutator, len(lsecb.builders))
	for i := range lsecb.builders {
		func(i int, root context.Context) {
			builder := lsecb.builders[i]
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*LinstockStocksEtfloss1wrankMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				nodes[i], specs[i] = builder.createSpec()
				var err error
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, lsecb.builders[i+1].mutation)
				} else {
					spec := &sqlgraph.BatchCreateSpec{Nodes: specs}
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, lsecb.driver, spec); err != nil {
						if sqlgraph.IsConstraintError(err) {
							err = &ConstraintError{msg: err.Error(), wrap: err}
						}
					}
				}
				if err != nil {
					return nil, err
				}
				mutation.id = &nodes[i].ID
				if specs[i].ID.Value != nil && nodes[i].ID == 0 {
					id := specs[i].ID.Value.(int64)
					nodes[i].ID = int32(id)
				}
				mutation.done = true
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, lsecb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (lsecb *LinstockStocksEtfloss1wrankCreateBulk) SaveX(ctx context.Context) []*LinstockStocksEtfloss1wrank {
	v, err := lsecb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (lsecb *LinstockStocksEtfloss1wrankCreateBulk) Exec(ctx context.Context) error {
	_, err := lsecb.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (lsecb *LinstockStocksEtfloss1wrankCreateBulk) ExecX(ctx context.Context) {
	if err := lsecb.Exec(ctx); err != nil {
		panic(err)
	}
}
