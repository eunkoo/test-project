// Code generated by ent, DO NOT EDIT.

package linstockstockssymboldailyprice

import (
	"ezar-b2b-api/ent/predicate"
	"time"

	"entgo.io/ent/dialect/sql"
)

// ID filters vertices based on their ID field.
func ID(id int32) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDEQ applies the EQ predicate on the ID field.
func IDEQ(id int32) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDNEQ applies the NEQ predicate on the ID field.
func IDNEQ(id int32) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldID), id))
	})
}

// IDIn applies the In predicate on the ID field.
func IDIn(ids ...int32) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		v := make([]interface{}, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.In(s.C(FieldID), v...))
	})
}

// IDNotIn applies the NotIn predicate on the ID field.
func IDNotIn(ids ...int32) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		v := make([]interface{}, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.NotIn(s.C(FieldID), v...))
	})
}

// IDGT applies the GT predicate on the ID field.
func IDGT(id int32) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldID), id))
	})
}

// IDGTE applies the GTE predicate on the ID field.
func IDGTE(id int32) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldID), id))
	})
}

// IDLT applies the LT predicate on the ID field.
func IDLT(id int32) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldID), id))
	})
}

// IDLTE applies the LTE predicate on the ID field.
func IDLTE(id int32) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldID), id))
	})
}

// Symbol applies equality check predicate on the "symbol" field. It's identical to SymbolEQ.
func Symbol(v string) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldSymbol), v))
	})
}

// Price applies equality check predicate on the "price" field. It's identical to PriceEQ.
func Price(v float64) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldPrice), v))
	})
}

// CreatedAt applies equality check predicate on the "created_at" field. It's identical to CreatedAtEQ.
func CreatedAt(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldCreatedAt), v))
	})
}

// TradeAt applies equality check predicate on the "trade_at" field. It's identical to TradeAtEQ.
func TradeAt(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldTradeAt), v))
	})
}

// SymbolEQ applies the EQ predicate on the "symbol" field.
func SymbolEQ(v string) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldSymbol), v))
	})
}

// SymbolNEQ applies the NEQ predicate on the "symbol" field.
func SymbolNEQ(v string) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldSymbol), v))
	})
}

// SymbolIn applies the In predicate on the "symbol" field.
func SymbolIn(vs ...string) predicate.LinstockStocksSymboldailyprice {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldSymbol), v...))
	})
}

// SymbolNotIn applies the NotIn predicate on the "symbol" field.
func SymbolNotIn(vs ...string) predicate.LinstockStocksSymboldailyprice {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldSymbol), v...))
	})
}

// SymbolGT applies the GT predicate on the "symbol" field.
func SymbolGT(v string) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldSymbol), v))
	})
}

// SymbolGTE applies the GTE predicate on the "symbol" field.
func SymbolGTE(v string) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldSymbol), v))
	})
}

// SymbolLT applies the LT predicate on the "symbol" field.
func SymbolLT(v string) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldSymbol), v))
	})
}

// SymbolLTE applies the LTE predicate on the "symbol" field.
func SymbolLTE(v string) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldSymbol), v))
	})
}

// SymbolContains applies the Contains predicate on the "symbol" field.
func SymbolContains(v string) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldSymbol), v))
	})
}

// SymbolHasPrefix applies the HasPrefix predicate on the "symbol" field.
func SymbolHasPrefix(v string) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldSymbol), v))
	})
}

// SymbolHasSuffix applies the HasSuffix predicate on the "symbol" field.
func SymbolHasSuffix(v string) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldSymbol), v))
	})
}

// SymbolEqualFold applies the EqualFold predicate on the "symbol" field.
func SymbolEqualFold(v string) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldSymbol), v))
	})
}

// SymbolContainsFold applies the ContainsFold predicate on the "symbol" field.
func SymbolContainsFold(v string) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldSymbol), v))
	})
}

// PriceEQ applies the EQ predicate on the "price" field.
func PriceEQ(v float64) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldPrice), v))
	})
}

// PriceNEQ applies the NEQ predicate on the "price" field.
func PriceNEQ(v float64) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldPrice), v))
	})
}

// PriceIn applies the In predicate on the "price" field.
func PriceIn(vs ...float64) predicate.LinstockStocksSymboldailyprice {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldPrice), v...))
	})
}

// PriceNotIn applies the NotIn predicate on the "price" field.
func PriceNotIn(vs ...float64) predicate.LinstockStocksSymboldailyprice {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldPrice), v...))
	})
}

// PriceGT applies the GT predicate on the "price" field.
func PriceGT(v float64) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldPrice), v))
	})
}

// PriceGTE applies the GTE predicate on the "price" field.
func PriceGTE(v float64) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldPrice), v))
	})
}

// PriceLT applies the LT predicate on the "price" field.
func PriceLT(v float64) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldPrice), v))
	})
}

// PriceLTE applies the LTE predicate on the "price" field.
func PriceLTE(v float64) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldPrice), v))
	})
}

// PriceIsNil applies the IsNil predicate on the "price" field.
func PriceIsNil() predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldPrice)))
	})
}

// PriceNotNil applies the NotNil predicate on the "price" field.
func PriceNotNil() predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldPrice)))
	})
}

// CreatedAtEQ applies the EQ predicate on the "created_at" field.
func CreatedAtEQ(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtNEQ applies the NEQ predicate on the "created_at" field.
func CreatedAtNEQ(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtIn applies the In predicate on the "created_at" field.
func CreatedAtIn(vs ...time.Time) predicate.LinstockStocksSymboldailyprice {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldCreatedAt), v...))
	})
}

// CreatedAtNotIn applies the NotIn predicate on the "created_at" field.
func CreatedAtNotIn(vs ...time.Time) predicate.LinstockStocksSymboldailyprice {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldCreatedAt), v...))
	})
}

// CreatedAtGT applies the GT predicate on the "created_at" field.
func CreatedAtGT(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtGTE applies the GTE predicate on the "created_at" field.
func CreatedAtGTE(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtLT applies the LT predicate on the "created_at" field.
func CreatedAtLT(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtLTE applies the LTE predicate on the "created_at" field.
func CreatedAtLTE(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldCreatedAt), v))
	})
}

// CreatedAtIsNil applies the IsNil predicate on the "created_at" field.
func CreatedAtIsNil() predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldCreatedAt)))
	})
}

// CreatedAtNotNil applies the NotNil predicate on the "created_at" field.
func CreatedAtNotNil() predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldCreatedAt)))
	})
}

// TradeAtEQ applies the EQ predicate on the "trade_at" field.
func TradeAtEQ(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldTradeAt), v))
	})
}

// TradeAtNEQ applies the NEQ predicate on the "trade_at" field.
func TradeAtNEQ(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldTradeAt), v))
	})
}

// TradeAtIn applies the In predicate on the "trade_at" field.
func TradeAtIn(vs ...time.Time) predicate.LinstockStocksSymboldailyprice {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldTradeAt), v...))
	})
}

// TradeAtNotIn applies the NotIn predicate on the "trade_at" field.
func TradeAtNotIn(vs ...time.Time) predicate.LinstockStocksSymboldailyprice {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldTradeAt), v...))
	})
}

// TradeAtGT applies the GT predicate on the "trade_at" field.
func TradeAtGT(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldTradeAt), v))
	})
}

// TradeAtGTE applies the GTE predicate on the "trade_at" field.
func TradeAtGTE(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldTradeAt), v))
	})
}

// TradeAtLT applies the LT predicate on the "trade_at" field.
func TradeAtLT(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldTradeAt), v))
	})
}

// TradeAtLTE applies the LTE predicate on the "trade_at" field.
func TradeAtLTE(v time.Time) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldTradeAt), v))
	})
}

// TradeAtIsNil applies the IsNil predicate on the "trade_at" field.
func TradeAtIsNil() predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldTradeAt)))
	})
}

// TradeAtNotNil applies the NotNil predicate on the "trade_at" field.
func TradeAtNotNil() predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldTradeAt)))
	})
}

// And groups predicates with the AND operator between them.
func And(predicates ...predicate.LinstockStocksSymboldailyprice) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for _, p := range predicates {
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Or groups predicates with the OR operator between them.
func Or(predicates ...predicate.LinstockStocksSymboldailyprice) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for i, p := range predicates {
			if i > 0 {
				s1.Or()
			}
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Not applies the not operator on the given predicate.
func Not(p predicate.LinstockStocksSymboldailyprice) predicate.LinstockStocksSymboldailyprice {
	return predicate.LinstockStocksSymboldailyprice(func(s *sql.Selector) {
		p(s.Not())
	})
}
