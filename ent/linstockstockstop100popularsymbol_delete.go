// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"ezar-b2b-api/ent/linstockstockstop100popularsymbol"
	"ezar-b2b-api/ent/predicate"
	"fmt"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// LinstockStocksTop100popularsymbolDelete is the builder for deleting a LinstockStocksTop100popularsymbol entity.
type LinstockStocksTop100popularsymbolDelete struct {
	config
	hooks    []Hook
	mutation *LinstockStocksTop100popularsymbolMutation
}

// Where appends a list predicates to the LinstockStocksTop100popularsymbolDelete builder.
func (lstd *LinstockStocksTop100popularsymbolDelete) Where(ps ...predicate.LinstockStocksTop100popularsymbol) *LinstockStocksTop100popularsymbolDelete {
	lstd.mutation.Where(ps...)
	return lstd
}

// Exec executes the deletion query and returns how many vertices were deleted.
func (lstd *LinstockStocksTop100popularsymbolDelete) Exec(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(lstd.hooks) == 0 {
		affected, err = lstd.sqlExec(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*LinstockStocksTop100popularsymbolMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			lstd.mutation = mutation
			affected, err = lstd.sqlExec(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(lstd.hooks) - 1; i >= 0; i-- {
			if lstd.hooks[i] == nil {
				return 0, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = lstd.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, lstd.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// ExecX is like Exec, but panics if an error occurs.
func (lstd *LinstockStocksTop100popularsymbolDelete) ExecX(ctx context.Context) int {
	n, err := lstd.Exec(ctx)
	if err != nil {
		panic(err)
	}
	return n
}

func (lstd *LinstockStocksTop100popularsymbolDelete) sqlExec(ctx context.Context) (int, error) {
	_spec := &sqlgraph.DeleteSpec{
		Node: &sqlgraph.NodeSpec{
			Table: linstockstockstop100popularsymbol.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt32,
				Column: linstockstockstop100popularsymbol.FieldID,
			},
		},
	}
	if ps := lstd.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	affected, err := sqlgraph.DeleteNodes(ctx, lstd.driver, _spec)
	if err != nil && sqlgraph.IsConstraintError(err) {
		err = &ConstraintError{msg: err.Error(), wrap: err}
	}
	return affected, err
}

// LinstockStocksTop100popularsymbolDeleteOne is the builder for deleting a single LinstockStocksTop100popularsymbol entity.
type LinstockStocksTop100popularsymbolDeleteOne struct {
	lstd *LinstockStocksTop100popularsymbolDelete
}

// Exec executes the deletion query.
func (lstdo *LinstockStocksTop100popularsymbolDeleteOne) Exec(ctx context.Context) error {
	n, err := lstdo.lstd.Exec(ctx)
	switch {
	case err != nil:
		return err
	case n == 0:
		return &NotFoundError{linstockstockstop100popularsymbol.Label}
	default:
		return nil
	}
}

// ExecX is like Exec, but panics if an error occurs.
func (lstdo *LinstockStocksTop100popularsymbolDeleteOne) ExecX(ctx context.Context) {
	lstdo.lstd.ExecX(ctx)
}
