// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"ezar-b2b-api/ent/linstockstocksetfvolume1mrank"
	"ezar-b2b-api/ent/linstockstockssymbol"
	"fmt"
	"time"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// LinstockStocksEtfvolume1mrankCreate is the builder for creating a LinstockStocksEtfvolume1mrank entity.
type LinstockStocksEtfvolume1mrankCreate struct {
	config
	mutation *LinstockStocksEtfvolume1mrankMutation
	hooks    []Hook
}

// SetRank sets the "rank" field.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetRank(i int32) *LinstockStocksEtfvolume1mrankCreate {
	lsec.mutation.SetRank(i)
	return lsec
}

// SetCreatedAt sets the "created_at" field.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetCreatedAt(t time.Time) *LinstockStocksEtfvolume1mrankCreate {
	lsec.mutation.SetCreatedAt(t)
	return lsec
}

// SetNillableCreatedAt sets the "created_at" field if the given value is not nil.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetNillableCreatedAt(t *time.Time) *LinstockStocksEtfvolume1mrankCreate {
	if t != nil {
		lsec.SetCreatedAt(*t)
	}
	return lsec
}

// SetUpdatedAt sets the "updated_at" field.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetUpdatedAt(t time.Time) *LinstockStocksEtfvolume1mrankCreate {
	lsec.mutation.SetUpdatedAt(t)
	return lsec
}

// SetNillableUpdatedAt sets the "updated_at" field if the given value is not nil.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetNillableUpdatedAt(t *time.Time) *LinstockStocksEtfvolume1mrankCreate {
	if t != nil {
		lsec.SetUpdatedAt(*t)
	}
	return lsec
}

// SetDeletedAt sets the "deleted_at" field.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetDeletedAt(t time.Time) *LinstockStocksEtfvolume1mrankCreate {
	lsec.mutation.SetDeletedAt(t)
	return lsec
}

// SetNillableDeletedAt sets the "deleted_at" field if the given value is not nil.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetNillableDeletedAt(t *time.Time) *LinstockStocksEtfvolume1mrankCreate {
	if t != nil {
		lsec.SetDeletedAt(*t)
	}
	return lsec
}

// SetValue sets the "value" field.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetValue(f float64) *LinstockStocksEtfvolume1mrankCreate {
	lsec.mutation.SetValue(f)
	return lsec
}

// SetSymbolID sets the "symbol_id" field.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetSymbolID(i int32) *LinstockStocksEtfvolume1mrankCreate {
	lsec.mutation.SetSymbolID(i)
	return lsec
}

// SetNillableSymbolID sets the "symbol_id" field if the given value is not nil.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetNillableSymbolID(i *int32) *LinstockStocksEtfvolume1mrankCreate {
	if i != nil {
		lsec.SetSymbolID(*i)
	}
	return lsec
}

// SetID sets the "id" field.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetID(i int32) *LinstockStocksEtfvolume1mrankCreate {
	lsec.mutation.SetID(i)
	return lsec
}

// SetLinstockStocksSymbolID sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity by ID.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetLinstockStocksSymbolID(id int32) *LinstockStocksEtfvolume1mrankCreate {
	lsec.mutation.SetLinstockStocksSymbolID(id)
	return lsec
}

// SetNillableLinstockStocksSymbolID sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity by ID if the given value is not nil.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetNillableLinstockStocksSymbolID(id *int32) *LinstockStocksEtfvolume1mrankCreate {
	if id != nil {
		lsec = lsec.SetLinstockStocksSymbolID(*id)
	}
	return lsec
}

// SetLinstockStocksSymbol sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SetLinstockStocksSymbol(l *LinstockStocksSymbol) *LinstockStocksEtfvolume1mrankCreate {
	return lsec.SetLinstockStocksSymbolID(l.ID)
}

// Mutation returns the LinstockStocksEtfvolume1mrankMutation object of the builder.
func (lsec *LinstockStocksEtfvolume1mrankCreate) Mutation() *LinstockStocksEtfvolume1mrankMutation {
	return lsec.mutation
}

// Save creates the LinstockStocksEtfvolume1mrank in the database.
func (lsec *LinstockStocksEtfvolume1mrankCreate) Save(ctx context.Context) (*LinstockStocksEtfvolume1mrank, error) {
	var (
		err  error
		node *LinstockStocksEtfvolume1mrank
	)
	if len(lsec.hooks) == 0 {
		if err = lsec.check(); err != nil {
			return nil, err
		}
		node, err = lsec.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*LinstockStocksEtfvolume1mrankMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = lsec.check(); err != nil {
				return nil, err
			}
			lsec.mutation = mutation
			if node, err = lsec.sqlSave(ctx); err != nil {
				return nil, err
			}
			mutation.id = &node.ID
			mutation.done = true
			return node, err
		})
		for i := len(lsec.hooks) - 1; i >= 0; i-- {
			if lsec.hooks[i] == nil {
				return nil, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = lsec.hooks[i](mut)
		}
		v, err := mut.Mutate(ctx, lsec.mutation)
		if err != nil {
			return nil, err
		}
		nv, ok := v.(*LinstockStocksEtfvolume1mrank)
		if !ok {
			return nil, fmt.Errorf("unexpected node type %T returned from LinstockStocksEtfvolume1mrankMutation", v)
		}
		node = nv
	}
	return node, err
}

// SaveX calls Save and panics if Save returns an error.
func (lsec *LinstockStocksEtfvolume1mrankCreate) SaveX(ctx context.Context) *LinstockStocksEtfvolume1mrank {
	v, err := lsec.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (lsec *LinstockStocksEtfvolume1mrankCreate) Exec(ctx context.Context) error {
	_, err := lsec.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (lsec *LinstockStocksEtfvolume1mrankCreate) ExecX(ctx context.Context) {
	if err := lsec.Exec(ctx); err != nil {
		panic(err)
	}
}

// check runs all checks and user-defined validators on the builder.
func (lsec *LinstockStocksEtfvolume1mrankCreate) check() error {
	if _, ok := lsec.mutation.Rank(); !ok {
		return &ValidationError{Name: "rank", err: errors.New(`ent: missing required field "LinstockStocksEtfvolume1mrank.rank"`)}
	}
	if _, ok := lsec.mutation.Value(); !ok {
		return &ValidationError{Name: "value", err: errors.New(`ent: missing required field "LinstockStocksEtfvolume1mrank.value"`)}
	}
	return nil
}

func (lsec *LinstockStocksEtfvolume1mrankCreate) sqlSave(ctx context.Context) (*LinstockStocksEtfvolume1mrank, error) {
	_node, _spec := lsec.createSpec()
	if err := sqlgraph.CreateNode(ctx, lsec.driver, _spec); err != nil {
		if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	if _spec.ID.Value != _node.ID {
		id := _spec.ID.Value.(int64)
		_node.ID = int32(id)
	}
	return _node, nil
}

func (lsec *LinstockStocksEtfvolume1mrankCreate) createSpec() (*LinstockStocksEtfvolume1mrank, *sqlgraph.CreateSpec) {
	var (
		_node = &LinstockStocksEtfvolume1mrank{config: lsec.config}
		_spec = &sqlgraph.CreateSpec{
			Table: linstockstocksetfvolume1mrank.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt32,
				Column: linstockstocksetfvolume1mrank.FieldID,
			},
		}
	)
	if id, ok := lsec.mutation.ID(); ok {
		_node.ID = id
		_spec.ID.Value = id
	}
	if value, ok := lsec.mutation.Rank(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeInt32,
			Value:  value,
			Column: linstockstocksetfvolume1mrank.FieldRank,
		})
		_node.Rank = value
	}
	if value, ok := lsec.mutation.CreatedAt(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksetfvolume1mrank.FieldCreatedAt,
		})
		_node.CreatedAt = value
	}
	if value, ok := lsec.mutation.UpdatedAt(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksetfvolume1mrank.FieldUpdatedAt,
		})
		_node.UpdatedAt = value
	}
	if value, ok := lsec.mutation.DeletedAt(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksetfvolume1mrank.FieldDeletedAt,
		})
		_node.DeletedAt = value
	}
	if value, ok := lsec.mutation.Value(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeFloat64,
			Value:  value,
			Column: linstockstocksetfvolume1mrank.FieldValue,
		})
		_node.Value = value
	}
	if nodes := lsec.mutation.LinstockStocksSymbolIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   linstockstocksetfvolume1mrank.LinstockStocksSymbolTable,
			Columns: []string{linstockstocksetfvolume1mrank.LinstockStocksSymbolColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt32,
					Column: linstockstockssymbol.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_node.SymbolID = nodes[0]
		_spec.Edges = append(_spec.Edges, edge)
	}
	return _node, _spec
}

// LinstockStocksEtfvolume1mrankCreateBulk is the builder for creating many LinstockStocksEtfvolume1mrank entities in bulk.
type LinstockStocksEtfvolume1mrankCreateBulk struct {
	config
	builders []*LinstockStocksEtfvolume1mrankCreate
}

// Save creates the LinstockStocksEtfvolume1mrank entities in the database.
func (lsecb *LinstockStocksEtfvolume1mrankCreateBulk) Save(ctx context.Context) ([]*LinstockStocksEtfvolume1mrank, error) {
	specs := make([]*sqlgraph.CreateSpec, len(lsecb.builders))
	nodes := make([]*LinstockStocksEtfvolume1mrank, len(lsecb.builders))
	mutators := make([]Mutator, len(lsecb.builders))
	for i := range lsecb.builders {
		func(i int, root context.Context) {
			builder := lsecb.builders[i]
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*LinstockStocksEtfvolume1mrankMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				nodes[i], specs[i] = builder.createSpec()
				var err error
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, lsecb.builders[i+1].mutation)
				} else {
					spec := &sqlgraph.BatchCreateSpec{Nodes: specs}
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, lsecb.driver, spec); err != nil {
						if sqlgraph.IsConstraintError(err) {
							err = &ConstraintError{msg: err.Error(), wrap: err}
						}
					}
				}
				if err != nil {
					return nil, err
				}
				mutation.id = &nodes[i].ID
				if specs[i].ID.Value != nil && nodes[i].ID == 0 {
					id := specs[i].ID.Value.(int64)
					nodes[i].ID = int32(id)
				}
				mutation.done = true
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, lsecb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (lsecb *LinstockStocksEtfvolume1mrankCreateBulk) SaveX(ctx context.Context) []*LinstockStocksEtfvolume1mrank {
	v, err := lsecb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (lsecb *LinstockStocksEtfvolume1mrankCreateBulk) Exec(ctx context.Context) error {
	_, err := lsecb.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (lsecb *LinstockStocksEtfvolume1mrankCreateBulk) ExecX(ctx context.Context) {
	if err := lsecb.Exec(ctx); err != nil {
		panic(err)
	}
}
