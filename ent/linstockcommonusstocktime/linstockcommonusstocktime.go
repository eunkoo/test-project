// Code generated by ent, DO NOT EDIT.

package linstockcommonusstocktime

const (
	// Label holds the string label denoting the linstockcommonusstocktime type in the database.
	Label = "linstock_common_usstocktime"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldTitle holds the string denoting the title field in the database.
	FieldTitle = "title"
	// FieldTime holds the string denoting the time field in the database.
	FieldTime = "time"
	// FieldCreatedAt holds the string denoting the created_at field in the database.
	FieldCreatedAt = "created_at"
	// FieldUpdatedAt holds the string denoting the updated_at field in the database.
	FieldUpdatedAt = "updated_at"
	// FieldDeletedAt holds the string denoting the deleted_at field in the database.
	FieldDeletedAt = "deleted_at"
	// Table holds the table name of the linstockcommonusstocktime in the database.
	Table = "linstock_common_usstocktime"
)

// Columns holds all SQL columns for linstockcommonusstocktime fields.
var Columns = []string{
	FieldID,
	FieldTitle,
	FieldTime,
	FieldCreatedAt,
	FieldUpdatedAt,
	FieldDeletedAt,
}

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	return false
}
