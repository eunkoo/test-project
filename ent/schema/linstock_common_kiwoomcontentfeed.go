// Code generated by entimport, DO NOT EDIT.

package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/field"
)

type LinstockCommonKiwoomcontentfeed struct {
	ent.Schema
}

func (LinstockCommonKiwoomcontentfeed) Fields() []ent.Field {
	return []ent.Field{field.Int32("id"), field.String("title"), field.String("url"), field.String("Thumbnail"), field.Time("created_at"), field.Time("updated_at").Optional(), field.Time("deleted_at").Optional()}
}
func (LinstockCommonKiwoomcontentfeed) Edges() []ent.Edge {
	return nil
}
func (LinstockCommonKiwoomcontentfeed) Annotations() []schema.Annotation {
	return []schema.Annotation{entsql.Annotation{Table: "linstock_common_kiwoomcontentfeed"}}
}
