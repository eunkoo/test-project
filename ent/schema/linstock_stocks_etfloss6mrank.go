// Code generated by entimport, DO NOT EDIT.

package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

type LinstockStocksEtfloss6mrank struct {
	ent.Schema
}

func (LinstockStocksEtfloss6mrank) Fields() []ent.Field {
	return []ent.Field{field.Int32("id"), field.Int32("rank"), field.Time("created_at").Optional(), field.Time("updated_at").Optional(), field.Time("deleted_at").Optional(), field.Float("value"), field.Int32("symbol_id").Optional()}
}
func (LinstockStocksEtfloss6mrank) Edges() []ent.Edge {
	return []ent.Edge{edge.From("linstock_stocks_symbol", LinstockStocksSymbol.Type).Ref("linstock_stocks_etfloss6mranks").Unique().Field("symbol_id")}
}
func (LinstockStocksEtfloss6mrank) Annotations() []schema.Annotation {
	return []schema.Annotation{entsql.Annotation{Table: "linstock_stocks_etfloss6mrank"}}
}
