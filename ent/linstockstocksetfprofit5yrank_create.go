// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"ezar-b2b-api/ent/linstockstocksetfprofit5yrank"
	"ezar-b2b-api/ent/linstockstockssymbol"
	"fmt"
	"time"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// LinstockStocksEtfprofit5yrankCreate is the builder for creating a LinstockStocksEtfprofit5yrank entity.
type LinstockStocksEtfprofit5yrankCreate struct {
	config
	mutation *LinstockStocksEtfprofit5yrankMutation
	hooks    []Hook
}

// SetRank sets the "rank" field.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetRank(i int32) *LinstockStocksEtfprofit5yrankCreate {
	lsec.mutation.SetRank(i)
	return lsec
}

// SetCreatedAt sets the "created_at" field.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetCreatedAt(t time.Time) *LinstockStocksEtfprofit5yrankCreate {
	lsec.mutation.SetCreatedAt(t)
	return lsec
}

// SetNillableCreatedAt sets the "created_at" field if the given value is not nil.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetNillableCreatedAt(t *time.Time) *LinstockStocksEtfprofit5yrankCreate {
	if t != nil {
		lsec.SetCreatedAt(*t)
	}
	return lsec
}

// SetUpdatedAt sets the "updated_at" field.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetUpdatedAt(t time.Time) *LinstockStocksEtfprofit5yrankCreate {
	lsec.mutation.SetUpdatedAt(t)
	return lsec
}

// SetNillableUpdatedAt sets the "updated_at" field if the given value is not nil.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetNillableUpdatedAt(t *time.Time) *LinstockStocksEtfprofit5yrankCreate {
	if t != nil {
		lsec.SetUpdatedAt(*t)
	}
	return lsec
}

// SetDeletedAt sets the "deleted_at" field.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetDeletedAt(t time.Time) *LinstockStocksEtfprofit5yrankCreate {
	lsec.mutation.SetDeletedAt(t)
	return lsec
}

// SetNillableDeletedAt sets the "deleted_at" field if the given value is not nil.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetNillableDeletedAt(t *time.Time) *LinstockStocksEtfprofit5yrankCreate {
	if t != nil {
		lsec.SetDeletedAt(*t)
	}
	return lsec
}

// SetValue sets the "value" field.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetValue(f float64) *LinstockStocksEtfprofit5yrankCreate {
	lsec.mutation.SetValue(f)
	return lsec
}

// SetSymbolID sets the "symbol_id" field.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetSymbolID(i int32) *LinstockStocksEtfprofit5yrankCreate {
	lsec.mutation.SetSymbolID(i)
	return lsec
}

// SetNillableSymbolID sets the "symbol_id" field if the given value is not nil.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetNillableSymbolID(i *int32) *LinstockStocksEtfprofit5yrankCreate {
	if i != nil {
		lsec.SetSymbolID(*i)
	}
	return lsec
}

// SetID sets the "id" field.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetID(i int32) *LinstockStocksEtfprofit5yrankCreate {
	lsec.mutation.SetID(i)
	return lsec
}

// SetLinstockStocksSymbolID sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity by ID.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetLinstockStocksSymbolID(id int32) *LinstockStocksEtfprofit5yrankCreate {
	lsec.mutation.SetLinstockStocksSymbolID(id)
	return lsec
}

// SetNillableLinstockStocksSymbolID sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity by ID if the given value is not nil.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetNillableLinstockStocksSymbolID(id *int32) *LinstockStocksEtfprofit5yrankCreate {
	if id != nil {
		lsec = lsec.SetLinstockStocksSymbolID(*id)
	}
	return lsec
}

// SetLinstockStocksSymbol sets the "linstock_stocks_symbol" edge to the LinstockStocksSymbol entity.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SetLinstockStocksSymbol(l *LinstockStocksSymbol) *LinstockStocksEtfprofit5yrankCreate {
	return lsec.SetLinstockStocksSymbolID(l.ID)
}

// Mutation returns the LinstockStocksEtfprofit5yrankMutation object of the builder.
func (lsec *LinstockStocksEtfprofit5yrankCreate) Mutation() *LinstockStocksEtfprofit5yrankMutation {
	return lsec.mutation
}

// Save creates the LinstockStocksEtfprofit5yrank in the database.
func (lsec *LinstockStocksEtfprofit5yrankCreate) Save(ctx context.Context) (*LinstockStocksEtfprofit5yrank, error) {
	var (
		err  error
		node *LinstockStocksEtfprofit5yrank
	)
	if len(lsec.hooks) == 0 {
		if err = lsec.check(); err != nil {
			return nil, err
		}
		node, err = lsec.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*LinstockStocksEtfprofit5yrankMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = lsec.check(); err != nil {
				return nil, err
			}
			lsec.mutation = mutation
			if node, err = lsec.sqlSave(ctx); err != nil {
				return nil, err
			}
			mutation.id = &node.ID
			mutation.done = true
			return node, err
		})
		for i := len(lsec.hooks) - 1; i >= 0; i-- {
			if lsec.hooks[i] == nil {
				return nil, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = lsec.hooks[i](mut)
		}
		v, err := mut.Mutate(ctx, lsec.mutation)
		if err != nil {
			return nil, err
		}
		nv, ok := v.(*LinstockStocksEtfprofit5yrank)
		if !ok {
			return nil, fmt.Errorf("unexpected node type %T returned from LinstockStocksEtfprofit5yrankMutation", v)
		}
		node = nv
	}
	return node, err
}

// SaveX calls Save and panics if Save returns an error.
func (lsec *LinstockStocksEtfprofit5yrankCreate) SaveX(ctx context.Context) *LinstockStocksEtfprofit5yrank {
	v, err := lsec.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (lsec *LinstockStocksEtfprofit5yrankCreate) Exec(ctx context.Context) error {
	_, err := lsec.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (lsec *LinstockStocksEtfprofit5yrankCreate) ExecX(ctx context.Context) {
	if err := lsec.Exec(ctx); err != nil {
		panic(err)
	}
}

// check runs all checks and user-defined validators on the builder.
func (lsec *LinstockStocksEtfprofit5yrankCreate) check() error {
	if _, ok := lsec.mutation.Rank(); !ok {
		return &ValidationError{Name: "rank", err: errors.New(`ent: missing required field "LinstockStocksEtfprofit5yrank.rank"`)}
	}
	if _, ok := lsec.mutation.Value(); !ok {
		return &ValidationError{Name: "value", err: errors.New(`ent: missing required field "LinstockStocksEtfprofit5yrank.value"`)}
	}
	return nil
}

func (lsec *LinstockStocksEtfprofit5yrankCreate) sqlSave(ctx context.Context) (*LinstockStocksEtfprofit5yrank, error) {
	_node, _spec := lsec.createSpec()
	if err := sqlgraph.CreateNode(ctx, lsec.driver, _spec); err != nil {
		if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	if _spec.ID.Value != _node.ID {
		id := _spec.ID.Value.(int64)
		_node.ID = int32(id)
	}
	return _node, nil
}

func (lsec *LinstockStocksEtfprofit5yrankCreate) createSpec() (*LinstockStocksEtfprofit5yrank, *sqlgraph.CreateSpec) {
	var (
		_node = &LinstockStocksEtfprofit5yrank{config: lsec.config}
		_spec = &sqlgraph.CreateSpec{
			Table: linstockstocksetfprofit5yrank.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt32,
				Column: linstockstocksetfprofit5yrank.FieldID,
			},
		}
	)
	if id, ok := lsec.mutation.ID(); ok {
		_node.ID = id
		_spec.ID.Value = id
	}
	if value, ok := lsec.mutation.Rank(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeInt32,
			Value:  value,
			Column: linstockstocksetfprofit5yrank.FieldRank,
		})
		_node.Rank = value
	}
	if value, ok := lsec.mutation.CreatedAt(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksetfprofit5yrank.FieldCreatedAt,
		})
		_node.CreatedAt = value
	}
	if value, ok := lsec.mutation.UpdatedAt(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksetfprofit5yrank.FieldUpdatedAt,
		})
		_node.UpdatedAt = value
	}
	if value, ok := lsec.mutation.DeletedAt(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: linstockstocksetfprofit5yrank.FieldDeletedAt,
		})
		_node.DeletedAt = value
	}
	if value, ok := lsec.mutation.Value(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeFloat64,
			Value:  value,
			Column: linstockstocksetfprofit5yrank.FieldValue,
		})
		_node.Value = value
	}
	if nodes := lsec.mutation.LinstockStocksSymbolIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   linstockstocksetfprofit5yrank.LinstockStocksSymbolTable,
			Columns: []string{linstockstocksetfprofit5yrank.LinstockStocksSymbolColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt32,
					Column: linstockstockssymbol.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_node.SymbolID = nodes[0]
		_spec.Edges = append(_spec.Edges, edge)
	}
	return _node, _spec
}

// LinstockStocksEtfprofit5yrankCreateBulk is the builder for creating many LinstockStocksEtfprofit5yrank entities in bulk.
type LinstockStocksEtfprofit5yrankCreateBulk struct {
	config
	builders []*LinstockStocksEtfprofit5yrankCreate
}

// Save creates the LinstockStocksEtfprofit5yrank entities in the database.
func (lsecb *LinstockStocksEtfprofit5yrankCreateBulk) Save(ctx context.Context) ([]*LinstockStocksEtfprofit5yrank, error) {
	specs := make([]*sqlgraph.CreateSpec, len(lsecb.builders))
	nodes := make([]*LinstockStocksEtfprofit5yrank, len(lsecb.builders))
	mutators := make([]Mutator, len(lsecb.builders))
	for i := range lsecb.builders {
		func(i int, root context.Context) {
			builder := lsecb.builders[i]
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*LinstockStocksEtfprofit5yrankMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				nodes[i], specs[i] = builder.createSpec()
				var err error
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, lsecb.builders[i+1].mutation)
				} else {
					spec := &sqlgraph.BatchCreateSpec{Nodes: specs}
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, lsecb.driver, spec); err != nil {
						if sqlgraph.IsConstraintError(err) {
							err = &ConstraintError{msg: err.Error(), wrap: err}
						}
					}
				}
				if err != nil {
					return nil, err
				}
				mutation.id = &nodes[i].ID
				if specs[i].ID.Value != nil && nodes[i].ID == 0 {
					id := specs[i].ID.Value.(int64)
					nodes[i].ID = int32(id)
				}
				mutation.done = true
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, lsecb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (lsecb *LinstockStocksEtfprofit5yrankCreateBulk) SaveX(ctx context.Context) []*LinstockStocksEtfprofit5yrank {
	v, err := lsecb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (lsecb *LinstockStocksEtfprofit5yrankCreateBulk) Exec(ctx context.Context) error {
	_, err := lsecb.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (lsecb *LinstockStocksEtfprofit5yrankCreateBulk) ExecX(ctx context.Context) {
	if err := lsecb.Exec(ctx); err != nil {
		panic(err)
	}
}
