// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"ezar-b2b-api/ent/linstockstockshistoricaldividendu"
	"ezar-b2b-api/ent/predicate"
	"fmt"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// LinstockStocksHistoricaldividenduDelete is the builder for deleting a LinstockStocksHistoricaldividendu entity.
type LinstockStocksHistoricaldividenduDelete struct {
	config
	hooks    []Hook
	mutation *LinstockStocksHistoricaldividenduMutation
}

// Where appends a list predicates to the LinstockStocksHistoricaldividenduDelete builder.
func (lshd *LinstockStocksHistoricaldividenduDelete) Where(ps ...predicate.LinstockStocksHistoricaldividendu) *LinstockStocksHistoricaldividenduDelete {
	lshd.mutation.Where(ps...)
	return lshd
}

// Exec executes the deletion query and returns how many vertices were deleted.
func (lshd *LinstockStocksHistoricaldividenduDelete) Exec(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(lshd.hooks) == 0 {
		affected, err = lshd.sqlExec(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*LinstockStocksHistoricaldividenduMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			lshd.mutation = mutation
			affected, err = lshd.sqlExec(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(lshd.hooks) - 1; i >= 0; i-- {
			if lshd.hooks[i] == nil {
				return 0, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = lshd.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, lshd.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// ExecX is like Exec, but panics if an error occurs.
func (lshd *LinstockStocksHistoricaldividenduDelete) ExecX(ctx context.Context) int {
	n, err := lshd.Exec(ctx)
	if err != nil {
		panic(err)
	}
	return n
}

func (lshd *LinstockStocksHistoricaldividenduDelete) sqlExec(ctx context.Context) (int, error) {
	_spec := &sqlgraph.DeleteSpec{
		Node: &sqlgraph.NodeSpec{
			Table: linstockstockshistoricaldividendu.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt32,
				Column: linstockstockshistoricaldividendu.FieldID,
			},
		},
	}
	if ps := lshd.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	affected, err := sqlgraph.DeleteNodes(ctx, lshd.driver, _spec)
	if err != nil && sqlgraph.IsConstraintError(err) {
		err = &ConstraintError{msg: err.Error(), wrap: err}
	}
	return affected, err
}

// LinstockStocksHistoricaldividenduDeleteOne is the builder for deleting a single LinstockStocksHistoricaldividendu entity.
type LinstockStocksHistoricaldividenduDeleteOne struct {
	lshd *LinstockStocksHistoricaldividenduDelete
}

// Exec executes the deletion query.
func (lshdo *LinstockStocksHistoricaldividenduDeleteOne) Exec(ctx context.Context) error {
	n, err := lshdo.lshd.Exec(ctx)
	switch {
	case err != nil:
		return err
	case n == 0:
		return &NotFoundError{linstockstockshistoricaldividendu.Label}
	default:
		return nil
	}
}

// ExecX is like Exec, but panics if an error occurs.
func (lshdo *LinstockStocksHistoricaldividenduDeleteOne) ExecX(ctx context.Context) {
	lshdo.lshd.ExecX(ctx)
}
