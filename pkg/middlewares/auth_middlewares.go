package middlewares

import (
	"context"
	"ezar-b2b-api/ent/linstockstocksapiclientkey"
	"ezar-b2b-api/pkg/core/db"
	"ezar-b2b-api/pkg/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func AuthApiKey() gin.HandlerFunc {
	return func(c *gin.Context) {
		apikey := c.Request.Header.Get("X-API-Key")
		if apikey == "" {
			c.AbortWithStatusJSON(http.StatusBadRequest, models.NewError("request does not contain an apikey"))
			return
		}

		db := db.Client()
		defer db.Close()

		query := db.LinstockStocksApiclientkey.
			Query().
			Where(linstockstocksapiclientkey.APIKey(apikey)).
			FirstX(context.Background())
		if query != nil && query.ExpireAt.Before(time.Now()) {
			db.LinstockStocksApiclientkey.
				Update().
				Where(linstockstocksapiclientkey.APIKey(apikey)).SetIsExpired(true)
		}
		if query == nil || query.IsExpired {
			c.AbortWithStatusJSON(http.StatusUnauthorized, models.NewError("not valid apikey"))
			return
		}
		c.Set("response_format", query.ResponseFormat)
		c.Next()
	}
}
