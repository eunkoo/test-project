package middlewares

import (
	"github.com/gin-contrib/cors"
)

// CORS for https://foo.com and https://github.com origins, allowing:
// - PUT and PATCH methods
// - Origin header
// - Credentials share
// - Preflight requests cached for 12 hours

func CorsMiddleware() cors.Config {
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowOrigins = []string{"https://public-api-test.ezar.co.kr", "https://samsung.ezar.co.kr", "http://localhost:3000", "http://localhost:4200", "https://public-api-us-staging.ezar.co.kr"}
	corsConfig.AddAllowMethods("GET", "POST")
	corsConfig.AllowHeaders = []string{"Origin", "X-API-KEY", "Content-Type", "Accept", "responseType"}

	return corsConfig
}
