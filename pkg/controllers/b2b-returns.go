package handlers

import (
	"context"
	"encoding/json"
	"ezar-b2b-api/ent"
	"ezar-b2b-api/ent/linstockstockssymbol"
	config "ezar-b2b-api/pkg/core/configs"
	"ezar-b2b-api/pkg/core/db"
	"ezar-b2b-api/pkg/core/redisClient"
	"ezar-b2b-api/pkg/models"
	"ezar-b2b-api/pkg/utils"
	"fmt"
	"math"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"entgo.io/ent/dialect/sql"
	"github.com/gin-gonic/gin"
)

var annualYears = [7]int{1, 3, 5, 7, 10, 15, 20}
var cummulativeYears = [11][3]int{{0, 0, 7}, {0, 1, 0}, {0, 3, 0}, {0, 6, 0}, {1, 0, 0}, {3, 0, 0}, {5, 0, 0}, {7, 0, 0}, {10, 0, 0}, {15, 0, 0}, {20, 0, 0}}

func FindHistoricalPrice(historical_prices []*models.HistoricalPrice, date string, start bool) (float64, string) {
	var prev float64
	var nonHolidayDate string
	fix, _ := time.Parse("2006-01-02", date)
	for _, x := range historical_prices {
		cur, _ := time.Parse("2006-01-02", x.Date)
		if x.Date == date {
			return x.AdjustedClose, x.Date
		} else if fix.Before(cur) {
			if start {
				return x.AdjustedClose, x.Date
			} else {
				break
			}
		}
		prev = x.AdjustedClose
		nonHolidayDate = x.Date
	}
	return prev, nonHolidayDate
}

func FindHistoricalPriceFull(historical_prices []*models.HistoricalPrice, date string, start bool) *models.HistoricalPrice {
	var rst *models.HistoricalPrice
	fix, _ := time.Parse("2006-01-02", date)
	for _, x := range historical_prices {
		cur, _ := time.Parse("2006-01-02", x.Date)
		if x.Date == date {
			return x
		} else if fix.Before(cur) {
			if start {
				return x
			} else {
				break
			}
		}
		rst = x
	}
	return rst
}

func getFirstTradingDate(symbols string) []*ent.LinstockStocksSymbol {
	db := db.Client()
	defer db.Close()

	symbolsList := strings.Split(symbols, ",")
	symbol := utils.AnyAllDrivVal(symbolsList)
	data := db.LinstockStocksSymbol.Query().Where(func(s *sql.Selector) {
		s.Where(sql.InValues(linstockstockssymbol.FieldSymbol, symbol...))
	}).WithLinstockStocksEtfdetail().AllX(context.TODO())
	return data
}

func getHistoricalPrices(symbol string, firstTradingDate time.Time) []*models.HistoricalPrice {
	path := fmt.Sprintf("/private/historical-price-full/%s", symbol)
	query := url.Values{}
	query.Add("from", firstTradingDate.Format("2006-01-02"))
	var historical_prices []*models.HistoricalPrice
	json.Unmarshal(FetchPriceFull(query, path), &historical_prices)
	utils.Reverse(historical_prices)
	return historical_prices
}

func getHistoricalPricesRange(symbols []string, from time.Time, to time.Time) map[string][]*models.HistoricalPrice {
	rst := make(map[string][]*models.HistoricalPrice)
	for _, v := range symbols {
		path := fmt.Sprintf("/private/historical-price-full/%s", v)
		query := url.Values{}
		query.Add("to", to.Format("2006-01-02"))
		query.Add("from", from.Format("2006-01-02"))
		var historical_prices []*models.HistoricalPrice
		json.Unmarshal(FetchPriceFull(query, path), &historical_prices)
		utils.Reverse(historical_prices)
		rst[v] = historical_prices
	}
	return rst
}

// B2B Annualized Return
// @Summary Annualized return values for provided symbol
// @Tags 		returns
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string false "ETF symbol: QQQ"
// @Security  ApiKeyAuth
// @Router 		/returns/annualized/{symbol} [get]
// @Success 	200 {object} models.AnnualizedReturn
func GetAnnualizedReturns(c *gin.Context) {
	syms, err1 := c.Params.Get("symbol")
	if err1 == false {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("Only us domicile"))
		return
	}
	data := getFirstTradingDate(syms)
	var startDates = make(map[string]string)
	for _, v := range data {
		if v.Edges.LinstockStocksEtfdetail != nil {
			startDates[v.Symbol] = v.Edges.LinstockStocksEtfdetail.FirstTradingDate.Format("2006-01-02")
		} else {
			c.Error(fmt.Errorf("No First Trading Date"))
		}
	}
	var rst []*models.AnnualizedReturn

	for name, date := range startDates {
		var elem models.AnnualizedReturn
		initial, err := time.Parse("2006-01-02", date)
		if err != nil {
			c.Error(err)
			continue
		}
		historical_prices := getHistoricalPrices(name, initial)

		end, err := time.Parse("2006-01-02", historical_prices[len(historical_prices)-1].Date)
		if err != nil {
			c.Error(err)
			continue
		}
		elem.Symbol = name
		for _, year := range annualYears {
			cur := end.AddDate(-year, 0, 0)
			if initial.After(cur) {
				break
			}
			start, strDate := FindHistoricalPrice(historical_prices, cur.Format("2006-01-02"), false)
			nonHolidayDate, _ := time.Parse("2006-01-02", strDate)
			days := end.Sub(nonHolidayDate).Hours() / 24

			switch year {
			case 1:
				elem.OneY = utils.CalculateAnnualizedReturn(start, historical_prices[len(historical_prices)-1].AdjustedClose, days)
			case 3:
				elem.ThreeY = utils.CalculateAnnualizedReturn(start, historical_prices[len(historical_prices)-1].AdjustedClose, days)
			case 5:
				elem.FiveY = utils.CalculateAnnualizedReturn(start, historical_prices[len(historical_prices)-1].AdjustedClose, days)
			case 7:
				elem.SevenY = utils.CalculateAnnualizedReturn(start, historical_prices[len(historical_prices)-1].AdjustedClose, days)
			case 10:
				elem.TenY = utils.CalculateAnnualizedReturn(start, historical_prices[len(historical_prices)-1].AdjustedClose, days)
			case 15:
				elem.FivetenY = utils.CalculateAnnualizedReturn(start, historical_prices[len(historical_prices)-1].AdjustedClose, days)
			case 20:
				elem.TwentY = utils.CalculateAnnualizedReturn(start, historical_prices[len(historical_prices)-1].AdjustedClose, days)
			}
		}
		days := end.Sub(initial).Hours() / 24
		elem.Max = utils.CalculateAnnualizedReturn(historical_prices[0].AdjustedClose, historical_prices[len(historical_prices)-1].AdjustedClose, days)
		rst = append(rst, &elem)
	}
	c.JSON(http.StatusOK, &rst)
}

// B2B Cumulative Return
// @Summary Cumulative return values for provided symbol
// @Tags 		returns
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string false "ETF symbol: QQQ"
// @Security  ApiKeyAuth
// @Router 		/returns/cumulative/{symbol} [get]
// @Success 	200 {object} models.CumulativeReturn
func GetCumulativeReturns(c *gin.Context) {
	syms, err1 := c.Params.Get("symbol")
	if err1 == false {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("Only us domicile"))
		return
	}
	data := getFirstTradingDate(syms)
	var startDates = make(map[string]string)
	for _, v := range data {
		if v.Edges.LinstockStocksEtfdetail != nil {
			startDates[v.Symbol] = v.Edges.LinstockStocksEtfdetail.FirstTradingDate.Format("2006-01-02")
		} else {
			c.Error(fmt.Errorf("No First Trading Date"))
		}
	}
	var rst []*models.CumulativeReturn
	for name, date := range startDates {
		var elem models.CumulativeReturn
		initial, err := time.Parse("2006-01-02", date)
		if err != nil {
			c.Error(err)
			continue
		}
		historical_prices := getHistoricalPrices(name, initial)

		end, err := time.Parse("2006-01-02", historical_prices[len(historical_prices)-1].Date)
		if err != nil {
			c.Error(err)
			continue
		}
		elem.Symbol = name
		for index, year := range cummulativeYears {
			cur := end.AddDate(-year[0], -year[1], -year[2])
			if initial.After(cur) {
				break
			}
			start, _ := FindHistoricalPrice(historical_prices, cur.Format("2006-01-02"), false)
			switch index {
			case 0:
				elem.OneW = utils.CalculateCumulativeReturns(start, historical_prices[len(historical_prices)-1].AdjustedClose)
			case 1:
				elem.OneM = utils.CalculateCumulativeReturns(start, historical_prices[len(historical_prices)-1].AdjustedClose)
			case 2:
				elem.ThreeM = utils.CalculateCumulativeReturns(start, historical_prices[len(historical_prices)-1].AdjustedClose)
			case 3:
				elem.SixM = utils.CalculateCumulativeReturns(start, historical_prices[len(historical_prices)-1].AdjustedClose)
			case 4:
				elem.OneY = utils.CalculateCumulativeReturns(start, historical_prices[len(historical_prices)-1].AdjustedClose)
			case 5:
				elem.ThreeY = utils.CalculateCumulativeReturns(start, historical_prices[len(historical_prices)-1].AdjustedClose)
			case 6:
				elem.FiveY = utils.CalculateCumulativeReturns(start, historical_prices[len(historical_prices)-1].AdjustedClose)
			case 7:
				elem.SevenY = utils.CalculateCumulativeReturns(start, historical_prices[len(historical_prices)-1].AdjustedClose)
			case 8:
				elem.TenY = utils.CalculateCumulativeReturns(start, historical_prices[len(historical_prices)-1].AdjustedClose)
			case 9:
				elem.FivetenY = utils.CalculateCumulativeReturns(start, historical_prices[len(historical_prices)-1].AdjustedClose)
			case 10:
				elem.TwentY = utils.CalculateCumulativeReturns(start, historical_prices[len(historical_prices)-1].AdjustedClose)
			}
		}
		elem.Max = utils.CalculateCumulativeReturns(historical_prices[0].AdjustedClose, historical_prices[len(historical_prices)-1].AdjustedClose)
		rst = append(rst, &elem)
	}
	c.JSON(http.StatusOK, &rst)

}

// B2B ReturnByYear
// @Summary By Year return values for provided symbol
// @Tags 		returns
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string false "ETF symbol: QQQ"
// @Security  ApiKeyAuth
// @Router 		/returns/byyear/{symbol} [get]
// @Success 	200 {object} models.ReturnsByYear
func GetReturnsByYear(c *gin.Context) {
	syms, err1 := c.Params.Get("symbol")
	if err1 == false {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("Only us domicile"))
		return
	}
	data := getFirstTradingDate(syms)
	var startDates = make(map[string]string)
	for _, v := range data {
		if v.Edges.LinstockStocksEtfdetail != nil {
			startDates[v.Symbol] = v.Edges.LinstockStocksEtfdetail.FirstTradingDate.Format("2006-01-02")
		} else {
			c.Error(fmt.Errorf("No First Trading Date"))
		}
	}
	var rst []*models.ReturnsByYear

	for name, date := range startDates {
		elem := models.ReturnsByYear{}
		initial, err := time.Parse("2006-01-02", date)
		if err != nil {
			c.Error(err)
			continue
		}
		historical_prices := getHistoricalPrices(name, initial)
		end, err := time.Parse("2006-01-02", historical_prices[len(historical_prices)-1].Date)
		if err != nil {
			c.Error(err)
			continue
		}
		startYear := initial.Year()
		endYear := end.Year()
		elemDates := make(map[string]float64)
		for year := startYear; year <= endYear; year++ {
			endOfYear := time.Date(year, time.December, 31, 0, 0, 0, 0, time.UTC)
			startOfYear := time.Date(year, time.January, 1, 0, 0, 0, 0, time.UTC)
			if startOfYear.Before(initial) {
				startOfYear = initial
			}
			if endOfYear.After(end) {
				endOfYear = end
			}
			start, _ := FindHistoricalPrice(historical_prices, startOfYear.Format("2006-01-02"), true)
			end, _ := FindHistoricalPrice(historical_prices, endOfYear.Format("2006-01-02"), false)
			elemDates[strconv.Itoa(year)] = utils.CalculateCumulativeReturns(start, end)

		}
		elem.Symbol = name
		elem.Returns = elemDates
		rst = append(rst, &elem)
	}

	c.JSON(http.StatusOK, &rst)
}

var regular_freq = [3][3]int{{0, 0, 1}, {0, 0, 7}, {0, 1, 0}}

func getPriceFromTo(from string, to string, symbol string) []*models.HistoricalPrice {
	new_query := url.Values{}
	new_query.Add("from", from)
	new_query.Add("to", to)
	var historical_prices []*models.HistoricalPrice
	path := fmt.Sprintf("/private/historical-price-full/%s", symbol)
	json.Unmarshal(FetchPriceFull(new_query, path), &historical_prices)
	utils.Reverse(historical_prices)
	return historical_prices
}

// B2B Regular Return
// @Summary Regular return values for provided symbol
// @Tags 		returns
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string false "ETF symbol: QQQ"
// @Param       from 	query 	string	false "from: 2020-01-02"
// @Param       to 	query 	string	false "to: 2022-01-02"
// @Param       frequency 	query 	string	false "frequency:daily, weekly, monthly"
// @Security  ApiKeyAuth
// @Router 		/returns/regular/{symbol} [get]
// @Success 	200 {object} models.ReturnsRegular
func GetRegularReturn(c *gin.Context) {

	query := c.Request.URL.Query()
	symbol, err := c.Params.Get("symbol")
	if err == false {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("Only us domicile"))
		return
	}
	from := query.Get("from")
	if from == "" {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("No from"))
		return
	}
	to := query.Get("to")
	if to == "" {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("No to"))
		return
	}
	frequency := query.Get("frequency")
	if frequency == "" {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("No frequency"))
		return
	}
	if frequency != "daily" && frequency != "weekly" && frequency != "monthly" {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("frequency: daily, weekly, monthly"))
		return
	}
	_, err1 := time.Parse("2006-01-02", from)
	if err1 != nil {
		c.AbortWithError(http.StatusBadRequest, err1)
		return
	}
	_, err1 = time.Parse("2006-01-02", to)
	if err1 != nil {
		c.AbortWithError(http.StatusBadRequest, err1)
		return
	}

	historical_prices := getPriceFromTo(from, to, symbol)
	if len(historical_prices) == 0 {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("No prices at the provided period"))
		return
	}

	datePrice := make(map[string][]float64)
	for _, v := range historical_prices {
		datePrice[v.Date] = append(datePrice[v.Date], v.AdjustedClose)
		datePrice[v.Date] = append(datePrice[v.Date], v.Close)
	}
	var per [3]int
	switch frequency {
	case "daily":
		per = regular_freq[0]
	case "weekly":
		per = regular_freq[1]
	case "monthly":
		per = regular_freq[2]
	}
	startDate, _ := time.Parse("2006-01-02", historical_prices[0].Date)

	endDate, _ := time.Parse("2006-01-02", historical_prices[len(historical_prices)-1].Date)
	endPrice := historical_prices[len(historical_prices)-1].AdjustedClose
	endPriceWoDividend := historical_prices[len(historical_prices)-1].Close
	sum := 0.0
	sumWoDividend := 0.0
	periods := 0.0
	days := endDate.Sub(startDate).Hours() / 24
	for startDate.Before(endDate) {
		cur := startDate
		val, ok := datePrice[cur.Format("2006-01-02")]
		if !ok && frequency == "daily" {
			startDate = startDate.AddDate(per[0], per[1], per[2])
			continue
		}
		for !ok {
			cur = cur.AddDate(0, 0, -1)
			val, ok = datePrice[cur.Format("2006-01-02")]
		}
		sum += (endPrice - val[0]) / val[0]
		sumWoDividend += (endPriceWoDividend - val[1]) / val[1]
		periods += 1
		startDate = startDate.AddDate(per[0], per[1], per[2])
	}
	returnVal := sum / periods
	returnValWoDividend := sumWoDividend / periods
	annual := math.Pow((1+returnVal), (365/days)) - 1

	rst := models.ReturnsRegular{
		Return:           utils.KeepDecimal(100*returnVal, 2),
		ReturnWoDividend: utils.KeepDecimal(100*returnValWoDividend, 2),
		Annual:           utils.KeepDecimal(annual*100, 2),
		Frequency:        frequency,
		Period:           int64(days),
		InvestedTime:     int64(periods),
	}
	c.JSON(http.StatusOK, rst)
}

// B2B Period Return
// @Summary Period return values for provided symbol
// @Tags 		returns
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string true "ETF symbol: QQQ"
// @Param       from 	query 	string	true "from: 2020-01-02"
// @Param       to 	query 	string	true "to: 2022-01-02"
// @Security  ApiKeyAuth
// @Router 		/returns/period/{symbol} [get]
// @Success 	200 {object} models.ReturnsPeriod
func GetPeriodReturns(c *gin.Context) {
	query := c.Request.URL.Query()
	syms, err1 := c.Params.Get("symbol")
	if err1 == false {
		c.JSON(http.StatusBadRequest, gin.H{"message": fmt.Errorf("Only us domicile").Error()})
		return
	}

	// TODO delete later (not batch)
	syms_slice := []string{syms}
	if utils.CheckValidETFSymbols(syms_slice) == false {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Invalid symbol"})
		return
	}

	symbols := strings.Split(syms, ",")
	fromQuery := query.Get("from")
	if fromQuery == "" {
		c.JSON(http.StatusBadRequest, gin.H{"message": "no from param"})
		return
	}
	toQuery := query.Get("to")
	if toQuery == "" {
		c.JSON(http.StatusBadRequest, gin.H{"message": "no to param"})
		return
	}
	from, err2 := time.Parse("2006-01-02", fromQuery)
	if err2 != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err2.Error()})
		return
	}
	to, err3 := time.Parse("2006-01-02", toQuery)
	if err3 != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err3.Error()})
		return
	}
	if from.After(to) {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Invalid date param"})
		return
	}

	histMap := getHistoricalPricesRange(symbols, from.AddDate(0, 0, -7), to)

	var rst []models.ReturnsPeriod
	data := getFirstTradingDate(syms)
	var startDates = make(map[string]time.Time)
	for _, v := range data {
		if v.Edges.LinstockStocksEtfdetail != nil {
			startDates[v.Symbol] = v.Edges.LinstockStocksEtfdetail.FirstTradingDate
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"message": "No First Trading Date"})
			return
		}
	}

	for _, v := range symbols {
		curHist := histMap[v]
		var fromPrice *models.HistoricalPrice

		switch {
		case startDates[v].After(to):
			c.JSON(http.StatusBadRequest, gin.H{"message": "Invalid from or to param"})
			return
		case startDates[v].After(from):
			fromPrice = FindHistoricalPriceFull(curHist, startDates[v].Format("2006-01-02"), true)
		default:
			fromPrice = FindHistoricalPriceFull(curHist, from.Format("2006-01-02"), false)
		}

		toPrice := FindHistoricalPriceFull(curHist, to.Format("2006-01-02"), false)

		rst = append(rst, models.ReturnsPeriod{
			Symbol:              v,
			ReturnAdjClose:      utils.CalculateCumulativeReturns(fromPrice.Close, toPrice.Close),
			ReturnFullyAdjClose: utils.CalculateCumulativeReturns(fromPrice.AdjustedClose, toPrice.AdjustedClose),
		})
	}

	c.JSON(http.StatusOK, rst[0])
}

// B2B Returns Rank
// @Summary ETF 수익률 랭킹
// @Tags 		returns
// @Accept  	json
// @Produce  	json
// @Param		period query string true "기간 (period): 1d, 1m, 3m, 1y, 5y"
// @Param		sec_type query string true "ETF: EF, ETN: EN, SSEF: SSEF"
// @Security	ApiKeyAuth
// @Router 		/returns/rank [get]
// @Success 	200 {object} []models.ReturnsRank
func GetReturnsRank(c *gin.Context) {
	query := c.Request.URL.Query()
	period := query.Get("period")
	if period == "" {
		c.JSON(http.StatusBadRequest, gin.H{"message": "No period"})
		return
	}
	// var rst []models.ReturnsRank
	secType := query.Get("sec_type")
	if secType == "" {
		c.JSON(http.StatusBadRequest, gin.H{"message": "No Sec type"})
		return
	}
	sec_type := strings.Split(secType, ",")
	for i, v := range sec_type {
		switch v {
		case "EF", "EN", "SSEF":
			continue
		case "ef", "en", "ssef":
			sec_type[i] = strings.ToUpper(v)
		default:
			c.JSON(http.StatusBadRequest, gin.H{"message": "Incorrect Sec Type"})
			return
		}
	}

	switch period {
	case "1d":
		period = "bf_1day"
	case "1w":
		period = "bf_1week"
	case "1m":
		period = "bf_1month"
	case "3m":
		period = "bf_3month"
	case "1y":
		period = "bf_1year"
	case "5y":
		period = "bf_5year"
	default:
		c.JSON(http.StatusBadRequest, gin.H{"message": "Incorrect period"})
		return
	}

	db := db.Client()
	defer db.Close()
	symbols := db.LinstockStocksSymbol.
		Query().
		Where(linstockstockssymbol.Domicile("us"),
			linstockstockssymbol.SecTypeIn(sec_type...)).
		Select(
			linstockstockssymbol.FieldSymbol,
		).
		StringsX(context.Background())

	redis := redisClient.Connect()
	defer redis.Close()
	cpRanks, err := redisClient.GetCpRanksWithSymbolWithCp(redis, symbols, period, "descending", 100, false)
	if err != nil {
		panic("Redis client is down")
	}
	symbols = []string{}
	rstMap := make(map[string]models.ReturnsRank)
	for ind, v := range cpRanks {
		rstMap[v.Symbol] = models.ReturnsRank{
			Symbol: v.Symbol,
			Return: utils.KeepDecimal(100*v.Cp, 2),
			Rank:   int64(ind) + 1,
		}

		symbols = append(symbols, v.Symbol)
	}

	symbolsDetails := db.LinstockStocksSymbol.
		Query().
		Where(
			linstockstockssymbol.SymbolIn(symbols...)).
		WithLinstockStocksEtfdetail().
		Select(
			linstockstockssymbol.FieldSymbol,
			linstockstockssymbol.FieldNameKo,
			linstockstockssymbol.FieldLogoImg,
			linstockstockssymbol.FieldPrice,
		).
		AllX(context.Background())

	var wg sync.WaitGroup
	wg.Add(len(cpRanks))
	for _, v := range symbolsDetails {
		go func(v *ent.LinstockStocksSymbol) {
			defer wg.Done()
			rankMap := rstMap[v.Symbol]
			rankMap.LeveragePower = v.Edges.LinstockStocksEtfdetail.LeveragePower
			rankMap.NameKo = v.NameKo
			rankMap.LogoImg = v.LogoImg
			rankMap.Price = utils.KeepDecimal(v.Price, 2)
			rstMap[v.Symbol] = rankMap
		}(v)
	}
	wg.Wait()

	if config.MarketOpenStatus == 1 || config.MarketOpenStatus == 2 {
		var wg sync.WaitGroup
		wg.Add(len(cpRanks))
		for _, symbol := range symbols {
			go func(symbol string) {
				defer wg.Done()
				price := redisClient.GetRealtimePrice(redis, symbol)
				if price == 0 {
					return
				}
				rankMap := rstMap[symbol]
				rankMap.Price = utils.KeepDecimal(price, 2)
				rstMap[symbol] = rankMap
			}(symbol)
		}
		wg.Wait()
	}
	rst := make([]models.ReturnsRank, 0, len(rstMap))

	for _, details := range rstMap {
		rst = append(rst, details)
	}
	sort.Slice(rst, func(i, j int) bool {
		return rst[i].Rank < rst[j].Rank
	})
	c.JSON(http.StatusOK, rst)
}
