package handlers

import (
	"context"
	"ezar-b2b-api/ent"
	"ezar-b2b-api/ent/linstockstocksetfdetail"
	"ezar-b2b-api/ent/linstockstockspubetfcategory"
	"ezar-b2b-api/ent/linstockstockssymbol"
	"ezar-b2b-api/pkg/core/db"
	"ezar-b2b-api/pkg/models"
	"ezar-b2b-api/pkg/utils"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

// List of ETF Category Router
// @Summary    	GET list of ETF category
// @Tags 	   	category
// @Description List of Categories
// @Accept  	json
// @Produce  	json
// @Param     feature query string false "feature"
// @Param     end_user query string false "end_user"
// @Param     domicile query string false "domicile"
// @Security  ApiKeyAuth
// @Router 		/categories	[get]
// @Success 	200 {object} models.Category
func GetCategoryList(c *gin.Context) {
	feature := c.Request.URL.Query().Get("feature")
	endUser := c.Request.URL.Query().Get("end_user")
	domicile := c.Request.URL.Query().Get("domicile")

	if domicile != "us" {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("only us domicile"))
		return
	}

	db := db.Client()
	defer db.Close()
	ctx := context.Background()
	queryset := db.LinstockStocksPubetfcategory.Query().
		Where(linstockstockspubetfcategory.Domicile(domicile))
	if len(feature) != 0 {
		queryset = queryset.Where(
			linstockstockspubetfcategory.Feature(feature),
		)
	}

	if len(endUser) != 0 {
		queryset = queryset.
			Where(
				linstockstockspubetfcategory.EndUser(endUser),
			)
	}
	data, err := GetQuery(queryset).All(ctx)

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	rst := CategoryListData(data)

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	utils.Response(c, rst, "category")
}

func CategoryListData(data []*ent.LinstockStocksPubetfcategory) []*models.Category {
	rst := []*models.Category{}
	for _, row := range data {
		isAscending := false
		if row.SortingTag == "ascending" {
			isAscending = true
		}
		if row.DeletedAt.IsZero() == false {
			continue
		}
		rst = append(rst, &models.Category{
			ID:            int64(row.ID),
			TitleKo:       row.TitleKo,
			DescriptionKo: row.DescriptionKo,
			Priority:      int64(row.Priority),
			EtfList:       MapEtfList(row.Edges.LinstockStocksPubetfcategorySymbols, true, isAscending),
			Domicile:      row.Domicile,
			ViewsChange:   row.ViewsChange,
			CategoryImg:   utils.AddImageStorage(row.TitleImg),
			Feature:       row.Feature,
		})
	}
	sort.Slice(rst, func(a, b int) bool {
		return rst[a].ViewsChange > rst[b].ViewsChange
	})

	return rst
}

// A ETF Category Router
// @Summary    	GET  ETF category
// @Tags 	   	category
// @Description Category details
// @Accept  	json
// @Produce  	json
// @Param       id 	path int false  "Servercategory by id"
// @Security  ApiKeyAuth
// @Router 		/categories/{id}	[get]
// @Success 	200 {object} models.Category
func GetCategoryDetail(c *gin.Context) {
	db := db.Client()
	defer db.Close()
	ctx := context.Background()
	catId, _ := c.Params.Get("id")
	idx, err := strconv.Atoi(catId)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	queryset := db.LinstockStocksPubetfcategory.Query().Where(linstockstockspubetfcategory.ID(int32(idx)))
	data, err := GetQuery(queryset).Only(ctx)

	if data == nil {
		c.JSON(http.StatusNotFound, err)
		return
	}
	isAscending := false
	if data.SortingTag == "ascending" {
		isAscending = true
	}

	rst := models.Category{
		ID:            int64(data.ID),
		TitleKo:       data.TitleKo,
		CategoryImg:   utils.AddImageStorage(data.TitleImg),
		DescriptionKo: data.DescriptionKo,
		Priority:      int64(data.Priority),
		EtfList:       MapEtfList(data.Edges.LinstockStocksPubetfcategorySymbols, false, isAscending),
		Domicile:      data.Domicile,
		Feature:       data.Feature,
	}

	utils.Response(c, rst, "category_id")

	db.LinstockStocksPubetfcategory.UpdateOneID(int32(idx)).
		AddViews(1).
		ExecX(ctx)

	db.LinstockStocksPubetfcategory.UpdateOneID(int32(idx)).
		SetViewsChange((data.Views + 1) - data.PrevViews).
		ExecX(ctx)
}

func GetQuery(queryset *ent.LinstockStocksPubetfcategoryQuery) *ent.LinstockStocksPubetfcategoryQuery {
	return queryset.Where(linstockstockspubetfcategory.DeletedAtIsNil()).
		Select(
			linstockstockspubetfcategory.FieldID,
			linstockstockspubetfcategory.FieldTitleKo,
			linstockstockspubetfcategory.FieldDescriptionKo,
			linstockstockspubetfcategory.FieldPriority,
			linstockstockspubetfcategory.FieldTitleImg,
			linstockstockspubetfcategory.FieldDomicile,
			linstockstockspubetfcategory.FieldDataTag,
			linstockstockspubetfcategory.FieldSortingTag,
			linstockstockspubetfcategory.FieldDeletedAt,
			linstockstockspubetfcategory.FieldIsRandom,
			linstockstockspubetfcategory.FieldViews,
			linstockstockspubetfcategory.FieldViewsChange,
			linstockstockspubetfcategory.FieldPrevViews,
			linstockstockspubetfcategory.FieldFeature,
		).
		WithLinstockStocksPubetfcategorySymbols(func(q *ent.LinstockStocksPubetfcategorySymbolQuery) {
			q.WithLinstockStocksSymbol(func(q *ent.LinstockStocksSymbolQuery) {
				q.Where(linstockstockssymbol.Status("active"), linstockstockssymbol.DeletedAtIsNil()).
					Select(
						linstockstockssymbol.FieldID,
						linstockstockssymbol.FieldNameKo,
						linstockstockssymbol.FieldDescriptionKo,
						linstockstockssymbol.FieldChangePercent,
						linstockstockssymbol.FieldSymbol,
						linstockstockssymbol.FieldLogoImg,
					).
					WithLinstockStocksEtfdetail(func(q *ent.LinstockStocksEtfdetailQuery) {
						q.Select(
							linstockstocksetfdetail.FieldSymbolID,
							linstockstocksetfdetail.FieldLeveragePower,
						)
					})
			})
		})
}

func MapEtfList(items []*ent.LinstockStocksPubetfcategorySymbol, isMax bool, isAscending bool) []*models.EtfItem {
	rst := []*models.EtfItem{}
	items = utils.Filter(items, func(t *ent.LinstockStocksPubetfcategorySymbol) bool {
		return t.Edges.LinstockStocksSymbol != nil
	})
	var symbols []string
	for _, symb := range items {
		symbols = append(symbols, symb.Edges.LinstockStocksSymbol.Symbol)
	}
	data := getFirstTradingDate(strings.Join(symbols, ","))
	for _, symbol := range data {
		if symbol != nil && symbol.Edges.LinstockStocksEtfdetail != nil {
			rst = append(rst, &models.EtfItem{
				ID:               int64(symbol.ID),
				Symbol:           symbol.Symbol,
				NameKo:           symbol.NameKo,
				LeveragePower:    symbol.Edges.LinstockStocksEtfdetail.LeveragePower,
				FirstTradingDate: symbol.Edges.LinstockStocksEtfdetail.FirstTradingDate.Format("2006-01-02"),
				DescriptionKo:    symbol.DescriptionKo,
			})
		}
	}
	sort.Slice(rst, func(i, j int) bool {
		return rst[i].ID < rst[j].ID
	})

	return rst
}
