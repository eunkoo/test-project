package handlers

import (
	"context"
	"ezar-b2b-api/ent/linstockstockssymbol"
	"ezar-b2b-api/pkg/core/db"
	"ezar-b2b-api/pkg/models"
	"ezar-b2b-api/pkg/utils"

	"net/http"

	"github.com/gin-gonic/gin"
)

// B2B Snp500list
// @Summary Snp500list values for provided symbol
// @Tags 		constituents
// @Accept  	json
// @Produce  	json
// @Security  ApiKeyAuth
// @Router 		/snp500list [get]
// @Success 	200 {object} models.Constituents
func GetSnp500list(c *gin.Context) {
	db := db.Client()
	defer db.Close()

	data := db.LinstockStocksSp500.Query().AllX(context.TODO())

	var rst []*models.Constituents
	for _, sp := range data {
		query, _ := db.LinstockStocksSymbol.Query().Where(linstockstockssymbol.Symbol(sp.ID)).First(context.Background())
		rst = append(rst, &models.Constituents{
			Symbol:    sp.ID,
			Name:      sp.Name,
			Name_Ko:   sp.NameKo,
			Isin:      query.Isin,
			Sector_ko: utils.SectorDict[sp.Sector],
			// Date_First_added: sp.CreatedAt.Format("2006-01-02"),
		})
	}
	c.JSON(http.StatusOK, rst)
}

// B2B Nasdaq100list
// @Summary Nasdaq100list values for provided symbol
// @Tags 		constituents
// @Accept  	json
// @Produce  	json
// @Security  ApiKeyAuth
// @Router 		/nasdaq100list [get]
// @Success 	200 {object} models.Constituents
func GetNasdaq100list(c *gin.Context) {
	db := db.Client()
	defer db.Close()

	data := db.LinstockStocksNasdaq.Query().AllX(context.TODO())

	var rst []*models.Constituents
	for _, sp := range data {
		query, _ := db.LinstockStocksSymbol.Query().Where(linstockstockssymbol.Symbol(sp.ID)).First(context.Background())
		rst = append(rst, &models.Constituents{
			Symbol:    sp.ID,
			Name:      sp.Name,
			Name_Ko:   sp.NameKo,
			Isin:      query.Isin,
			Sector_ko: utils.SectorDict[sp.Sector],
			// Date_First_added: sp.CreatedAt.Format("2006-01-02"),
		})
	}
	c.JSON(http.StatusOK, rst)
}

// B2B DowJonesList
// @Summary DowJonesList values for provided symbol
// @Tags 		constituents
// @Accept  	json
// @Produce  	json
// @Security  ApiKeyAuth
// @Router 		/dowjoneslist [get]
// @Success 	200 {object} models.Constituents
func GetDowJoneslist(c *gin.Context) {
	db := db.Client()
	defer db.Close()

	data := db.LinstockStocksDowjone.Query().AllX(context.TODO())

	var rst []*models.Constituents
	for _, sp := range data {
		query, _ := db.LinstockStocksSymbol.Query().Where(linstockstockssymbol.Symbol(sp.ID)).First(context.Background())
		rst = append(rst, &models.Constituents{
			Symbol:    sp.ID,
			Name:      sp.Name,
			Name_Ko:   sp.NameKo,
			Isin:      query.Isin,
			Sector_ko: utils.SectorDict[sp.Sector],
			// Date_First_added: sp.CreatedAt.Format("2006-01-02"),
		})
	}
	c.JSON(http.StatusOK, rst)
}
