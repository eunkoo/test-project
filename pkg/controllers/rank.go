package handlers

import (
	"context"
	"ezar-b2b-api/ent"
	"ezar-b2b-api/ent/linstockstockssymbol"
	"ezar-b2b-api/pkg/models"
	"ezar-b2b-api/pkg/utils"
)

type Rank struct{}

func (r *Rank) GetProfit1wRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfprofit1wrank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetProfit1mRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfprofit1mrank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetProfit3mRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfprofit3mrank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetProfit6mRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfprofit6mrank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetProfit1yRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfprofit1yrank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetProfit5yRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfprofit5yrank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetLoss1wRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfloss1wrank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetLoss1mRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfloss1mrank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetLoss3mRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfloss3mrank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetLoss6mRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfloss6mrank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetLoss1yRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfloss1yrank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetLoss5yRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfloss5yrank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetMarketcapRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfmarketcaprank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetVolume1dRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfvolume1drank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetVolume10dRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfvolume10drank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetVolume1mRankData(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfvolume1mrank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetBigPriceRank(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfbigpricerank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func (r *Rank) GetSmallPriceRank(db *ent.Client, ctx context.Context, domicile string) []*models.Rank {
	client := db.LinstockStocksEtfsmallpricerank.Query()
	queryset := client.WithLinstockStocksSymbol(getRankEdge).AllX(ctx)
	var rst []*models.Rank
	for _, v := range queryset {
		if v.Edges.LinstockStocksSymbol.Domicile != domicile {
			continue
		}
		rst = append(rst, &models.Rank{
			Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
			KoName:        v.Edges.LinstockStocksSymbol.NameKo,
			Value:         utils.KeepDecimal(v.Value*100, 2),
			Rank:          int64(v.Rank),
			LeveragePower: v.Edges.LinstockStocksSymbol.Edges.LinstockStocksEtfdetail.LeveragePower,
		})
	}
	return rst
}

func getRankEdge(q *ent.LinstockStocksSymbolQuery) {
	q.Select(
		linstockstockssymbol.FieldSymbol,
		linstockstockssymbol.FieldNameKo,
		linstockstockssymbol.FieldDomicile,
	).WithLinstockStocksEtfdetail()
}
