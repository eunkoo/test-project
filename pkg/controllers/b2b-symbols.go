package handlers

import (
	"context"
	"ezar-b2b-api/ent"
	"ezar-b2b-api/ent/linstockstockssymbol"
	config "ezar-b2b-api/pkg/core/configs"
	"ezar-b2b-api/pkg/core/db"
	"ezar-b2b-api/pkg/models"
	"ezar-b2b-api/pkg/utils"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"entgo.io/ent/dialect/sql"
	"github.com/gin-gonic/gin"
)

// Stock List Router
// @Summary   	Retrieve Stock logo image.
// @Tags 	 	symbols
// @Description Stock Logo Image.
// @Accept  	json
// @Produce  	image/png
// @Param       symbol 	path 	string	false  "SPY,QQQ"
// @Router 		/symbols/{symbol}/images [get]
// @Security  ApiKeyAuth
// @Success 	302
func GetSymbolImage(c *gin.Context) {
	symbol, _ := c.Params.Get("symbol")
	path := []string{
		config.IMAGE_STORAGE,
		"logo",
		strings.Join([]string{symbol, "png"}, "."),
	}
	location := strings.Join(path, "/")
	resp, err := http.Head(location)
	if err != nil {
		c.JSON(http.StatusBadRequest, nil)
		return
	}
	if resp.StatusCode != 200 {
		path := []string{
			config.IEX_IMAGE_STORAGE,
			"iex",
			"api",
			"logos",
			strings.Join([]string{symbol, "png"}, "."),
		}
		location := strings.Join(path, "/")
		resp, err := http.Head(location)
		if err != nil || resp.StatusCode != 200 {
			c.File("./lib/assets/images/no.png")
			return
		}
	}
	c.Redirect(http.StatusFound, location)
}

// Stock List Router
// @Summary
// @Tags 	 	symbols
// @Description
// @Accept  	json
// @Produce  	json
// @Param       spec 	query 	string	false "spec"
// @Param       isin 	query 	string	false "isin"
// @Param       cusip 	query 	string	false "cusip"
// @Param       type 	query 	string	false "type"
// @Param       domicile 	query 	string	false "domicile"
// @Param       sec_type 	query 	boolean false "sec_type"
// @Param       legal_type 	query 	string 	false "filter with legal_type: PTP,GNR"
// @Security  ApiKeyAuth
// @Router 		/symbols [get]
func GetSymbolList(c *gin.Context) {

	db := db.Client()
	defer db.Close()
	ctx := context.Background()
	var rst []any
	specCond := c.Request.URL.Query().Get("spec")
	isinCond := c.Request.URL.Query().Get("isin")
	cusipCond := c.Request.URL.Query().Get("cusip")
	domicile := c.Request.URL.Query().Get("domicile")
	if domicile == "" {
		domicile = "us"
	}
	if len(specCond) == 0 && len(isinCond) == 0 && len(cusipCond) == 0 {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("need to get spec or isin or cusip"))
		return
	}
	sec_type := c.Request.URL.Query().Get("sec_type")
	queryset := db.LinstockStocksSymbol.Query().Where(
		linstockstockssymbol.Status("active"),
		linstockstockssymbol.Domicile(domicile),
		linstockstockssymbol.DeletedAtIsNil(),
	)
	if len(sec_type) > 0 {
		queryset = symbolSecSelector(queryset, sec_type)
	}

	legalTypeCond := c.Request.URL.Query().Get("legal_type")
	queryset = symbolLegalTypeSelector(queryset, legalTypeCond)

	typeCond := c.Request.URL.Query().Get("type")
	queryset = symbolSpecSelector(queryset, specCond, isinCond, cusipCond)
	switch typeCond {
	case "simple":
		rst = symbolSimpleEndware(c, ctx, queryset)
		break
	case "etf_detail":
		rst = symbolEtfDetailEndware(c, ctx, queryset)
		break
	default:
		rst = symbolDefaultEndware(c, ctx, queryset)
		break
	}
	utils.Response(c, rst, "symbols")
}

// ETF Detail List
// @Summary   	GET List of ETF details
// @Tags 	  	etf_details
// @Description
// @Accept  	json
// @Produce   	json
// @Param       spec 	query 	string	false "spec"
// @Param       isin 	query 	string	false "isin"
// @Param       cusip 	query 	string	false "cusip"
// @Security  ApiKeyAuth
// @Router 		/etf_details [get]
func GetEtfDetails(c *gin.Context) {
	db := db.Client()
	defer db.Close()
	ctx := context.Background()
	var rst []any
	specCond := c.Request.URL.Query().Get("spec")
	isinCond := c.Request.URL.Query().Get("isin")
	cusipCond := c.Request.URL.Query().Get("cusip")
	if len(specCond) == 0 && len(isinCond) == 0 && len(cusipCond) == 0 {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("need to get spec or isin or cusip"))
		return
	}

	if len(specCond) == 0 {
		c.JSON(http.StatusBadRequest, fmt.Errorf("need to get spec"))
		return
	}
	queryset := db.LinstockStocksSymbol.
		Query().
		Where(linstockstockssymbol.Domicile("us"),
			linstockstockssymbol.Status("active"),
			linstockstockssymbol.DeletedAtIsNil())
	queryset = symbolSpecSelector(queryset, specCond, isinCond, cusipCond)
	queryset = queryset.Where(
		linstockstockssymbol.HasLinstockStocksEtfdetailWith(),
	)
	rst = etfDetailEndware(c, ctx, queryset)
	if len(rst) == 0 {
		c.JSON(http.StatusNotFound, nil)
	}
	utils.Response(c, rst, "etf_details")
}

// ETF Rank Router
// @Summary  	ETF rank
// @Tags 	 	symbols
// @Description ETF
// @Accept  	json
// @Produce  	json
// @Param       path 	path 	string	false  "profit, loss, volume, price, marketcap "
// @Param       range 	query 	string	false  "profit, loss(1w, 1m, 3m, 6m, 1y, 5y), volume(1d, 10d, 1m)"
// @Param       order 	query 	string	false  "price(desc, asc)"
// @Param       domicile 	query 	string	false  "domicile(us, kr, ir)"
// @Security  ApiKeyAuth
// @Router 		/symbols/ranks/{path} [get]
// @Success 	200 {object} models.Rank
func GetEtfRanks(c *gin.Context) {
	var rst []*models.Rank
	db := db.Client()
	defer db.Close()
	ctx := context.Background()
	rankGroup := c.Params.ByName("group")
	period := c.Request.URL.Query().Get("range")
	rank := Rank{}
	order := c.Request.URL.Query().Get("order")
	domicile := c.Request.URL.Query().Get("domicile")
	if domicile == "" {
		domicile = "us"
	}
	switch rankGroup {
	case "profit":
		switch period {
		case "1w":
			rst = rank.GetProfit1wRankData(db, ctx, domicile)
			break
		case "1m":
			rst = rank.GetProfit1mRankData(db, ctx, domicile)
			break
		case "3m":
			rst = rank.GetProfit3mRankData(db, ctx, domicile)
			break
		case "6m":
			rst = rank.GetProfit6mRankData(db, ctx, domicile)
			break
		case "1y":
			rst = rank.GetProfit1yRankData(db, ctx, domicile)
			break
		case "5y":
			rst = rank.GetProfit5yRankData(db, ctx, domicile)
			break
		}
		break

	case "loss":
		switch period {
		case "1w":
			rst = rank.GetLoss1wRankData(db, ctx, domicile)
			break
		case "1m":
			rst = rank.GetLoss1mRankData(db, ctx, domicile)
			break
		case "3m":
			rst = rank.GetLoss3mRankData(db, ctx, domicile)
			break
		case "6m":
			rst = rank.GetLoss6mRankData(db, ctx, domicile)
			break
		case "1y":
			rst = rank.GetLoss1yRankData(db, ctx, domicile)
			break
		case "5y":
			rst = rank.GetLoss5yRankData(db, ctx, domicile)
			break
		}
		break

	case "volume":
		switch period {
		case "1d":
			rst = rank.GetVolume1dRankData(db, ctx, domicile)
			break
		case "10d":
			rst = rank.GetVolume10dRankData(db, ctx, domicile)
			break
		case "1m":
			rst = rank.GetVolume1mRankData(db, ctx, domicile)
			break
		}
		break

	case "marketcap":
		rst = rank.GetMarketcapRankData(db, ctx, domicile)
		break

	case "price":
		switch order {
		case "asc":
			rst = rank.GetSmallPriceRank(db, ctx, domicile)
			break
		case "desc":
			rst = rank.GetBigPriceRank(db, ctx, domicile)
		}
		break
	}
	c.JSON(http.StatusOK, rst)
}

// SSEF Symbols
// @Summary  	SSEF Symbols
// @Tags 	 	symbols
// @Description SSEF
// @Accept  	json
// @Produce  	json
// @Security  ApiKeyAuth
// @Router 		/symbols/ssef [get]
// @Success 	200 {object} models.SSEF
func GetSSEF(c *gin.Context) {
	db := db.Client()
	defer db.Close()
	query := db.LinstockStocksSymbol.Query().Where(linstockstockssymbol.Status("active"), linstockstockssymbol.SecType("SSEF")).WithLinstockStocksEtfdetail().AllX(context.Background())
	var rst []*models.SSEF
	for _, v := range query {
		rst = append(rst, &models.SSEF{
			Symbol:        v.Symbol,
			Name:          v.Name,
			Name_ko:       v.NameKo,
			IsInverse:     v.Edges.LinstockStocksEtfdetail.IsInverse,
			IsLeveraged:   v.Edges.LinstockStocksEtfdetail.IsLeveraged,
			LeveragePower: v.Edges.LinstockStocksEtfdetail.LeveragePower,
			OriginEtf:     v.Edges.LinstockStocksEtfdetail.OriginEtf,
		})
	}
	utils.Response(c, rst, "ssef")
}

func symbolSpecSelector(
	queryset *ent.LinstockStocksSymbolQuery,
	specCond string,
	isinCond string,
	cusipCond string,
) *ent.LinstockStocksSymbolQuery {
	if specCond != "all" {
		spec := utils.AnyAllDrivVal(strings.Split(specCond, ","))
		isin := utils.AnyAllDrivVal(strings.Split(isinCond, ","))
		cusip := utils.AnyAllDrivVal(strings.Split(cusipCond, ","))
		return queryset.Where(func(s *sql.Selector) {
			s.Where(sql.InValues(linstockstockssymbol.FieldSymbol, spec...)).Or().
				Where(sql.InValues(linstockstockssymbol.FieldIsin, isin...)).Or().
				Where(sql.InValues(linstockstockssymbol.FieldCusip, cusip...))
		})
	} else {
		return queryset
	}
}

func etfDetailEndware(c *gin.Context, ctx context.Context, queryset *ent.LinstockStocksSymbolQuery) []any {
	rst := []*models.EtfDetail_b2b{}
	data := queryset.WithLinstockStocksEtfdetail().AllX(ctx)

	for _, v := range data {
		etf_detail := models.EtfDetail_b2b{}
		if v.DeletedAt.IsZero() == false {
			continue
		}
		if v.Edges.LinstockStocksEtfdetail != nil {
			etf_detail = models.EtfDetail_b2b{
				Symbol:            v.Symbol,
				NameKo:            v.NameKo,
				Name:              v.Name,
				Cons:              strings.Split(v.Edges.LinstockStocksEtfdetail.ConsKo, "@"),
				Pros:              strings.Split(v.Edges.LinstockStocksEtfdetail.ProsKo, "@"),
				IndexCriteria:     strings.Split(v.Edges.LinstockStocksEtfdetail.IndexCriteria, "@"),
				SelectionCriteria: strings.Split(v.Edges.LinstockStocksEtfdetail.SelectionCriteria, "@"),
				WeightingCriteria: strings.Split(v.Edges.LinstockStocksEtfdetail.WeightingCriteria, "@"),
				LeveragePower:     v.Edges.LinstockStocksEtfdetail.LeveragePower,
				IsInverse:         v.Edges.LinstockStocksEtfdetail.IsInverse,
				IsLeveraged:       v.Edges.LinstockStocksEtfdetail.IsLeveraged,
				OriginEtf:         v.Edges.LinstockStocksEtfdetail.OriginEtf,
				Expense:           v.Edges.LinstockStocksEtfdetail.Expense,
				BenchMark:         v.Edges.LinstockStocksEtfdetail.BenchMark,
				FirstTradingDate:  v.Edges.LinstockStocksEtfdetail.FirstTradingDate.Format("2006-01-02"),
				HomePage:          v.Edges.LinstockStocksEtfdetail.HomePage,
				AssetManager:      v.Edges.LinstockStocksEtfdetail.AssetManager,
			}
		}
		rst = append(rst, &etf_detail)
	}
	return utils.AnyAll(rst)
}

func symbolSecSelector(queryset *ent.LinstockStocksSymbolQuery, SecType string) *ent.LinstockStocksSymbolQuery {

	sec_type := utils.AnyAllDrivVal(strings.Split(SecType, ","))

	queryset = queryset.Where(func(s *sql.Selector) {
		s.Where(sql.InValues(linstockstockssymbol.FieldSecType, sec_type...))
	})
	return queryset
}

func symbolLegalTypeSelector(queryset *ent.LinstockStocksSymbolQuery, legalTypeCond string) *ent.LinstockStocksSymbolQuery {
	if legalTypeCond == "" {
		return queryset
	}
	types := utils.AnyAllDrivVal(strings.Split(legalTypeCond, ","))
	return queryset.Where(func(s *sql.Selector) {
		s.Where(sql.InValues(linstockstockssymbol.FieldLegalType, types...))
	})
}

func symbolIsEtnSelector(queryset *ent.LinstockStocksSymbolQuery, isEtnCond string) (*ent.LinstockStocksSymbolQuery, error) {
	if len(isEtnCond) != 0 {
		is_etn, err := strconv.ParseBool(isEtnCond)
		if err != nil {
			return nil, err
		}
		if is_etn == true {
			queryset = queryset.Where(linstockstockssymbol.SecType("EF"))
		}
	}
	return queryset, nil
}

func symbolSimpleEndware(c *gin.Context, ctx context.Context, queryset *ent.LinstockStocksSymbolQuery) []any {
	rst := []*models.SymbolSimple{}
	queryset.Select(
		linstockstockssymbol.FieldID,
		linstockstockssymbol.FieldSymbol,
		linstockstockssymbol.FieldNameKo,
	).Scan(ctx, &rst)

	return utils.AnyAll(rst)
}

func symbolDefaultEndware(c *gin.Context, ctx context.Context, queryset *ent.LinstockStocksSymbolQuery) []any {
	rst := []*models.Symbol{}
	data := queryset.WithLinstockStocksEtfdetail().AllX(ctx)
	for _, v := range data {
		if v.DeletedAt.IsZero() == false {
			continue
		}
		var first_date interface{} = nil
		if v.Edges.LinstockStocksEtfdetail != nil && v.Edges.LinstockStocksEtfdetail.FirstTradingDate.IsZero() == false {
			first_date = v.Edges.LinstockStocksEtfdetail.FirstTradingDate.Format("2006-01-02")
		}
		el := models.Symbol{
			ID:               int64(v.ID),
			Tag:              strings.Split(v.Tag, "@"),
			Symbol:           v.Symbol,
			Isin:             v.Isin,
			Cusip:            v.Cusip,
			Name:             v.Name,
			NameKo:           v.NameKo,
			SecType:          v.SecType,
			OrderRank:        int64(v.OrderRank),
			DescriptionKo:    v.DescriptionKo,
			FirstTradingDate: first_date,
			LogoImg:          utils.AddImageStorage(v.LogoImg),
			Status:           v.Status,
			Domicile:         v.Domicile,
		}
		rst = append(rst, &el)
	}
	return utils.AnyAll(rst)
}

func symbolEtfDetailEndware(c *gin.Context, ctx context.Context, queryset *ent.LinstockStocksSymbolQuery) []any {
	rst := []*models.SymbolWithEtfDetail{}
	data := queryset.WithLinstockStocksEtfdetail().AllX(ctx)
	for _, v := range data {
		etf_detail := models.EtfDetail_b2b{}
		if v.DeletedAt.IsZero() == false {
			continue
		}
		if v.Edges.LinstockStocksEtfdetail != nil {
			etf_detail = models.EtfDetail_b2b{
				Symbol:            v.Symbol,
				NameKo:            v.NameKo,
				Name:              v.Name,
				Cons:              strings.Split(v.Edges.LinstockStocksEtfdetail.ConsKo, "@"),
				Pros:              strings.Split(v.Edges.LinstockStocksEtfdetail.ProsKo, "@"),
				IndexCriteria:     strings.Split(v.Edges.LinstockStocksEtfdetail.IndexCriteria, "@"),
				SelectionCriteria: strings.Split(v.Edges.LinstockStocksEtfdetail.SelectionCriteria, "@"),
				WeightingCriteria: strings.Split(v.Edges.LinstockStocksEtfdetail.WeightingCriteria, "@"),
				LeveragePower:     v.Edges.LinstockStocksEtfdetail.LeveragePower,
				IsInverse:         v.Edges.LinstockStocksEtfdetail.IsInverse,
				IsLeveraged:       v.Edges.LinstockStocksEtfdetail.IsLeveraged,
				OriginEtf:         v.Edges.LinstockStocksEtfdetail.OriginEtf,
				Expense:           v.Edges.LinstockStocksEtfdetail.Expense,
				BenchMark:         v.Edges.LinstockStocksEtfdetail.BenchMark,
				FirstTradingDate:  v.Edges.LinstockStocksEtfdetail.FirstTradingDate.Format("2006-01-02"),
				HomePage:          v.Edges.LinstockStocksEtfdetail.HomePage,
			}
		}
		rst = append(rst, &models.SymbolWithEtfDetail{
			Symbol: models.Symbol{
				ID:            int64(v.ID),
				Tag:           strings.Split(v.Tag, "@"),
				Symbol:        v.Symbol,
				Isin:          v.Isin,
				Cusip:         v.Cusip,
				Name:          v.Name,
				NameKo:        v.NameKo,
				SecType:       v.SecType,
				OrderRank:     int64(v.OrderRank),
				DescriptionKo: v.DescriptionKo,
				LogoImg:       utils.AddImageStorage(v.LogoImg),
				Status:        v.Status,
				Domicile:      v.Domicile,
			},
			EtfDetail: etf_detail,
		})
	}
	return utils.AnyAll(rst)
}
