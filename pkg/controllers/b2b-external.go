package handlers

import (
	"context"
	"encoding/json"
	"ezar-b2b-api/ent"
	"ezar-b2b-api/ent/linstockstocksetfcountryweight"
	"ezar-b2b-api/ent/linstockstocksetfdetail"
	"ezar-b2b-api/ent/linstockstocksetfholder"
	"ezar-b2b-api/ent/linstockstocksetfsectorweight"
	"ezar-b2b-api/ent/linstockstockshistoricaldividendu"
	"ezar-b2b-api/ent/linstockstockssymbol"
	config "ezar-b2b-api/pkg/core/configs"
	"ezar-b2b-api/pkg/core/db"
	"ezar-b2b-api/pkg/core/logger"
	"ezar-b2b-api/pkg/models"
	"ezar-b2b-api/pkg/utils"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
)

func FetchDividendPolygon(symbol string) *models.HistoricalDividendPolygon {
	var temp *models.HistoricalDividendPolygon
	queryParams := url.Values{}
	queryParams.Add("apiKey", config.POLYGON_API_KEY)
	queryParams.Add("ticker", symbol)
	url := url.URL{
		Scheme:   "https",
		Host:     config.POLYGON_HOST,
		Path:     "/v3/reference/dividends",
		RawQuery: queryParams.Encode(),
	}
	resp, _ := http.Get(url.String())
	data, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	json.Unmarshal(data, &temp)

	return temp
}

func FetchPriceFullPolygon(date time.Time) (*models.HistoricalPricePolygon, time.Time) {
	cur := date
	for {
		var temp *models.HistoricalPricePolygon

		queryParams := url.Values{}
		queryParams.Add("apiKey", config.POLYGON_API_KEY)
		queryParams.Add("adjusted", "true")
		url := url.URL{
			Scheme:   "https",
			Host:     config.POLYGON_HOST,
			Path:     fmt.Sprintf("/v2/aggs/grouped/locale/us/market/stocks/%s", cur.Format("2006-01-02")),
			RawQuery: queryParams.Encode(),
		}
		resp, _ := http.Get(url.String())
		data, _ := ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()

		json.Unmarshal(data, &temp)
		switch temp.Count {
		case 0:
			cur = cur.AddDate(0, 0, -1)
		default:
			return temp, cur
		}
	}
}

func FetchPriceFullEOD(symbol string, startDate string) []byte {
	queryParams := url.Values{}
	queryParams.Add("api_token", config.EOD_API_KEY)
	queryParams.Add("from", startDate)
	queryParams.Add("period", "d")
	queryParams.Add("fmt", "json")
	url := url.URL{
		Scheme:   "https",
		Host:     config.EOD_HOST,
		Path:     fmt.Sprintf("/api/eod/%s.US", symbol),
		RawQuery: queryParams.Encode(),
	}
	resp, _ := http.Get(url.String())
	data, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	return data
}

func FetchPriceFull(query url.Values, path string) []byte {
	query.Add("apikey", config.FMP_API_KEY)
	url := url.URL{
		Scheme:   "https",
		Host:     config.FMP_HOST,
		Path:     fmt.Sprintf("/api/v3/%s", path),
		RawQuery: query.Encode(),
	}
	resp, _ := http.Get(url.String())
	data, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	return data
}

// B2B ALL ETF-holdings
// @Summary ALL ETF-holdings
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Security  ApiKeyAuth
// @Router 		/etf-holdings [get]
// @Success 	200 {object} []models.EtfHoldings
func GetEtfHoldings(c *gin.Context) {
	db := db.Client()
	defer db.Close()
	data := db.LinstockStocksEtfholder.Query().Select(linstockstocksetfholder.FieldEtfHolders).StringsX(context.Background())
	var rst []*models.EtfHoldings
	for _, val := range data {
		var etfHolding models.EtfHoldings
		err := json.Unmarshal([]byte(val), &etfHolding)
		if err != nil {
			logger.SentryLogger(err)
		} else {
			rst = append(rst, &etfHolding)
		}
	}
	utils.Response(c, rst, "etf_holdings")
}

// B2B ETF-holdings
// @Summary ETF-holdings
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string false "ETF symbol: QQQ"
// @Security  ApiKeyAuth
// @Router 		/etf-holdings/{symbol} [get]
// @Success 	200 {object} models.EtfHoldings
func GetEtfHoldingsBySymbol(c *gin.Context) {
	symbol, err1 := c.Params.Get("symbol")
	if !err1 {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("no symbol provided"))
		return

	}
	db := db.Client()
	defer db.Close()
	symbols := strings.Split(symbol, ",")
	data := db.LinstockStocksEtfholder.Query().Where(linstockstocksetfholder.IDIn(symbols...)).Select(linstockstocksetfholder.FieldEtfHolders).StringsX(context.Background())
	var rst []*models.EtfHoldings
	for _, val := range data {
		var etfHolding models.EtfHoldings
		err := json.Unmarshal([]byte(val), &etfHolding)
		if err != nil {
			logger.SentryLogger(err)
		} else {
			rst = append(rst, &etfHolding)
		}
	}
	utils.Response(c, rst, "etf_holdings")
}

// B2B ALL ETF-country-weightings
// @Summary ALL ETF-country-weightings
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Security  ApiKeyAuth
// @Router 		/etf-country-weightings [get]
// @Success 	200 {object} models.CountryWeightings
func GetEtfCountryWeightings(c *gin.Context) {
	db := db.Client()
	defer db.Close()
	data := db.LinstockStocksEtfcountryweight.
		Query().
		Select(linstockstocksetfcountryweight.FieldCountryWeightings).
		StringsX(context.Background())
	var rst []*models.CountryWeightings
	for _, val := range data {
		var CountryWeighting models.CountryWeightings
		err := json.Unmarshal([]byte(val), &CountryWeighting)
		if err != nil {
			logger.SentryLogger(err)
		} else {
			rst = append(rst, &CountryWeighting)
		}
	}
	utils.Response(c, rst, "country_weighting")
}

// B2B ETF-country-weightings
// @Summary ETF-country-weightings
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string false "ETF symbol: QQQ"
// @Security  ApiKeyAuth
// @Router 		/etf-country-weightings/{symbol} [get]
// @Success 	200 {object} models.CountryWeightings
func GetEtfCountryWeightingsBySymbol(c *gin.Context) {

	symbol, err1 := c.Params.Get("symbol")
	if !err1 {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("no symbol provided"))
		return
	}
	symbols := strings.Split(symbol, ",")
	db := db.Client()
	defer db.Close()
	data := db.LinstockStocksEtfcountryweight.
		Query().
		Where(linstockstocksetfcountryweight.IDIn(symbols...)).
		Select(linstockstocksetfcountryweight.FieldCountryWeightings).
		StringsX(context.Background())
	var rst []*models.CountryWeightings
	for _, val := range data {
		var CountryWeighting models.CountryWeightings
		err := json.Unmarshal([]byte(val), &CountryWeighting)
		if err != nil {
			logger.SentryLogger(err)
		} else {
			rst = append(rst, &CountryWeighting)
		}
	}
	utils.Response(c, rst, "country_weighting")
}

// B2B ALL ETF-Sector-Weightings
// @Summary ALL ETF-Sector-Weightings
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Security  ApiKeyAuth
// @Router 		/etf-sector-weightings [get]
// @Success 	200 {object} models.SectorWeightings
func GetEtfSectorWeightings(c *gin.Context) {
	db := db.Client()
	defer db.Close()
	data := db.LinstockStocksEtfsectorweight.
		Query().
		Select(linstockstocksetfsectorweight.FieldSectorWeightings).
		StringsX(context.Background())

	var rst []*models.SectorWeightings
	for _, val := range data {
		var SectorWeightings models.SectorWeightings
		err := json.Unmarshal([]byte(val), &SectorWeightings)
		if err != nil {
			logger.SentryLogger(err)
		} else {
			rst = append(rst, &SectorWeightings)
		}
	}
	utils.Response(c, rst, "sector_weighting")
}

// B2B ETF-Sector-Weightings
// @Summary ETF-Sector-Weightings
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string false "ETF symbol: QQQ"
// @Security  ApiKeyAuth
// @Router 		/etf-sector-weightings/{symbol} [get]
// @Success 	200 {object} models.SectorWeightings
func GetEtfSectorWeightingsBySymbol(c *gin.Context) {
	symbol, err1 := c.Params.Get("symbol")
	if !err1 {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("no symbol provided"))
		return
	}
	symbols := strings.Split(symbol, ",")

	db := db.Client()
	defer db.Close()
	data := db.LinstockStocksEtfsectorweight.
		Query().
		Where(linstockstocksetfsectorweight.IDIn(symbols...)).
		Select(linstockstocksetfsectorweight.FieldSectorWeightings).
		StringsX(context.Background())

	var rst []*models.SectorWeightings
	for _, val := range data {
		var SectorWeightings models.SectorWeightings
		err := json.Unmarshal([]byte(val), &SectorWeightings)
		if err != nil {
			logger.SentryLogger(err)
		} else {
			rst = append(rst, &SectorWeightings)
		}
	}
	utils.Response(c, rst, "sector_weighting")
}

// B2B ETF-Sector-Exposure
// @Summary ETF-Sector-Exposure
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string false "ETF symbol: QQQ"
// @Security  ApiKeyAuth
// @Router 		/etf-stock-exposure/{symbol} [get]
// @Success 	200 {object} models.StockExposure_b2b
func GetEtfSectorExposure(c *gin.Context) {
	query := c.Request.URL.Query()
	query.Add("apikey", config.FMP_API_KEY)
	symbol, err1 := c.Params.Get("symbol")
	if err1 == false {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("No symbol provided"))
		return
	}
	path := fmt.Sprintf("etf-stock-exposure/%s", symbol)
	url := url.URL{
		Scheme:   "https",
		Host:     config.FMP_HOST,
		Path:     fmt.Sprintf("/api/v3/%s", path),
		RawQuery: query.Encode(),
	}
	var original_data []*models.StockExposure
	resp, err := http.Get(url.String())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	json.Unmarshal(data, &original_data)
	symbolToWeightPercent := make(map[string](float64))
	for _, v := range original_data {
		symbolToWeightPercent[v.EtfSymbol] = v.WeightPercentage
	}
	db := db.Client()
	defer db.Close()
	var rst []*models.StockExposure_b2b
	for _, v := range original_data {
		if strings.Contains(v.EtfSymbol, ".") == false {
			symbol, err := db.LinstockStocksSymbol.
				Query().
				Where(linstockstockssymbol.Symbol(v.EtfSymbol)).First(context.Background())
			if err != nil {
				c.Error(err)
			} else {
				name_ko := symbol.NameKo
				etf_detail, err2 := db.LinstockStocksEtfdetail.
					Query().
					Where(linstockstocksetfdetail.SymbolID(symbol.ID)).
					First(context.Background())
				if err2 != nil {
					c.Error(err2)
				} else {
					origin_etf := etf_detail.OriginEtf
					leverage_power := etf_detail.LeveragePower
					is_inverse := etf_detail.IsInverse
					is_leverage := etf_detail.IsLeveraged
					weight_percent := v.WeightPercentage
					if is_inverse || is_leverage {
						value, ok := symbolToWeightPercent[origin_etf]
						if ok {
							weight_percent = value
						}
					}
					rst = append(rst, utils.TransformEtfSectorExposure(v.AssetExposure, v.EtfSymbol, name_ko, v.MarketValue, v.SharesNumber, leverage_power, weight_percent))
				}
			}
		}
	}
	utils.Response(c, rst, "sector_exposure")
}

// B2B ALL Historical Dividends
// @Summary Historical Dividends
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Param 		symbol path string false  "ETF symbol"
// @Security  ApiKeyAuth
// @Router 		/historical-price-full/stock_dividend [get]
// @Success 	200 {object} models.StockDividend_b2b
func GetHistoricalStockDividend(c *gin.Context) {
	db := db.Client()
	defer db.Close()
	symbols := db.LinstockStocksSymbol.Query().Where(linstockstockssymbol.SecTypeIn([]string{"SSEF", "EF", "EN"}...), linstockstockssymbol.Domicile("us")).Select(linstockstockssymbol.FieldSymbol).StringsX(context.Background())
	var rst []*models.StockDividend_b2b
	var wg sync.WaitGroup
	wg.Add(len(symbols))
	for _, val := range symbols {
		historical_db := db.LinstockStocksHistoricaldividendu.Query().Where(linstockstockshistoricaldividendu.Symbol(val)).Order(ent.Desc(linstockstockshistoricaldividendu.FieldExDividendDate)).AllX(context.Background())
		if len(historical_db) == 0 {
			stockDiv := new(models.StockDividend_b2b)
			stockDiv.Symbol = val
			stockDiv.Historical = []models.Historical_b2b{}
			rst = append(rst, stockDiv)
			wg.Done()
		} else {
			go func(symbol string, historical_db []*ent.LinstockStocksHistoricaldividendu) {
				defer func() {
					if r := recover(); r != nil {
						logger.SentryLogger(
							fmt.Errorf("GetHistoricalStockDividend panicked: %v", r))
					}
					wg.Done()
				}()
				stockDiv := getStockDividendBySymbol(symbol, historical_db)
				rst = append(rst, stockDiv)
			}(val, historical_db)
			time.Sleep(time.Microsecond * 30)
		}
	}
	wg.Wait()
	utils.Response(c, rst, "stock_dividend")
}

// B2B Historical Dividends
// @Summary Historical Dividends
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Param 		symbol path string false  "ETF symbol"
// @Security  ApiKeyAuth
// @Router 		/historical-price-full/stock_dividend/{symbol} [get]
// @Success 	200 {object} models.StockDividend_b2b
func GetHistoricalStockDividendBySymbol(c *gin.Context) {
	symb, err := c.Params.Get("symbol")
	if !err {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("no symbol provided"))
		return
	}
	var rst []*models.StockDividend_b2b
	symbols := strings.Split(symb, ",")
	db := db.Client()
	defer db.Close()
	var wg sync.WaitGroup
	wg.Add(len(symbols))

	for _, val := range symbols {
		historical_db := db.LinstockStocksHistoricaldividendu.Query().Where(linstockstockshistoricaldividendu.Symbol(val)).Order(ent.Desc(linstockstockshistoricaldividendu.FieldExDividendDate)).AllX(context.Background())
		if len(historical_db) == 0 {
			stockDiv := new(models.StockDividend_b2b)
			stockDiv.Symbol = val
			stockDiv.Historical = []models.Historical_b2b{}
			rst = append(rst, stockDiv)
			wg.Done()
		} else {
			go func(symbol string, historical_db []*ent.LinstockStocksHistoricaldividendu) {
				defer func() {
					if r := recover(); r != nil {
						logger.SentryLogger(
							fmt.Errorf("GetHistoricalStockDividend panicked: %v", r))
					}
					wg.Done()
				}()
				stockDiv := getStockDividendBySymbol(symbol, historical_db)
				rst = append(rst, stockDiv)
			}(val, historical_db)
			time.Sleep(time.Microsecond * 30)
		}
	}
	wg.Wait()
	utils.Response(c, rst, "stock_dividend")
}

func getStockDividendBySymbol(symbol string, historical_db []*ent.LinstockStocksHistoricaldividendu) *models.StockDividend_b2b {
	query := url.Values{}
	query.Add("apikey", config.FMP_API_KEY)
	rst := new(models.StockDividend_b2b)
	rst.Symbol = symbol

	var daily_prices []*models.HistoricalPriceEOD
	var dividend_polygon *models.HistoricalDividendPolygon
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		json.Unmarshal(FetchPriceFullEOD(symbol, historical_db[len(historical_db)-1].ExDividendDate.Format("2006-01-02")), &daily_prices)
	}()
	go func() {
		defer wg.Done()
		dividend_polygon = FetchDividendPolygon(symbol)
		if len(dividend_polygon.Result) != 0 {
			rst.Frequency = dividend_polygon.Result[0].Frequency
		}
	}()
	wg.Wait()

	utils.Reverse(daily_prices)
	if daily_prices[0].AdjustedClose == 0 {
		return rst
	}
	rst.DividendYield = utils.CalculateDividendYield(symbol, daily_prices[0], historical_db)
	for _, v := range historical_db {
		hist := models.Historical_b2b{
			AdjDividend: v.AdjustedDividend,
			Dividend:    v.Dividend,
		}
		if !v.ExDividendDate.IsZero() {
			hist.Date = v.ExDividendDate.Format("2006-01-02")
		}
		if !v.DeclarationDate.IsZero() {
			hist.DeclarationDate = v.DeclarationDate.Format("2006-01-02")
		}
		if !v.PaymentDate.IsZero() {
			hist.PaymentDate = v.PaymentDate.Format("2006-01-02")
		}
		if !v.RecordDate.IsZero() {
			hist.RecordDate = v.RecordDate.Format("2006-01-02")
		}
		exDivDate := v.ExDividendDate
		var payment *float64
		for _, d := range daily_prices {
			cur, _ := time.Parse("2006-01-02", d.Date)
			if cur.Before(exDivDate) {
				break
			} else {
				payment = &d.Close

			}
		}
		if payment != nil && !v.PaymentDate.IsZero() && *payment == 0 {
			hist.PayoutPriceRatio = utils.KeepDecimal(v.AdjustedDividend/(*payment)*100, 3)
		}
		if len(daily_prices) != 0 && daily_prices[0].Close != 0 {
			hist.PayoutCloseRatio = utils.KeepDecimal((v.AdjustedDividend/daily_prices[0].Close)*100, 3)
		}
		rst.Historical = append(rst.Historical, hist)
	}
	return rst

}

// B2B Historical Dividends Rank
// @Summary Historical Dividends Rank
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Security  ApiKeyAuth
// @Router 		/historical-price-full/stock_dividend/rank [get]
// @Success 	200 {object} models.StockDividendRank_b2b
func GetHistoricalStockDividendRank(c *gin.Context) {
	db := db.Client()
	defer db.Close()

	var symbols_db []models.SymbolSimple
	err1 := db.LinstockStocksSymbol.Query().Where(
		linstockstockssymbol.Domicile("us"),
		linstockstockssymbol.Or(
			linstockstockssymbol.SecType("EN"),
			linstockstockssymbol.SecType("EF"),
			linstockstockssymbol.SecType("SSEF"),
		),
		linstockstockssymbol.HasLinstockStocksEtfdetailWith(
			linstockstocksetfdetail.FirstTradingDateLTE(time.Now().AddDate(-3, 0, 0)),
		),
	).
		Select(
			linstockstockssymbol.FieldID,
			linstockstockssymbol.FieldSymbol,
			linstockstockssymbol.FieldNameKo,
		).
		Scan(context.TODO(), &symbols_db)
	if err1 != nil {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("No symbol provided"))
		return
	}

	symbol_map := make(map[string]models.SymbolSimple)
	var symbols []string
	for _, v := range symbols_db {
		symbol_map[v.Symbol] = v
		symbols = append(symbols, v.Symbol)
	}

	hist_map := make(map[string]float64)
	temp, cur_date := FetchPriceFullPolygon(time.Now())
	for _, v := range temp.Result {
		hist_map[v.Symbol] = v.Price
	}

	var dividend_db []models.StockDividendDb
	err2 := db.LinstockStocksHistoricaldividendu.Query().Where(
		linstockstockshistoricaldividendu.ExDividendDateGTE(cur_date.AddDate(-1, 0, -1)),
		linstockstockshistoricaldividendu.SymbolIn(symbols...),
	).
		Select(
			linstockstockshistoricaldividendu.FieldExDividendDate,
			linstockstockshistoricaldividendu.FieldSymbol,
			linstockstockshistoricaldividendu.FieldDividend,
		).
		Scan(context.TODO(), &dividend_db)
	if err2 != nil {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("No symbol provided"))
	}

	dividend_map := make(map[string][]models.StockDividendDb)
	for _, v := range dividend_db {
		dividend_map[v.Symbol] = append(dividend_map[v.Symbol], v)
	}

	var dividend_db_year []models.StockDividendDb
	err3 := db.LinstockStocksHistoricaldividendu.Query().Where(
		linstockstockshistoricaldividendu.ExDividendDateLTE(cur_date.AddDate(-1, 0, -1)),
		linstockstockshistoricaldividendu.ExDividendDateGTE(cur_date.AddDate(-2, 0, -1)),
		linstockstockshistoricaldividendu.SymbolIn(symbols...),
	).
		Select(
			linstockstockshistoricaldividendu.FieldExDividendDate,
			linstockstockshistoricaldividendu.FieldSymbol,
			linstockstockshistoricaldividendu.FieldDividend,
		).
		Scan(context.TODO(), &dividend_db_year)
	if err3 != nil {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("No symbol provided"))
	}

	dividend_map_year := make(map[string][]models.StockDividendDb)
	for _, v := range dividend_db_year {
		dividend_map_year[v.Symbol] = append(dividend_map_year[v.Symbol], v)
	}

	rst := utils.CalculateDividendYieldRank(symbol_map, hist_map, dividend_map, dividend_map_year)
	utils.Response(c, rst, "json")
}

// B2B Historical Full Prices
// @Summary Historical Full Prices
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string false "ETF symbol: QQQ"
// @Param 		from query string false "Date format:2015-08-20)"
// @Param 	 	to query string false "Date format: 2015-08-20)"
// @Security  ApiKeyAuth
// @Router 		/historical-price-full/{symbol} [get]
// @Success 	200 {object} models.FullHistoricalPrices
func GetEtfHistoricalPriceFull(c *gin.Context) {
	query := c.Request.URL.Query()
	symbol, err1 := c.Params.Get("symbol")
	if err1 == false {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("No symbol provided"))
		return
	}
	path := fmt.Sprintf("/private/historical-price-full/%s", symbol)

	var original_data []*models.HistoricalPrice
	json.Unmarshal(FetchPriceFull(query, path), &original_data)
	var rst []*models.HistoricalPrice_b2b
	for _, v := range original_data {
		rst = append(rst, utils.TransformHistoricalPrice(v))
	}
	result := models.FullHistoricalPrices{
		Symbol: symbol,
		Prices: rst,
	}
	c.JSON(http.StatusOK, &result)
}

// B2B ETF Quote
// @Summary ETF Quote
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string false "ETF symbol: QQQ"
// @Security  ApiKeyAuth
// @Router 		/etf-quote/{symbol} [get]
// @Success 	200 {object} models.StockQuote_b2b
func GetEtfQuote(c *gin.Context) {
	query := c.Request.URL.Query()
	query.Add("apikey", config.FMP_API_KEY)
	symbol, err1 := c.Params.Get("symbol")
	if err1 == false {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("No symbol provided"))
		return
	}
	if strings.Count(symbol, ",") >= 50 {
		s := strings.Split(symbol, ",")
		symbol = strings.Join(s[:50], ",")
	}
	path := fmt.Sprintf("quote/%s", symbol)
	url := url.URL{
		Scheme:   "https",
		Host:     config.FMP_HOST,
		Path:     fmt.Sprintf("/api/v3/%s", path),
		RawQuery: query.Encode(),
	}
	var original_data []*models.StockQuote
	resp, err := http.Get(url.String())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	json.Unmarshal(data, &original_data)
	var rst []*models.StockQuote_b2b
	for _, v := range original_data {
		rst = append(rst, utils.TransformStockQuote(v))
	}
	c.JSON(http.StatusOK, &rst)
}

// B2B ETF Price Only
// @Summary ETF Price Only
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string false "ETF symbol: QQQ"
// @Security  ApiKeyAuth
// @Router 		/etf-price-only/{symbol} [get]
// @Success 	200 {object} models.QuoteShort
func GetEtfPriceOnly(c *gin.Context) {
	query := c.Request.URL.Query()
	query.Add("apikey", config.FMP_API_KEY)
	symbol, err1 := c.Params.Get("symbol")
	if err1 == false {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("No symbol provided"))
		return
	}
	path := fmt.Sprintf("quote-short/%s", symbol)
	url := url.URL{
		Scheme:   "https",
		Host:     config.FMP_HOST,
		Path:     fmt.Sprintf("/api/v3/%s", path),
		RawQuery: query.Encode(),
	}
	var rst []*models.QuoteShort
	resp, err := http.Get(url.String())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	json.Unmarshal(data, &rst)
	c.JSON(http.StatusOK, &rst)
}

// B2B ETF Basic Stat
// @Summary ETF Basic Stat
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string false "ETF symbol: QQQ"
// @Security  ApiKeyAuth
// @Router 		/etf-basic-stat/{symbol} [get]
// @Success 	200 {object} models.Stats_b2b
func GetEtfBasicStat(c *gin.Context) {
	query := c.Request.URL.Query()
	query.Add("token", config.IEXCLOUD_API_KEY)
	query.Add("types", "stats")
	symbol, err1 := c.Params.Get("symbol")
	if err1 == false {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("No symbol provided"))
		return
	}
	path := "/stable/stock/market/batch"
	if strings.Count(symbol, ",") >= 100 {
		s := strings.Split(symbol, ",")
		symbol = strings.Join(s[:50], ",")
	}
	query.Add("symbols", symbol)
	url := url.URL{
		Scheme:   "https",
		Host:     config.IEXCLOUD_HOST,
		Path:     path,
		RawQuery: query.Encode(),
	}
	var original_data map[string](*models.DetailStats)
	resp, err := http.Get(url.String())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	json.Unmarshal(data, &original_data)
	rst := make(map[string]models.Stats_b2b)
	for k, v := range original_data {
		rst[k] = *utils.TransformEtfBasicStats(v)
	}
	c.JSON(http.StatusOK, &rst)

}

// B2B Maximum drawdown
// @Summary Maximum drawdown
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Param 	 	symbol path string false "ETF symbol: QQQ"
// @Param 		from query string false "Date format:2015-08-20)"
// @Param 	 	to query string false "Date format: 2015-08-20)"
// @Security  ApiKeyAuth
// @Router 		/MDD/{symbol} [get]
// @Success 	200 {object} models.DrawDown
func MDD(c *gin.Context) {
	query := c.Request.URL.Query()
	symbol, err1 := c.Params.Get("symbol")
	if err1 == false {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("No symbol provided"))
		return
	}
	path := fmt.Sprintf("/private/historical-price-full/%s", symbol)

	var original_data []*models.HistoricalPrice
	json.Unmarshal(FetchPriceFull(query, path), &original_data)

	first, _ := time.Parse("2006-01-02", query.Get("from"))
	last, _ := time.Parse("2006-01-02", query.Get("to"))
	if first.After(last) {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("Bad input start date after end date"))
	}
	rst := utils.GetMDD(original_data, first, last)
	utils.Response(c, rst, "MDD")

}

// B2B Leverage Inverse Map
// @Summary Leverage Inverse Map
// @Tags 		external
// @Accept  	json
// @Produce  	json
// @Security  ApiKeyAuth
// @Router 		/leverage-inverse-map [get]
// @Success 	200 {object} []models.LeverageInverseMap
func GetLeverageInverseMap(c *gin.Context) {
	db := db.Client()
	defer db.Close()

	LeverageInverseMap := make(map[string][]models.AssetSymbols)
	var rst []models.LeverageInverseMap
	query := db.LinstockStocksEtfdetail.Query().WithLinstockStocksSymbol().AllX(context.Background())
	for _, v := range query {
		if v == nil || v.UnderlyingAsset == "" {
			continue
		}
		LeverageInverseMap[v.UnderlyingAsset] = append(LeverageInverseMap[v.UnderlyingAsset],
			models.AssetSymbols{
				Symbol:        v.Edges.LinstockStocksSymbol.Symbol,
				LeveragePower: v.LeveragePower,
				Name:          v.Edges.LinstockStocksSymbol.Name,
				NameKo:        v.Edges.LinstockStocksSymbol.NameKo,
			})
	}
	for k, v := range LeverageInverseMap {
		parse_name := strings.Split(k, "@")
		priority, name := parse_name[0], parse_name[1]
		priority_int, _ := strconv.Atoi(priority)
		rst = append(rst, models.LeverageInverseMap{
			Priority:        int32(priority_int),
			UnderlyingAsset: name,
			Symbols:         v,
		})
	}
	sort.Slice(rst, func(i, j int) bool {
		return rst[i].Priority < rst[j].Priority
	})
	utils.Response(c, rst, "LeverageInverseMap")
}
