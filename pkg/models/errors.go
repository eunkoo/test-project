package models

type ErrorMessage struct {
	Message string `json:"message"`
}

func NewError(msg string) *ErrorMessage {
	return &ErrorMessage{
		Message: msg,
	}
}
