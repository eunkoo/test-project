package models

import (
	"fmt"
	"strings"
	"time"
)

type Holder struct {
	Asset            string  `json:"asset"`
	Cusip            string  `json:"cusip"`
	Isin             string  `json:"isin"`
	MarketValue      float64 `json:"marketValue"`
	Name             string  `json:"name"`
	SharesNumber     int64   `json:"sharesNumber"`
	Updated          string  `json:"updated"`
	WeightPercentage float64 `json:"weightPercentage"`
}

type CountryWeight struct {
	Country          string `json:"country"`
	WeightPercentage string `json:"weightPercentage"`
}

type CountryName struct {
	Country    string `json:"rawCountry"`
	CountryEng string `json:"countryEng"`
	CountryKor string `json:"countryKor"`
}

type SectorName struct {
	Sector   string `json:"rawSector"`
	SectorKo string `json:"sectorKor"`
}

type SectorWeight struct {
	Sector           string `json:"sector"`
	WeightPercentage string `json:"weightPercentage"`
}

type StockExposure struct {
	EtfSymbol        string  `json:"etfSymbol"`
	AssetExposure    string  `json:"assetExposure"`
	SharesNumber     int64   `json:"sharesNumber"`
	WeightPercentage float64 `json:"weightPercentage"`
	MarketValue      float64 `json:"marketValue"`
}

type StockDividend struct {
	Symbol     string       `json:"symbol"`
	Historical []Historical `json:"historical"`
}

type StockDividendDb struct {
	Symbol		string		`json:"symbol"`
	ExDivDate	time.Time	`json:"ex_dividend_date"`
	Dividend	float64		`json:"dividend"`
}

type Historical struct {
	Date            string  `json:"date"`
	Label           string  `json:"label"`
	AdjDividend     float64 `json:"adjDividend"`
	Dividend        float64 `json:"dividend"`
	RecordDate      string  `json:"recordDate"`
	PaymentDate     string  `json:"paymentDate"`
	DeclarationDate string  `json:"declarationDate"`
}
type HistoricalPrice struct {
	Date          string  `json:"date"`
	Close         float64 `json:"close"`
	AdjustedClose float64 `json:"adjustedClose"`
}

type StockQuote struct {
	Symbol               string  `json:"symbol"`
	Name                 string  `json:"name"`
	Price                float64 `json:"price"`
	ChangesPercentage    float64 `json:"changesPercentage"`
	Change               float64 `json:"change"`
	DayLow               float64 `json:"dayLow"`
	DayHigh              float64 `json:"dayHigh"`
	YearHigh             float64 `json:"yearHigh"`
	YearLow              float64 `json:"yearLow"`
	MarketCap            float64 `json:"marketCap"`
	PriceAvg50           float64 `json:"priceAvg50"`
	PriceAvg200          float64 `json:"priceAvg200"`
	Volume               int     `json:"volume"`
	AvgVolume            int     `json:"avgVolume"`
	Exchange             string  `json:"exchange"`
	Open                 float64 `json:"open"`
	PreviousClose        float64 `json:"previousClose"`
	Eps                  float64 `json:"eps"`
	Pe                   float64 `json:"pe"`
	EarningsAnnouncement string  `json:"earningsAnnouncement"`
	SharesOutstanding    int64   `json:"sharesOutstanding"`
	Timestamp            int     `json:"timestamp"`
}

type QuoteShort struct {
	Symbol string  `json:"symbol"`
	Price  float64 `json:"price"`
	Volume int     `json:"volume"`
}

type DetailStats struct {
	Stats struct {
		CompanyName               string  `json:"companyName"`
		Marketcap                 int64   `json:"marketcap"`
		Week52High                float64 `json:"week52high"`
		Week52Low                 float64 `json:"week52low"`
		Week52HighSplitAdjustOnly float64 `json:"week52highSplitAdjustOnly"`
		Week52LowSplitAdjustOnly  float64 `json:"week52lowSplitAdjustOnly"`
		Week52Change              float64 `json:"week52change"`
		SharesOutstanding         int     `json:"sharesOutstanding"`
		Float                     int     `json:"float"`
		Avg10Volume               int     `json:"avg10Volume"`
		Avg30Volume               int     `json:"avg30Volume"`
		Day200MovingAvg           float64 `json:"day200MovingAvg"`
		Day50MovingAvg            float64 `json:"day50MovingAvg"`
		Employees                 int     `json:"employees"`
		TtmEPS                    int     `json:"ttmEPS"`
		TtmDividendRate           float64 `json:"ttmDividendRate"`
		DividendYield             float64 `json:"dividendYield"`
		NextDividendDate          string  `json:"nextDividendDate"`
		ExDividendDate            string  `json:"exDividendDate"`
		NextEarningsDate          string  `json:"nextEarningsDate"`
		PeRatio                   int     `json:"peRatio"`
		Beta                      float64 `json:"beta"`
		MaxChangePercent          float64 `json:"maxChangePercent"`
		Year5ChangePercent        float64 `json:"year5ChangePercent"`
		Year2ChangePercent        float64 `json:"year2ChangePercent"`
		Year1ChangePercent        float64 `json:"year1ChangePercent"`
		YtdChangePercent          float64 `json:"ytdChangePercent"`
		Month6ChangePercent       float64 `json:"month6ChangePercent"`
		Month3ChangePercent       float64 `json:"month3ChangePercent"`
		Month1ChangePercent       float64 `json:"month1ChangePercent"`
		Day30ChangePercent        float64 `json:"day30ChangePercent"`
		Day5ChangePercent         float64 `json:"day5ChangePercent"`
	} `json:"stats"`
}

type DrawDown struct {
	Max       DrawDownMax       `json:"max"`
	Second    DrawDownMax       `json:"second"`
	SingleDay DrawDownSingleDay `json:"single_day"`
	Third     DrawDownMax       `json:"third"`
}

func (b *DrawDown) String() string {
	var res []string
	// if (b.SingleDay != DrawDownSingleDay{}) {
	res = append(res, b.SingleDay.String())
	// }
	// if (b.Max != DrawDownMax{}) {
	res = append(res, b.Max.String())
	// }
	// if (b.Second != DrawDownMax{}) {
	res = append(res, b.Second.String())
	// }
	// if (b.Third != DrawDownMax{}) {
	res = append(res, b.Third.String())
	// }
	return strings.Join(res, "|")
}

type DrawDownMax struct {
	Drawdown float64      `json:"drawdown"`
	Peak     DrawDownPeak `json:"peak"`
	Trough   DrawDownPeak `json:"trough"`
}

func (b *DrawDownMax) String() string {
	return StructToString(*b)
}

type DrawDownPeak struct {
	Date  string  `json:"date"`
	Price float64 `json:"price"`
}

func (b *DrawDownPeak) String() string {
	res := []string{
		strings.ReplaceAll(b.Date, "-", ""), fmt.Sprintf("%f", b.Price)}
	return strings.Join(res, "|")
}

type DrawDownSingleDay struct {
	Drawdown float64 `json:"drawdown"`
	Price    float64 `json:"price"`
	Trough   string  `json:"trough"`
}

func (b *DrawDownSingleDay) String() string {
	res := []string{
		fmt.Sprintf("%f", b.Drawdown),
		fmt.Sprintf("%f", b.Price),
		strings.ReplaceAll(b.Trough, "-", ""),
	}
	return strings.Join(res, "|")
}
