package models

import (
	"fmt"
	"reflect"
	"strings"
)

func StructToString[T any](b T) string {
	rv := reflect.ValueOf(b)
	num := rv.NumField()
	var rst []string
	for i := 0; i < num; i++ {
		fi := rv.Field(i)
		var s string

		switch fi.Kind() {
		case reflect.String:
			s = fmt.Sprintf("%s", fi)
		case reflect.Float64:
			s = fmt.Sprintf("%f", fi.Float())
		case reflect.Int64:
			s = fmt.Sprintf("%d", fi.Int())
		case reflect.Bool:
			if fi.Bool() == true {
				s = "1"
			} else {
				s = "0"
			}
		case reflect.Slice:
			arr := reflect.ValueOf(fi.Interface())
			var sArr []string
			for j := 0; j < arr.Len(); j++ {
				sArr = append(sArr, arr.Index(j).String())
			}
			s = strings.Join(sArr, "@")
		case reflect.Struct:
			switch fi.Type().String() {
			case "models.DrawDownPeak":
				val := fi.Interface().(DrawDownPeak)
				s = val.String()
			case "models.EtfDetail_b2b":
				val := fi.Interface().(EtfDetail_b2b)
				s = val.String()
			case "models.Symbol":
				val := fi.Interface().(Symbol)
				s = val.String()
			}
		}
		rst = append(rst, s)
	}
	return strings.Join(rst, "|")
}
