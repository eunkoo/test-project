package models

import (
	"fmt"
	"strings"
)

type Holder_b2b struct {
	Asset            string  `json:"asset"`
	Cusip            string  `json:"cusip"`
	Isin             string  `json:"isin"`
	MarketValue      float64 `json:"market_value"`
	Name             string  `json:"name"`
	Name_KO          string  `json:"name_ko"`
	SharesNumber     int64   `json:"shares_number"`
	Updated          string  `json:"date"`
	WeightPercentage float64 `json:"weight_percent"`
}

func (b *Holder_b2b) String() string {
	res := []string{
		fmt.Sprintf("%s", b.Asset),
		fmt.Sprintf("%s", b.Cusip),
		fmt.Sprintf("%s", b.Isin),
		fmt.Sprintf("%f", b.MarketValue),
		fmt.Sprintf("%s", b.Name),
		fmt.Sprintf("%s", b.Name_KO),
		fmt.Sprintf("%d", b.SharesNumber),
		strings.ReplaceAll(b.Updated, "-", ""),
		fmt.Sprintf("%f", b.WeightPercentage),
	}
	return strings.Join(res, "|")
}

type CountryWeight_b2b struct {
	Country          string  `json:"country"`
	Country_KO       string  `json:"country_ko"`
	WeightPercentage float64 `json:"weight_percent"`
}

func (b *CountryWeight_b2b) String() string {
	return StructToString(*b)
}

type StockExposure_b2b struct {
	AssetExposure    string  `json:"asset_exposure"`
	EtfSymbol        string  `json:"etf_symbol"`
	EtfNameKo        string  `json:"etf_name_ko"`
	MarketValue      float64 `json:"market_value"`
	SharesNumber     int64   `json:"shares_number"`
	WeightPercentage float64 `json:"weight_percent"`
	LeveragePower    float64 `json:"leverage_power"`
}

func (b *StockExposure_b2b) String() string {
	return StructToString(*b)
}

type StockDividend_b2b struct {
	Symbol        string           `json:"symbol"`
	DividendYield float64          `json:"div_yield" default:"0.0000"`
	Frequency     int64            `json:"frequency"`
	Historical    []Historical_b2b `json:"history"`
}

type StockDividendRank_b2b struct {
	Rank          int64   `json:"rank,omitempty"`
	NameKo        string  `json:"name_ko"`
	Symbol        string  `json:"symbol"`
	DividendYield float64 `json:"dividend_yield"`
}

type Historical_b2b struct {
	AdjDividend      float64 `json:"adj_dividend"`
	Date             string  `json:"ex_date"`
	DeclarationDate  string  `json:"declaration_date"`
	Dividend         float64 `json:"dividend"`
	PaymentDate      string  `json:"payment_date"`
	RecordDate       string  `json:"record_date"`
	PayoutPriceRatio float64 `json:"payout_ex_close_ratio"`
	PayoutCloseRatio float64 `json:"payout_close_ratio"`
}

func (b *Historical_b2b) String() string {
	res := []string{
		fmt.Sprintf("%f", b.AdjDividend),
		strings.ReplaceAll(b.Date, "-", ""),
		strings.ReplaceAll(b.DeclarationDate, "-", ""),
		fmt.Sprintf("%f", b.Dividend),
		strings.ReplaceAll(b.PaymentDate, "-", ""),
		strings.ReplaceAll(b.RecordDate, "-", ""),
		fmt.Sprintf("%f", b.PayoutPriceRatio),
		fmt.Sprintf("%f", b.PayoutCloseRatio),
	}
	return strings.Join(res, "|")
}

type HistoricalPrice_b2b struct {
	AdjustedClose float64 `json:"adjusted_close"`
	Close         float64 `json:"close"`
	Date          string  `json:"date"`
}

type StockQuote_b2b struct {
	Symbol         string  `json:"symbol"`
	Name           string  `json:"name"`
	Price          float64 `json:"price"`
	ChangesPercent float64 `json:"changes_percent"`
	Change         float64 `json:"change"`
	DayLow         float64 `json:"day_low"`
	DayHigh        float64 `json:"day_high"`
	YearHigh       float64 `json:"year_high"`
	YearLow        float64 `json:"year_low"`
	MarketCap      float64 `json:"market_cap"`
	PriceAvg50     float64 `json:"price_avg50"`
	PriceAvg200    float64 `json:"price_avg200"`
	Volume         int     `json:"volume"`
	AvgVolume      int     `json:"avg_volume"`
	Exchange       string  `json:"exchange"`
	Open           float64 `json:"open"`
	PreviousClose  float64 `json:"previous_close"`
	Timestamp      int     `json:"timestamp"`
}

type Stats_b2b struct {
	CompanyName               string  `json:"company_name"`
	Marketcap                 int64   `json:"marketcap"`
	Week52High                float64 `json:"week52_high"`
	Week52Low                 float64 `json:"week52_low"`
	Week52HighSplitAdjustOnly float64 `json:"week52_high_split_adjust_only"`
	Week52LowSplitAdjustOnly  float64 `json:"week52_low_split_adjust_only"`
	Week52Change              float64 `json:"week52_change"`
	Avg10DVolume              int     `json:"avg_10d_volume"`
	Avg30DVolume              int     `json:"avg_30d_volume"`
	Day200MovingAvg           float64 `json:"day200_moving_avg"`
	Day50MovingAvg            float64 `json:"day50_moving_avg"`
	TtmDividendRate           float64 `json:"ttm_dividend_rate"`
	DividendYield             float64 `json:"dividend_yield"`
	NextDividendDate          string  `json:"next_dividend_date"`
	ExDividendDate            string  `json:"ex_dividend_date"`
	Beta                      float64 `json:"beta"`
	Year5ChangePercent        float64 `json:"year5_change_percent"`
	Year2ChangePercent        float64 `json:"year2_change_percent"`
	Year1ChangePercent        float64 `json:"year1_change_percent"`
	YtdChangePercent          float64 `json:"ytd_change_percent"`
	Month6ChangePercent       float64 `json:"month6_change_percent"`
	Month3ChangePercent       float64 `json:"month3_change_percent"`
	Month1ChangePercent       float64 `json:"month1_change_percent"`
	Day30ChangePercent        float64 `json:"day30_change_percent"`
	Day5ChangePercent         float64 `json:"day5_change_percent"`
}

type SectorWeight_b2b struct {
	Sector           string  `json:"sector"`
	SectorKo         string  `json:"sector_ko"`
	WeightPercentage float64 `json:"weight_percent"`
}

func (b *SectorWeight_b2b) String() string {
	return StructToString(*b)
}

type EtfDetail_b2b struct {
	Symbol            string   `json:"symbol"`
	Name              string   `json:"name"`
	NameKo            string   `json:"name_ko"`
	Cons              []string `json:"cons_ko"`
	Pros              []string `json:"pros_ko"`
	IndexCriteria     []string `json:"index_criteria"`
	SelectionCriteria []string `json:"selection_criteria"`
	WeightingCriteria []string `json:"weighting_criteria"`
	LeveragePower     float64  `json:"leverage_power"`
	IsInverse         bool     `json:"is_inverse"`
	IsLeveraged       bool     `json:"is_leveraged"`
	OriginEtf         string   `json:"origin_etf"`
	Expense           float64  `json:"expense"`
	BenchMark         string   `json:"bench_mark"`
	FirstTradingDate  string   `json:"first_trading_date"`
	HomePage          string   `json:"home_page"`
	AssetManager      string   `json:"asset_manger"`
}

func (b *EtfDetail_b2b) String() string {
	IsInverse := "0"
	IsLeveraged := "0"
	if b.IsInverse == true {
		IsInverse = "1"
	}
	if b.IsLeveraged == true {
		IsLeveraged = "1"
	}
	res := []string{
		b.Symbol,
		b.Name,
		b.NameKo,
		strings.Join(b.Cons, "@"),
		strings.Join(b.Pros, "@"),
		strings.Join(b.IndexCriteria, "@"),
		strings.Join(b.SelectionCriteria, "@"),
		strings.Join(b.WeightingCriteria, "@"),
		fmt.Sprintf("%f", b.LeveragePower),
		IsInverse,
		IsLeveraged,
		b.OriginEtf,
		fmt.Sprintf("%f", b.Expense),
		b.BenchMark,
		strings.ReplaceAll(b.FirstTradingDate, "-", ""),
		b.HomePage,
		b.AssetManager,
	}
	return strings.Join(res, "|")
}

type FullHistoricalPrices struct {
	Symbol string      `json:"symbol"`
	Prices interface{} `json:"historical_price"`
}
type CountryWeightings struct {
	Symbol     string              `json:"symbol"`
	Weightings []CountryWeight_b2b `json:"country_weightings"`
}

type SectorWeightings struct {
	Symbol     string             `json:"symbol"`
	Weightings []SectorWeight_b2b `json:"sector_weightings"`
}

type EtfHoldings struct {
	Symbol   string       `json:"symbol"`
	Holdings []Holder_b2b `json:"etf_holding"`
}

type HistoricalPriceEOD struct {
	Date          string  `json:"date"`
	Open          float64 `json:"open"`
	High          float64 `json:"high"`
	Low           float64 `json:"low"`
	Close         float64 `json:"close"`
	AdjustedClose float64 `json:"adjusted_close"`
	Volume        int     `json:"volume"`
}

type HistoricalPricePolygon struct {
	Count  int64 `json:"resultsCount"`
	Result []struct {
		Symbol string  `json:"T"`
		Price  float64 `json:"c"`
	} `json:"results,omitempty"`
}

type LeverageInverseMap struct {
	Priority        int32          `json:"priority"`
	UnderlyingAsset string         `json:"underlying_asset"`
	Symbols         []AssetSymbols `json:"symbols"`
}

func (b *LeverageInverseMap) ToString() []string {
	var res []string
	for _, v := range b.Symbols {
		res = append(res, fmt.Sprintf("%d|%s|%s", b.Priority, b.UnderlyingAsset, v.String()))
	}
	return res
}

type AssetSymbols struct {
	Symbol        string  `json:"symbol"`
	LeveragePower float64 `json:"leverage_power"`
	Name          string  `json:"name"`
	NameKo        string  `json:"name_ko"`
}

func (b *AssetSymbols) String() string {
	res := []string{
		fmt.Sprintf("%s", b.Symbol),
		fmt.Sprintf("%f", b.LeveragePower),
		fmt.Sprintf("%s", b.Name),
		fmt.Sprintf("%s", b.NameKo),
	}
	return strings.Join(res, "|")
}

type HistoricalDividendPolygon struct {
	Result []struct {
		CashAmount      float64 `json:"cash_amount"`
		DeclarationDate string  `json:"declaration_date"`
		ExDividendDate  string  `json:"ex_dividend_date"`
		Frequency       int64   `json:"frequency"`
		PayDate         string  `json:"pay_date"`
		RecordDate      string  `json:"record_date"`
		Ticker          string  `json:"ticker"`
	} `json:"results,omitempty"`
}
