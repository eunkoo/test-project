package models

type AnnualizedReturn struct {
	Symbol   string      `json:"symbol"`
	OneY     interface{} `json:"1y,omitempty"`
	ThreeY   interface{} `json:"3y,omitempty"`
	FiveY    interface{} `json:"5y,omitempty"`
	SevenY   interface{} `json:"7y,omitempty"`
	TenY     interface{} `json:"10y,omitempty"`
	FivetenY interface{} `json:"15y,omitempty"`
	TwentY   interface{} `json:"20y,omitempty"`
	Max      interface{} `json:"max,omitempty"`
}

type CumulativeReturn struct {
	Symbol   string      `json:"symbol"`
	OneW     interface{} `json:"1w,omitempty"`
	OneM     interface{} `json:"1m,omitempty"`
	ThreeM   interface{} `json:"3m,omitempty"`
	SixM     interface{} `json:"6m,omitempty"`
	OneY     interface{} `json:"1y,omitempty"`
	ThreeY   interface{} `json:"3y,omitempty"`
	FiveY    interface{} `json:"5y,omitempty"`
	SevenY   interface{} `json:"7y,omitempty"`
	TenY     interface{} `json:"10y,omitempty"`
	FivetenY interface{} `json:"15y,omitempty"`
	TwentY   interface{} `json:"20y,omitempty"`
	Max      interface{} `json:"max,omitempty"`
}

type ReturnsByYear struct {
	Symbol  string      `json:"symbol"`
	Returns interface{} `json:"returns_by_year"`
}

type ReturnsRegular struct {
	Return           float64 `json:"return"`
	ReturnWoDividend float64 `json:"return_wo_dividend"`
	Annual           float64 `json:"annualized_return"`
	Frequency        string  `json:"frequency"`
	Period           int64   `json:"period"`
	InvestedTime     int64   `json:"invested_times"`
}

type ReturnsPeriod struct {
	Symbol              string  `json:"symbol"`
	ReturnAdjClose      float64 `json:"return_adj_close"`
	ReturnFullyAdjClose float64 `json:"return_fully_adj_close"`
}

type ReturnsRank struct {
	Return        float64 `json:"return"`
	NameKo        string  `json:"name_ko"`
	Rank          int64   `json:"rank,omitempty"`
	Symbol        string  `json:"symbol"`
	Price         float64   `json:"p"`
	LogoImg       string  `json:"logo_img"`
	LeveragePower float64 `json:"leverage_power"`
}
