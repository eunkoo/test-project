package models

import (
	"fmt"
	"strings"
)

type Category struct {
	ID            int64      `json:"id"`
	TitleKo       string     `json:"title_ko"`
	DescriptionKo string     `json:"description_ko"`
	EtfList       []*EtfItem `json:"etf_list"`
	Priority      int64      `json:"priority"`
	CategoryImg   string     `json:"category_img"`
	Domicile      string     `json:"domicile"`
	ViewsChange   int32      `json:"views_change"`
	Feature       string     `json:"-"`
}

func (b *Category) ToString() []string {
	var result []string
	for _, etf := range b.EtfList {
		result = append(result, fmt.Sprintf("%d|%s|%s|%d|%s|%s|%s",
			b.ID,
			b.TitleKo,
			b.CategoryImg,
			b.Priority,
			b.DescriptionKo,
			b.Feature,
			etf.String()))
	}
	return result
}

type Description struct {
	En string `json:"en"`
	Ko string `json:"ko"`
}

type EtfItem struct {
	ID               int64   `json:"id"`
	NameKo           string  `json:"name_ko"`
	DescriptionKo    string  `json:"description_ko"`
	Symbol           string  `json:"symbol"`
	FirstTradingDate string  `json:"first_trading_date"`
	LeveragePower    float64 `json:"leverage_power"`
}

func (b *EtfItem) String() string {
	res := []string{
		fmt.Sprintf("%d", b.ID),
		b.NameKo,
		b.DescriptionKo,
		b.Symbol,
		strings.ReplaceAll(b.FirstTradingDate, "-", ""),
		fmt.Sprintf("%f", b.LeveragePower),
	}
	return strings.Join(res, "|")
}
