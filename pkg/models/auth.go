package models

import (
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	CompanyName string `form:"company_name" json:"company_name" xml:"company_name"  binding:"required"`
	Email       string `form:"email" json:"email" xml:"email" binding:"required"`
	Password    string `form:"password" json:"password" xml:"password" binding:"required"`
}

func (user *User) HashPassword(password string) error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		return err
	}
	user.Password = string(bytes)
	return nil
}
func (user *User) CheckPassword(providedPassword string) error {
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(providedPassword))
	if err != nil {
		return err
	}
	return nil
}

type Register_Client_b2b struct {
	ClientEmail    string `json:"email"`
	APIKey         string `json:"api_key"`
	ClientPassword string `json:"password"`
	CompanyName    string `json:"company_name"`
	ExpireDate     string `json:"expire_time"`
}

func (user *Register_Client_b2b) HashPassword_b2b(password string) error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		return err
	}
	user.ClientPassword = string(bytes)
	return nil
}
func (user *Register_Client_b2b) CheckPassword_b2b(providedPassword string) error {
	err := bcrypt.CompareHashAndPassword([]byte(user.ClientPassword), []byte(providedPassword))
	if err != nil {
		return err
	}
	return nil
}
