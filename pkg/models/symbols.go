package models

import (
	"fmt"
	"strconv"
	"strings"
)

type Symbol struct {
	ID               int64       `json:"id"`
	Tag              []string    `json:"tag"`
	Symbol           string      `json:"symbol"`
	Isin             string      `json:"isin"`
	Cusip            string      `json:"cusip"`
	Name             string      `json:"name"`
	NameKo           string      `json:"name_ko"`
	SecType          string      `json:"sec_type"`
	OrderRank        int64       `json:"order_rank"`
	DescriptionKo    string      `json:"description_ko"`
	FirstTradingDate interface{} `json:"first_trading_date"`
	LogoImg          string      `json:"logo_img"`
	Status           string      `json:"status"`
	Domicile         string      `json:"-"`
}

func (b *Symbol) String() string {
	res := []string{
		b.Symbol,
		b.Name,
		b.LogoImg,
		fmt.Sprintf("%d", b.OrderRank),
		strings.Join(b.Tag, ","),
		b.DescriptionKo,
		b.Cusip,
		b.Isin,
		b.NameKo,
		b.SecType,
		b.Domicile,
	}
	return strings.Join(res, "|")
}

type SymbolWithEtfDetail struct {
	Symbol
	EtfDetail EtfDetail_b2b `json:"etf_detail"`
}

func (b *SymbolWithEtfDetail) String() string {
	return StructToString(*b)
}

type EtfDetail struct {
	Cons                       []string `json:"cons"`
	Pros                       []string `json:"pros"`
	IndexCriteria              []string `json:"index_criteria"`
	SelectionCriteria          []string `json:"selection_criteria"`
	WeightingCriteria          []string `json:"weighting_criteria"`
	InceptionDate              string   `json:"inception_date"`
	LeveragePower              float64  `json:"leverage_power"`
	IsInverse                  bool     `json:"is_inverse"`
	IsLeveraged                bool     `json:"is_leveraged"`
	IsSectorExposureAvailable  bool     `json:"is_sector_exposure_available"`
	IsCountryExposureAvailable bool     `json:"is_country_exposure_available"`
	OriginEtf                  string   `json:"origin_etf"`
	Expense                    float64  `json:"expense"`
	BenchMark                  string   `json:"bench_mark"`
	FirstTradingDate           string   `json:"first_trading_date"`
	HomePage                   string   `json:"home_page"`
	Okatalk                    string   `json:"okatalk"`
	OkatalkName                string   `json:"okatalk_name"`
	OkatalkImg                 string   `json:"okatalk_img"`
}

func (b *EtfDetail) String() string {

	res := []string{
		strings.Join(b.Cons, "@"),
		strings.Join(b.Pros, "@"),
		strings.Join(b.IndexCriteria, "@"),
		strings.Join(b.SelectionCriteria, "@"),
		strings.Join(b.WeightingCriteria, "@"),
		b.InceptionDate,
		fmt.Sprintf("%f", b.LeveragePower),
		strconv.FormatBool(b.IsInverse),
		strconv.FormatBool(b.IsLeveraged),
		strconv.FormatBool(b.IsSectorExposureAvailable),
		strconv.FormatBool(b.IsCountryExposureAvailable),
		b.OriginEtf,
		fmt.Sprintf("%f", b.Expense),
		b.BenchMark,
		strings.ReplaceAll(b.FirstTradingDate, "-", ""),
		b.HomePage,
		b.Okatalk,
		b.OkatalkName,
		b.OkatalkImg,
	}
	return strings.Join(res, "|")
}

type SymbolSimple struct {
	ID     int64  `json:"id"`
	Symbol string `json:"symbol"`
	NameKo string `json:"name_ko"`
}

func (b *SymbolSimple) String() string {
	return StructToString(*b)
}

type Constituents struct {
	Symbol    string `json:"symbol"`
	Name      string `json:"name"`
	Name_Ko   string `json:"name_ko"`
	Isin      string `json:"isin"`
	Sector_ko string `json:"sector_ko"`
}

type Rank struct {
	Symbol        string  `json:"symbol"`
	Rank          int64   `json:"rank"`
	KoName        string  `json:"name_ko"`
	Value         float64 `json:"value"`
	LeveragePower float64 `json:"leverage_power"`
}

type SSEF struct {
	Symbol        string  `json:"symbol"`
	Name          string  `json:"name"`
	Name_ko       string  `json:"name_ko"`
	IsInverse     bool    `json:"is_inverse"`
	IsLeveraged   bool    `json:"is_leveraged"`
	LeveragePower float64 `json:"leverage_power"`
	OriginEtf     string  `json:"origin_etf"`
}

func (b *SSEF) String() string {
	return StructToString(*b)
}
