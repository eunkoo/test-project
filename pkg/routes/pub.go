package routes

import (
	handlers "ezar-b2b-api/pkg/controllers"
	auth "ezar-b2b-api/pkg/core/authentication"
	"ezar-b2b-api/pkg/middlewares"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func SetupRoutes(server *gin.Engine) *gin.Engine {
	server.Use(cors.New(middlewares.CorsMiddleware()))
	server.POST("login", auth.GenerateToken)
	server.POST("register", auth.RegisterUser)
	server.POST("authToken", auth.AuthToken)

	v1 := server.Group("/api/v1")
	public := v1.Group("/pub")

	//Middlewares

	public.Use(middlewares.AuthApiKey())

	b2b := public.Group("/b2b")
	{
		b2b.GET("/snp500list", handlers.GetSnp500list)
		b2b.GET("/nasdaq100list", handlers.GetNasdaq100list)
		b2b.GET("/dowjoneslist", handlers.GetDowJoneslist)

		b2b.GET("/etf_details", handlers.GetEtfDetails)
		b2b.GET("/categories", handlers.GetCategoryList)
		b2b.GET("/categories/:id", handlers.GetCategoryDetail)
		b2b.GET("MDD/:symbol", handlers.MDD)
		b2b.GET("etf-holdings", handlers.GetEtfHoldings)
		b2b.GET("etf-holdings/:symbol", handlers.GetEtfHoldingsBySymbol)
		b2b.GET("etf-country-weightings", handlers.GetEtfCountryWeightings)
		b2b.GET("etf-country-weightings/:symbol", handlers.GetEtfCountryWeightingsBySymbol)
		b2b.GET("etf-stock-exposure/:symbol", handlers.GetEtfSectorExposure)
		b2b.GET("etf-sector-weightings", handlers.GetEtfSectorWeightings)
		b2b.GET("etf-sector-weightings/:symbol", handlers.GetEtfSectorWeightingsBySymbol)
		b2b.GET("historical-price-full/:symbol", handlers.GetEtfHistoricalPriceFull)
		b2b.GET("etf-quote/:symbol", handlers.GetEtfQuote)
		b2b.GET("etf-price-only/:symbol", handlers.GetEtfPriceOnly)
		b2b.GET("etf-basic-stat/:symbol", handlers.GetEtfBasicStat)
		b2b.GET("historical-price-full/stock_dividend", handlers.GetHistoricalStockDividend)
		b2b.GET("historical-price-full/stock_dividend/:symbol", handlers.GetHistoricalStockDividendBySymbol)
		b2b.GET("historical-price-full/stock_dividend/rank", handlers.GetHistoricalStockDividendRank)
		b2b.GET("leverage-inverse-map", handlers.GetLeverageInverseMap)
		returns := b2b.Group("/returns")
		{
			returns.GET("period/:symbol", handlers.GetPeriodReturns)
			returns.GET("byyear/:symbol", handlers.GetReturnsByYear)
			returns.GET("regular/:symbol", handlers.GetRegularReturn)
			returns.GET("cumulative/:symbol", handlers.GetCumulativeReturns)
			returns.GET("annualized/:symbol", handlers.GetAnnualizedReturns)
			returns.GET("rank", handlers.GetReturnsRank)
		}
		symbols := b2b.Group("/symbols")
		{
			symbols.GET("", handlers.GetSymbolList)
			symbols.GET("/ranks/:group", handlers.GetEtfRanks)
			symbols.GET("/:symbol/images", handlers.GetSymbolImage)
			symbols.GET("/ssef", handlers.GetSSEF)
		}
	}
	return server
}
