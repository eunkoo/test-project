package logger

import (
	"time"

	"github.com/getsentry/sentry-go"
)

func SentryLogger(err error) {
	sentry.CaptureException(err)
	defer sentry.Flush(2 * time.Second)
}
