package auth

import (
	"context"
	"ezar-b2b-api/ent/linstockstocksapiclientkey"
	"ezar-b2b-api/pkg/core/db"
	"ezar-b2b-api/pkg/models"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func RegisterUser(c *gin.Context) {
	apikey := c.Request.Header.Get("X-API-Key")
	if apikey == "" {
		c.AbortWithError(401, fmt.Errorf("request does not contain an apikey"))
		return
	}

	db := db.Client()
	defer db.Close()

	query := db.LinstockStocksApiclientkey.
		Query().
		Where(linstockstocksapiclientkey.APIKey(apikey)).
		FirstX(context.Background())
	if query == nil || query.IsAdmin == false {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("You are not allowed to create new user"))
		return
	}
	var user models.Register_Client_b2b
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		c.Abort()
		return
	}
	if err := user.HashPassword_b2b(user.ClientPassword); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		c.Abort()
		return
	}

	date, err := time.Parse("2006-01-02", user.ExpireDate)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		c.Abort()
		return
	}

	ctx := context.Background()
	db.LinstockStocksApiclientkey.
		Create().
		SetClientPassword(user.ClientPassword).
		SetClientEmail(user.ClientEmail).
		SetCreatedAt(time.Now()).
		SetCompanyName(user.CompanyName).
		SetAPIKey(user.APIKey).
		SetExpireAt(date).
		SetIsAdmin(false).
		SetResponseFormat("json").
		SetIsExpired(date.Before(time.Now())).
		SaveX(ctx)

	c.SecureJSON(http.StatusCreated, gin.H{"userName": user.CompanyName, "email": user.ClientEmail})
}

type TokenRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
type LoginUser struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func GenerateToken(c *gin.Context) {
	var request TokenRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		c.Abort()
		return
	}
	// check if email exists and password is correct
	db := db.Client()
	defer db.Close()
	query := db.LinstockStocksApiclientkey.
		Query().
		Where(linstockstocksapiclientkey.ClientEmail(request.Email)).FirstX(context.Background())
	if query == nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid credentials"})
		c.Abort()
		return
	}
	user := models.User{CompanyName: query.CompanyName, Password: query.ClientPassword, Email: query.ClientEmail}

	credentialError := user.CheckPassword(request.Password)
	if credentialError != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "invalid credentials"})
		c.Abort()
		return
	}
	tokenString, err := GenerateJWT(user.Email, user.CompanyName)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		c.Abort()
		return
	}
	cookie1 := &http.Cookie{Name: "Authorization", Value: tokenString, HttpOnly: true}
	http.SetCookie(c.Writer, cookie1)
	c.SecureJSON(http.StatusOK, gin.H{"name": user.CompanyName, "email": user.Email, "apikey": query.APIKey, "is_admin": query.IsAdmin})
}

func AuthToken(c *gin.Context) {
	tokenString, err := c.Request.Cookie("Authorization")
	if err != nil {
		c.JSON(401, gin.H{"success": false,
			"response": "request does not contain an access token"})
		c.Abort()
		return
	}
	err = ValidateToken(tokenString.Value)
	if err != nil {
		c.JSON(401, gin.H{"success": false,
			"response": err.Error()})
		c.Abort()
		return
	}
	c.JSON(200, gin.H{"success": true,
		"response": "User Authenticated"})
}
