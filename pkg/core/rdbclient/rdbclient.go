package rdbclient

import (
	config "ezar-b2b-api/pkg/core/configs"
	"ezar-b2b-api/pkg/core/logger"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
)

func Write(remoteFile, payload string) (err error) {
	port := 22
	hostname := config.SFTP_HOST
	keypath := config.SFTP_KEYPATH
	user := config.SFTP_USER
	addr := fmt.Sprintf("%s:%d", hostname, port)
	authMethod, err := readPublicKey(keypath)
	if err != nil {
		logger.SentryLogger(fmt.Errorf("failed to read public key: %s", keypath))
		return err
	}
	config := ssh.ClientConfig{
		User:            user,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Auth:            authMethod,
	}
	conn, err := ssh.Dial("tcp", addr, &config)
	if err != nil {
		logger.SentryLogger(fmt.Errorf("failed to connect to [%s]: %v", addr, err))
		return err
	}
	defer conn.Close()
	fmt.Println("\n Connected to ftp server\n")

	sc, err := sftp.NewClient(conn)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to start SFTP subsystem: %v\n", err)
		logger.SentryLogger(fmt.Errorf("unable to start SFTP subsystem: %v", err))
		os.Exit(1)
		return err
	}
	defer sc.Close()

	return UploadFile(*sc, remoteFile, payload)
}

func readPublicKey(keypath string) ([]ssh.AuthMethod, error) {
	pKeyBytes, err := ioutil.ReadFile(keypath)
	if err != nil {
		return nil, err
	}
	signer, err := ssh.ParsePrivateKey(pKeyBytes)
	if err != nil {
		return nil, err
	}
	return []ssh.AuthMethod{
		ssh.PublicKeys(signer),
	}, nil
}

func UploadFile(sc sftp.Client, remoteFile, payload string) (err error) {
	fmt.Fprintf(os.Stdout, "Uploading data to [%s] ...\n", remoteFile)

	// Note: SFTP To Go doesn't support O_RDWR mode
	dstFile, err := sc.OpenFile(remoteFile, (os.O_WRONLY | os.O_CREATE | os.O_TRUNC))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to open remote file: %v\n", err)
		logger.SentryLogger(fmt.Errorf("unable to open remote file: %v", err))
		return
	}
	defer dstFile.Close()

	bytes, err := dstFile.Write([]byte(payload))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to upload local file: %v\n", err)
		logger.SentryLogger(fmt.Errorf("unable to upload local file: %v", err))
		os.Exit(1)
	}
	fmt.Fprintf(os.Stdout, "%d bytes copied\n", bytes)

	return
}

func listFiles(sc sftp.Client, remoteDir string) (err error) {
	fmt.Fprintf(os.Stdout, "Listing [%s] ...\n\n", remoteDir)

	log.Print(sc.Getwd())
	files, err := sc.ReadDir(remoteDir)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to list remote dir: %v\n", err)
		return
	}

	for _, f := range files {
		var name, modTime, size string

		name = f.Name()
		modTime = f.ModTime().Format("2006-01-02 15:04:05")
		size = fmt.Sprintf("%12d", f.Size())

		if f.IsDir() {
			name = name + "/"
			modTime = ""
			size = "PRE"
		}
		// Output each file name and size in bytes
		fmt.Fprintf(os.Stdout, "%19s %12s %s\n", modTime, size, name)
	}

	return
}
