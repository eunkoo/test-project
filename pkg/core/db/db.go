package db

import (
	"ezar-b2b-api/ent"
	config "ezar-b2b-api/pkg/core/configs"
	"ezar-b2b-api/pkg/core/logger"

	"entgo.io/ent/dialect/sql"
	_ "github.com/go-sql-driver/mysql"
)

func Driver() *sql.Driver {
	drv, err := sql.Open(config.DBEngine, config.DB_DNS)
	if err != nil {
		logger.SentryLogger(err)
	}
	err = drv.DB().Ping()
	return drv
}

func Client() *ent.Client {
	drv := Driver()
	return ent.NewClient(ent.Driver(drv))
}
