package redisClient

import "strconv"

type RedisPriceSet struct {
	Symbol   string  `json:"symbol"`
	Price    float64 `json:"price"`
	TradedAt string  `json:"tradedAt"`
}

type PricePoolModel struct {
	Symbol string `json:"symbol" structs:"symbol"`

	Pr      float64 `json:"price"`
	Cp      float64 `json:"cp"`
	PrePr   float64 `json:"pre_price"`
	PreCp   float64 `json:"pre_cp"`
	AfterPr float64 `json:"after_price"`
	AfterCp float64 `json:"after_cp"`

	Previous float64 `json:"previous" structs:"previous"`
	Cl       float64 `json:"close"`
}

func (r *PricePoolModel) Create(raw map[string]string) *PricePoolModel {
	price, _ := strconv.ParseFloat(raw["price"], 64)
	pre_price, _ := strconv.ParseFloat(raw["pre_price"], 64)
	after_price, _ := strconv.ParseFloat(raw["after_price"], 64)
	close, _ := strconv.ParseFloat(raw["close"], 64)
	prev, _ := strconv.ParseFloat(raw["previous"], 64)

	return &PricePoolModel{
		Symbol:   raw["symbol"],
		Pr:       price,
		PrePr:    pre_price,
		AfterPr:  after_price,
		Cl:       close,
		Previous: prev,
	}
}
