package redisClient

import (
	"context"
	config "ezar-b2b-api/pkg/core/configs"
	"ezar-b2b-api/pkg/utils"
	"fmt"
	"sort"
	"strconv"
	"sync"
	"time"

	mapset "github.com/deckarep/golang-set/v2"
	"github.com/go-redis/redis/v8"
)

func Connect() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     config.REDIS_HOST,
		Password: "",
		DB:       0,
	})
}

func GetPricePool(client *redis.Client, ctx context.Context, symbol string) (map[string]string, error) {
	ppKey := utils.GetRedisPricePoolKey(symbol)
	cmd := client.HGetAll(ctx, ppKey)
	err := cmd.Err()
	if err != nil {
		return nil, err
	}
	return cmd.Val(), nil
}

func GetRealtimePrice(client *redis.Client, symbol string) float64 {
	switch config.MarketOpenStatus {
	case 1:
		price, _ := GetPrice(client, symbol, "pre_price")
		return price
	case 3:
		price, _ := GetPrice(client, symbol, "after_price")
		return price
	default:
		price, _ := GetPrice(client, symbol, "price")
		return price
	}
}

func GetPrice(client *redis.Client, symbol string, field string) (float64, error) {
	ctx := context.Background()
	ppKey := fmt.Sprintf("PP:%s", symbol)
	rst, err := strconv.ParseFloat(client.HGet(ctx, ppKey, field).Val(), 64)
	if err != nil {
		return 0, nil
	}
	return rst, nil
}

func GetCp(client *redis.Client, sym string, rangeFlag string) float64 {
	ctx := context.Background()
	key := ""
	switch {
	case rangeFlag == "bf_5min":
		key = "CP:MIN5"
	case rangeFlag == "bf_60min":
		key = "CP:MIN60"
	case rangeFlag == "bf_1day":
		key = "CP:DAY"
	case rangeFlag == "bf_1week":
		key = "CP:WEEK1"
	case rangeFlag == "bf_1month":
		key = "CP:MONTH1"
	case rangeFlag == "bf_3month":
		key = "CP:MONTH3"
	case rangeFlag == "bf_1year":
		key = "CP:YEAR1"
	case rangeFlag == "bf_5year":
		key = "CP:YEAR5"
	default:
		return 0
	}
	cmd := client.ZScore(ctx, key, sym)
	if cmd.Err() != nil {
		fmt.Println(cmd.Err())
		return 0
	}
	return cmd.Val()
}

func GetCpRanksWithAll(client *redis.Client, rangeFlag string, sortFlag string, limit int64) ([]string, error) {
	rst := []string{}
	ctx := context.Background()
	key := ""
	switch {
	case rangeFlag == "bf_5min":
		key = "CP:MIN5"
	case rangeFlag == "bf_60min":
		key = "CP:MIN60"
	case rangeFlag == "bf_1day":
		key = "CP:DAY"
	case rangeFlag == "bf_1week":
		key = "CP:WEEK1"
	case rangeFlag == "bf_1month":
		key = "CP:MONTH1"
	case rangeFlag == "bf_3month":
		key = "CP:MONTH3"
	case rangeFlag == "bf_1year":
		key = "CP:YEAR1"
	case rangeFlag == "bf_5year":
		key = "CP:YEAR5"
	default:
		key = "CP:DAY"
	}

	switch sortFlag {
	case "ascending":
		rst = client.ZRange(ctx, key, 0, limit).Val()
	case "descending":
		rst = client.ZRevRange(ctx, key, 0, limit).Val()
	default:
		rst = client.ZRevRange(ctx, key, 0, limit).Val()
	}

	return rst, nil
}

type SymbolCP struct {
	Symbol string
	Cp     float64
}

func GetCpRanksWithSymbol(client *redis.Client, symbols []string, rangeFlag string, sortFlag string, limit int64, isHotCold bool) ([]string, error) {
	rst := []string{}
	symbolCps := []*SymbolCP{}
	ctx := context.Background()
	key := ""
	switch {
	case rangeFlag == "bf_5min":
		key = "CP:MIN5"
	case rangeFlag == "bf_60min":
		key = "CP:MIN60"
	case rangeFlag == "bf_1day":
		key = "CP:DAY"
	case rangeFlag == "bf_1week":
		key = "CP:WEEK1"
	case rangeFlag == "bf_1month":
		key = "CP:MONTH1"
	case rangeFlag == "bf_3month":
		key = "CP:MONTH3"
	case rangeFlag == "bf_1year":
		key = "CP:YEAR1"
	case rangeFlag == "bf_5year":
		key = "CP:YEAR5"
	default:
		key = "CP:DAY"
	}

	cmd := client.ZMScore(ctx, key, symbols...)
	if cmd.Err() != nil {
		return rst, nil
	}
	scores := cmd.Val()
	for idx, sym := range symbols {
		cp := scores[idx]
		symbolCps = append(symbolCps, &SymbolCP{
			Symbol: sym,
			Cp:     cp,
		})
	}

	switch {
	case isHotCold == true && sortFlag == "descending":
		symbolCps = utils.Filter(symbolCps, func(v *SymbolCP) bool {
			return v.Cp > 0
		})
	case isHotCold == true && sortFlag == "ascending":
		symbolCps = utils.Filter(symbolCps, func(v *SymbolCP) bool {
			return v.Cp < 0
		})
	}

	switch sortFlag {
	case "descending":
		sort.Slice(symbolCps, func(a, b int) bool {
			return symbolCps[a].Cp > symbolCps[b].Cp
		})
	case "ascending":
		sort.Slice(symbolCps, func(a, b int) bool {
			return symbolCps[a].Cp < symbolCps[b].Cp
		})
	default:
		sort.Slice(symbolCps, func(a, b int) bool {
			return symbolCps[a].Cp > symbolCps[b].Cp
		})
	}

	if len(symbolCps) > int(limit) {
		symbolCps = symbolCps[0:limit]
	}

	rst = utils.Map(symbolCps, func(s *SymbolCP) string { return s.Symbol })

	return rst, nil
}

func GetCpRanksWithSymbolWithCp(client *redis.Client, symbols []string, rangeFlag string, sortFlag string, limit int64, isHotCold bool) ([]*SymbolCP, error) {

	symbolCps := []*SymbolCP{}
	ctx := context.Background()
	key := ""
	switch {
	case rangeFlag == "bf_5min":
		key = "CP:MIN5"
	case rangeFlag == "bf_60min":
		key = "CP:MIN60"
	case rangeFlag == "bf_1day":
		key = "CP:DAY"
	case rangeFlag == "bf_1week":
		key = "CP:WEEK1"
	case rangeFlag == "bf_1month":
		key = "CP:MONTH1"
	case rangeFlag == "bf_3month":
		key = "CP:MONTH3"
	case rangeFlag == "bf_1year":
		key = "CP:YEAR1"
	case rangeFlag == "bf_5year":
		key = "CP:YEAR5"
	default:
		key = "CP:DAY"
	}

	cmd := client.ZMScore(ctx, key, symbols...)
	if cmd.Err() != nil {
		return symbolCps, nil
	}
	scores := cmd.Val()
	for idx, sym := range symbols {
		cp := scores[idx]
		symbolCps = append(symbolCps, &SymbolCP{
			Symbol: sym,
			Cp:     cp,
		})
	}

	switch {
	case isHotCold == true && sortFlag == "descending":
		symbolCps = utils.Filter(symbolCps, func(v *SymbolCP) bool {
			return v.Cp > 0
		})
	case isHotCold == true && sortFlag == "ascending":
		symbolCps = utils.Filter(symbolCps, func(v *SymbolCP) bool {
			return v.Cp < 0
		})
	}

	switch sortFlag {
	case "descending":
		sort.Slice(symbolCps, func(a, b int) bool {
			return symbolCps[a].Cp > symbolCps[b].Cp
		})
	case "ascending":
		sort.Slice(symbolCps, func(a, b int) bool {
			return symbolCps[a].Cp < symbolCps[b].Cp
		})
	default:
		sort.Slice(symbolCps, func(a, b int) bool {
			return symbolCps[a].Cp > symbolCps[b].Cp
		})
	}

	if len(symbolCps) > int(limit) {
		symbolCps = symbolCps[0:limit]
	}

	return symbolCps, nil
}
func SetMarketFlag(flag int64) int64 {
	client := Connect()
	defer client.Close()
	ctx := context.Background()
	key := "MARKET:SECTION"

	err := client.Set(ctx, key, flag, 0).Err()
	if err != nil {
		panic(fmt.Errorf("Fail setting Market flag at redis"))
	}
	return flag
}

func GetMarketFlag() int64 {
	client := Connect()
	defer client.Close()
	ctx := context.Background()
	key := "MARKET:SECTION"

	cmd := client.Get(ctx, key)
	err := cmd.Err()
	if err != nil {
		fmt.Println("Fail getting market flag at redis", err)
	}
	rst, _ := strconv.Atoi(cmd.Val())
	return int64(rst)
}

func GetHotColdSymbol() []string {
	client := Connect()
	defer client.Close()
	ctx := context.Background()
	switch config.MarketOpenStatus {
	case 1:
		now := time.Now().UTC()
		hour := now.Hour()
		amountLimit := 0
		var cmd *redis.StringSliceCmd
		switch hour {
		case 9:
			key := "VO:DAY"
			rankLimit := 30
			cmd = client.ZRevRange(ctx, key, 0, int64(rankLimit))
		case 10:
			key := "VO:DAY"
			rankLimit := 50
			cmd = client.ZRevRange(ctx, key, 0, int64(rankLimit))
		case 11:
			key := "VO:DAY"
			rankLimit := 50
			cmd = client.ZRevRange(ctx, key, 0, int64(rankLimit))
		default:
			key := "AMO:DAY"
			amountLimit = 150000
			cmd = client.ZRangeByScore(ctx, key, &redis.ZRangeBy{
				Min: fmt.Sprintf("%d", amountLimit),
				Max: "+Inf",
			})
		}

		err := cmd.Err()
		if err != nil {
			fmt.Println("GetHotColdSymbol", err, config.MarketOpenStatus)
		}
		return cmd.Val()
	default:
		rst := mapset.NewSet[string]()
		var wg sync.WaitGroup
		wg.Add(2)
		go func() {
			defer wg.Done()
			key := "AMO:DAY"
			cmd := client.ZRevRange(ctx, key, 0, 700)
			err := cmd.Err()
			if err != nil {
				fmt.Println("GetHotColdSymbol", err, config.MarketOpenStatus)
			}
			amoSymbols := cmd.Val()
			amoSymSet := mapset.NewSet(amoSymbols...)
			rst = rst.Union(amoSymSet)
		}()
		go func() {
			defer wg.Done()
			key := "VO:DAY"
			cmd := client.ZRevRange(ctx, key, 0, 300)
			err := cmd.Err()
			if err != nil {
				fmt.Println("GetHotColdSymbol", err, config.MarketOpenStatus)
			}
			voSymbols := cmd.Val()
			voSymSet := mapset.NewSet(voSymbols...)
			rst = rst.Union(voSymSet)
		}()
		wg.Wait()

		return rst.ToSlice()
	}
}
