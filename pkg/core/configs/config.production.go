//go:build production
// +build production

package config

import (
	"fmt"
	"os"
)

var Mode string = "Production"

var DBEngine = "mysql"
var DB_HOST = "prod-rds.c3zn8lnmsclj.ap-northeast-2.rds.amazonaws.com:3306"
var DB_ACCOUNT = os.Getenv("DB_ACCOUNT")
var DB_PASSWORD = os.Getenv("DB_PASSWORD")
var SCHEMA_NAME = "linstock_production"
var IMAGE_STORAGE = "https://linstock-back-production.s3.ap-northeast-2.amazonaws.com/static"
var DB_DNS = fmt.Sprintf("%s:%s@tcp(%s)/%s?parseTime=True", DB_ACCOUNT, DB_PASSWORD, DB_HOST, SCHEMA_NAME)

var FMP_HOST = "financialmodelingprep.com"
var FMP_API_KEY = os.Getenv("FMP_API_KEY")

var IEXCLOUD_HOST = "cloud.iexapis.com"
var IEXCLOUD_API_KEY = os.Getenv("IEXCLOUD_API_KEY")
var IEX_IMAGE_STORAGE = "storage.googleapis.com"

var LINSTOCK_HOST = "prc.linstock.us"

var TINGO_HOST = "api.tiingo.com"
var TINGO_PASSWORD = os.Getenv("TINGO_PASSWORD")
var TINGO_TOKEN = os.Getenv("TINGO_TOKEN")

var SFTP_HOST = "43.200.98.73"
var SFTP_KEYPATH = "pkg/core/configs/private-sftp-key"
var SFTP_USER = os.Getenv("SFTP_USER")

var EOD_HOST = "eodhistoricaldata.com"
var EOD_API_KEY = os.Getenv("EOD_API_KEY")

var POLYGON_HOST = "api.polygon.io"
var POLYGON_API_KEY = os.Getenv("POLYGON_API_KEY")

var REDIS_HOST = "mini-redis-ro.quvnne.ng.0001.apn2.cache.amazonaws.com:6379"

// 0: Close,
// 1: Pre-Market-Open,
// 2: Market-Open,
// 3: Post-Market-Open,

var MarketOpenStatus int64 = 0
