package utils

import (
	"context"
	"ezar-b2b-api/ent/linstockstockssymbol"
	"ezar-b2b-api/pkg/core/db"

	"entgo.io/ent/dialect/sql"
)

func GetSymbolByCusip(cusip []string) map[string]string {
	db := db.Client()
	defer db.Close()
	cusip_drive := AnyAllDrivVal(cusip)
	db_resp_cusip := db.LinstockStocksSymbol.Query().Where(func(s *sql.Selector) {
		s.Where(sql.InValues(linstockstockssymbol.FieldCusip, cusip_drive...)).Select(linstockstockssymbol.FieldNameKo, linstockstockssymbol.FieldCusip)
	}).AllX(context.Background())
	cusip_to_name := make(map[string]string)
	for _, v := range db_resp_cusip {
		cusip_to_name[v.Cusip] = v.NameKo
	}
	return cusip_to_name
}

func GetSymbolByIsin(isin []string) map[string]string {
	db := db.Client()
	defer db.Close()
	isin_drive := AnyAllDrivVal(isin)

	db_resp_isin := db.LinstockStocksSymbol.Query().Where(func(s *sql.Selector) {
		s.Where(sql.InValues(linstockstockssymbol.FieldIsin, isin_drive...)).Select(linstockstockssymbol.FieldNameKo, linstockstockssymbol.FieldIsin)
	}).AllX(context.Background())

	isin_to_name := make(map[string]string)
	for _, v := range db_resp_isin {
		isin_to_name[v.Isin] = v.NameKo
	}
	return isin_to_name
}
