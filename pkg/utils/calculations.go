package utils

import (
	"ezar-b2b-api/ent"
	"ezar-b2b-api/pkg/models"
	"math"
	"sort"
	"time"
)

func CalculateAnnualizedReturn(startDatePrice float64, endDatePrice float64, days float64) float64 {
	total_return := (endDatePrice - startDatePrice) / startDatePrice
	rst := math.Pow((1 + total_return), (365 / days))
	rst = (rst - 1)
	return KeepDecimal(rst*100, 2)
}

func CalculateCumulativeReturns(startDatePrice float64, endDatePrice float64) float64 {
	if startDatePrice == 0 {
		return 0
	}
	cummulative := (endDatePrice - startDatePrice) / startDatePrice
	return KeepDecimal(cummulative*100, 2)
}

func CalculateDividendYieldRank(symbol_map map[string]models.SymbolSimple, hist_map map[string]float64, dividend_map map[string][]models.StockDividendDb, dividend_map_year map[string][]models.StockDividendDb) []models.StockDividendRank_b2b {
	var rst []models.StockDividendRank_b2b

	for i, v := range dividend_map {

		var total float64 = 0
		var dividend_rate float64
		if len(v) == 0 {
			dividend_rate = 0
		} else {
			for _, v2 := range v {
				total += v2.Dividend
				if hist_map[i] != 0 && KeepDecimal((v2.Dividend/hist_map[i])*100, 3) > 20 {
					hist_map[i] = 0
					break
				}
			}
			if hist_map[i] != 0 {
				dividend_rate = KeepDecimal((total/hist_map[i])*100, 3)
			} else {
				dividend_rate = 0
			}
		}

		_, is := dividend_map_year[i]
		if is {
			var temp float64 = 0
			for _, v := range dividend_map_year[i] {
				temp += v.Dividend
			}
			prev_dividend_rate := KeepDecimal((temp/hist_map[i])*100, 3)
			if dividend_rate > prev_dividend_rate && dividend_rate > 3*prev_dividend_rate {
				dividend_rate = 0
			} else if dividend_rate < prev_dividend_rate && 3*dividend_rate < prev_dividend_rate {
				dividend_rate = 0
			}
		}

		cur := models.StockDividendRank_b2b{
			NameKo:        symbol_map[i].NameKo,
			Symbol:        i,
			DividendYield: dividend_rate,
		}

		rst = append(rst, cur)
	}

	sort.Slice(rst, func(i, j int) bool {
		return rst[i].DividendYield > rst[j].DividendYield
	})

	for i := 0; i < len(rst); i++ {
		rst[i].Rank = int64(i + 1)
	}

	if len(rst) >= 300 {
		return rst[:300]
	} else {
		return rst
	}
}

func CalculateDividendYield(symbol string, historical *models.HistoricalPriceEOD, dividend []*ent.LinstockStocksHistoricaldividendu) float64 {
	var total float64
	var rst float64
	cur, _ := time.Parse("2006-01-02", historical.Date)
	curYear := cur.AddDate(-1, 0, 0)
	if len(dividend) == 0 {
		rst = 0
	} else {
		for _, v := range dividend {
			if v.ExDividendDate.After(curYear) {
				total += v.Dividend
			} else {
				break
			}
		}

		rst = KeepDecimal(total/historical.AdjustedClose*100, 3)
	}

	return rst
}

func CalculateDividendYieldBatch(syms []string, historical map[string]*models.HistoricalPriceEOD, dividend_map map[string][]*ent.LinstockStocksHistoricaldividendu) map[string]float64 {
	rst := make(map[string]float64)
	for _, symbol := range syms {
		var total float64
		cur, _ := time.Parse("2006-01-02", historical[symbol].Date)
		curYear := cur.AddDate(-1, 0, 0)
		if len(dividend_map[symbol]) == 0 {
			rst[symbol] = 0
		} else {
			for _, v := range dividend_map[symbol] {
				if v.ExDividendDate.After(curYear) {
					total += v.Dividend
				} else {
					break
				}
			}
			rst[symbol] = KeepDecimal(total/historical[symbol].AdjustedClose*100, 3)
		}
	}

	return rst
}
