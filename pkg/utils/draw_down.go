package utils

import (
	"ezar-b2b-api/pkg/models"
	"math"
	"sort"
	"time"
)

type MddElem struct {
	max_drawdown float64
	peak         string
	trough       string
	peak_price   float64
	trough_price float64
}

func MaxDrawDown(points []*models.HistoricalPrice, first time.Time, last time.Time) MddElem {
	sort.Slice(points, func(i, j int) bool {
		first, _ := time.Parse("2006-01-02", points[i].Date)
		second, _ := time.Parse("2006-01-02", points[j].Date)
		return first.Before(second)
	})
	var max_drawdown float64 = 0
	var peak string
	var trough string
	var peak_price float64 = 0
	var trough_price float64 = 0
	var max_close = &models.HistoricalPrice{
		Date:          first.String(),
		AdjustedClose: math.Inf(-1),
		Close:         math.Inf(-1)}

	for _, v := range points {
		cur, _ := time.Parse("2006-01-02", v.Date)
		if cur.Before(first) {
			continue
		} else if cur.After(last) {
			break
		} else {
			cur_drawdown := (v.AdjustedClose - max_close.AdjustedClose) / max_close.AdjustedClose
			if cur_drawdown < max_drawdown {
				max_drawdown = cur_drawdown
				peak = max_close.Date
				trough = v.Date
				peak_price = max_close.AdjustedClose
				trough_price = v.AdjustedClose

			}
			if v.AdjustedClose > max_close.AdjustedClose {
				max_close = v
			}
		}
	}
	return MddElem{max_drawdown, peak, trough, peak_price, trough_price}
}

func secondMaxDrawDown(points []*models.HistoricalPrice, first time.Time, last time.Time) (MddElem, MddElem) {
	mdd := MaxDrawDown(points, first, last)
	peak, _ := time.Parse("2006-01-02", mdd.peak)
	trough, _ := time.Parse("2006-01-02", mdd.trough)
	var mdd2 MddElem
	op := MaxDrawDown(points, first, peak)
	op1 := MaxDrawDown(points, trough, last)
	if op1.max_drawdown > op.max_drawdown {
		mdd2 = op
	} else {
		mdd2 = op1
	}
	return mdd, mdd2
}

func thirdMaxDrawDown(points []*models.HistoricalPrice, first time.Time, last time.Time) (MddElem, MddElem, MddElem) {
	mdd1, mdd2 := secondMaxDrawDown(points, first, last)
	var mdd3 MddElem
	peak1, _ := time.Parse("2006-01-02", mdd1.peak)
	peak2, err := time.Parse("2006-01-02", mdd2.peak)
	if err != nil {
		return mdd1, mdd2, mdd3
	}
	trough1, _ := time.Parse("2006-01-02", mdd1.trough)
	trough2, err := time.Parse("2006-01-02", mdd2.trough)
	if err != nil {
		return mdd1, mdd2, mdd3
	}
	if peak1.After(peak2) && trough1.After(trough2) {
		op, op1, op2 := MaxDrawDown(points, first, peak2), MaxDrawDown(points, trough2, peak1), MaxDrawDown(points, trough1, last)
		mdd3, _ = FindMaxAndMin([]MddElem{op, op1, op2}, func(e MddElem) float64 { return e.max_drawdown })
	} else if peak2.After(peak1) && trough2.After(trough1) {
		op, op1, op2 := MaxDrawDown(points, first, peak1), MaxDrawDown(points, trough1, peak2), MaxDrawDown(points, trough2, last)
		mdd3, _ = FindMaxAndMin([]MddElem{op, op1, op2}, func(e MddElem) float64 { return e.max_drawdown })
	}
	return mdd1, mdd2, mdd3
}

func GetMDD(points []*models.HistoricalPrice, first time.Time, last time.Time) *models.DrawDown {
	mdd1, mdd2, mdd3 := thirdMaxDrawDown(points, first, last)
	sg := singleDayMaxDrawDown(points)
	mdds := MapTo([]MddElem{mdd1, mdd2, mdd3},
		func(e MddElem) models.DrawDownMax {
			return models.DrawDownMax{
				Drawdown: KeepDecimal(e.max_drawdown*100, 2),
				Peak:     models.DrawDownPeak{Date: e.peak, Price: e.peak_price},
				Trough:   models.DrawDownPeak{Date: e.trough, Price: e.trough_price},
			}
		})
	return &models.DrawDown{
		Max:       mdds[0],
		Second:    mdds[1],
		SingleDay: sg,
		Third:     mdds[2],
	}
}

func singleDayMaxDrawDown(points []*models.HistoricalPrice) models.DrawDownSingleDay {
	var max_drawdown float64 = 0
	var trough string
	var price float64
	for ind := 1; ind < len(points); ind++ {
		prev_close := points[ind-1]
		cur_close := points[ind]
		cur_drawdown := (cur_close.AdjustedClose - prev_close.AdjustedClose) / prev_close.AdjustedClose
		if cur_drawdown < max_drawdown {
			max_drawdown = cur_drawdown
			trough = cur_close.Date
			price = cur_close.AdjustedClose
		}
	}
	return models.DrawDownSingleDay{
		Drawdown: KeepDecimal(max_drawdown*100, 2),
		Trough:   trough,
		Price:    price,
	}
}
