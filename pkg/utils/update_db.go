package utils

import (
	"context"
	"encoding/json"
	"ezar-b2b-api/ent/linstockstockssymbol"
	config "ezar-b2b-api/pkg/core/configs"
	"ezar-b2b-api/pkg/models"

	"ezar-b2b-api/pkg/core/db"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"
)

func UpdateDB_exchange() {

	db := db.Client()
	defer db.Close()
	query := db.LinstockStocksSymbol.Query().AllX(context.Background())
	for _, symbol := range query {
		symb := symbol.Symbol
		query := url.Values{}
		query.Add("apikey", config.FMP_API_KEY)
		path := fmt.Sprintf("profile/%s", symb)
		url := url.URL{
			Scheme:   "https",
			Host:     config.FMP_HOST,
			Path:     fmt.Sprintf("/api/v3/%s", path),
			RawQuery: query.Encode(),
		}
		var result []interface{}
		resp, err := http.Get(url.String())
		if err != nil {
			continue
		}

		data, err := ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()
		if err != nil {
			continue
		}
		json.Unmarshal(data, &result)
		if len(result) == 0 {
			continue
		}
		cur := result[0].(map[string]interface{})
		exchange := cur["exchange"].(string)
		symbol.Update().SetExchange(exchange).SaveX(context.Background())
		log.Println(symbol.ID)
	}
}

func UpdateDB_status() {
	db := db.Client()
	defer db.Close()
	query := db.LinstockStocksSymbol.Query().AllX(context.Background())
	for _, symbol := range query {
		if symbol.DeletedAt.IsZero() {
			symbol.Update().SetStatus("delisted").SaveX(context.Background())
		} else {
			symbol.Update().SetStatus("active").SaveX(context.Background())
		}
	}
}

func First_Trade_Dates() {
	queryParams := url.Values{}
	queryParams.Add("token", config.TINGO_TOKEN)
	db := db.Client()
	defer db.Close()
	query := db.LinstockStocksSymbol.Query().WithLinstockStocksEtfdetail().AllX(context.Background())
	f, _ := os.Create("First_Dates.txt")
	defer f.Close()
	_, err2 := f.WriteString("Symbol Cusip Isin DB_First_Date Tiingo_First_Date\n")
	if err2 != nil {
		log.Println(err2)
		return
	}
	for _, v := range query {
		if v.Edges.LinstockStocksEtfdetail == nil {
			continue
		}
		first_dateDB := v.Edges.LinstockStocksEtfdetail.FirstTradingDate.Format("2006-01-02")
		url := url.URL{
			Scheme:   "https",
			Host:     config.TINGO_HOST,
			Path:     fmt.Sprintf("tiingo/daily/%s", v.Symbol),
			RawQuery: queryParams.Encode(),
		}
		resp, err := http.Get(url.String())
		if err != nil {
			log.Println("ERROR")
			continue
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println("ERROR")
			continue
		}
		var tingo_resp interface{}
		json.Unmarshal(data, &tingo_resp)
		tingo_m := tingo_resp.(map[string]interface{})
		tingo_first_date := tingo_m["startDate"]
		if tingo_first_date == nil {
			log.Println("ERROR")
			continue
		}
		tingo_date, err := time.Parse("2006-01-02", tingo_first_date.(string))
		if err != nil {
			log.Println("ERROR")
			continue
		}
		cusip := "null"
		if v.Cusip != "" {
			cusip = v.Cusip
		}
		isin := "null"
		if v.Isin != "" {
			isin = v.Isin
		}

		if first_dateDB != tingo_m["startDate"] {
			v.Edges.LinstockStocksEtfdetail.Update().SetFirstTradingDate(tingo_date).SaveX(context.Background())
			f.WriteString(fmt.Sprintf("%s %s %s %s %s\n", v.Symbol, cusip, isin, first_dateDB, tingo_first_date))
		}
	}
}

// func Test_Cumulative() {
// 	db := db.Client()
// 	defer db.Close()
// 	q := db.LinstockStocksSymbol.Query().Where(linstockstockssymbol.Or(linstockstockssymbol.IsEtf(true), linstockstockssymbol.IsEtn(true))).AllX(context.Background())
// 	f, _ := os.Create("Trouble.txt")
// 	for _, v := range q {
// 		log.Println(v.Symbol)
// 		res := handlers.GetCumulativeReturns(v.Symbol)
// 		log.Println(len(res))
// 		if len(res) != 1 {
// 			f.WriteString(fmt.Sprintf("%s\n", v.Symbol))
// 			continue
// 		}
// 		_, err := json.Marshal(res[0])
// 		if err != nil {
// 			f.WriteString(fmt.Sprintf("%s\n", v.Symbol))
// 		} else {
// 			log.Println(res[0])
// 		}
// 	}
// }

func Version_Table() {
	db := db.Client()
	defer db.Close()
	query := db.LinstockStocksSymbol.Query().Where(linstockstockssymbol.Status("active")).AllX(context.Background())
	for _, v := range query {
		db.LinstockStocksSymbolversion.Create().SetSymbol(v.Symbol).SetVersion(1).SaveX(context.Background())
	}
}

func getTradingAmount(symbol string) (float64, bool) {
	query := url.Values{}
	query.Add("apikey", config.FMP_API_KEY)
	path := fmt.Sprintf("quote/%s", symbol)
	url := url.URL{
		Scheme:   "https",
		Host:     config.FMP_HOST,
		Path:     fmt.Sprintf("/api/v3/%s", path),
		RawQuery: query.Encode(),
	}
	var result []interface{}
	resp, err := http.Get(url.String())
	if err != nil {
		return 0, true
	}

	data, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return 0, true
	}
	json.Unmarshal(data, &result)
	if len(result) == 0 {
		return 0, true
	}
	cur := result[0].(map[string]interface{})

	trade_amount := cur["previousClose"].(float64) * cur["volume"].(float64)

	return KeepDecimal(trade_amount, 2), false
}

type EodHistoricalDividendUs struct {
	Date            string  `json:"date"`
	DeclarationDate string  `json:"declarationDate"`
	RecordDate      string  `json:"recordDate"`
	PaymentDate     string  `json:"paymentDate"`
	Period          string  `json:"period"`
	Value           float64 `json:"value"`
	UnadjustedValue float64 `json:"unadjustedValue"`
	Currency        string  `json:"currency"`
}

func eodRequest(symbol string) ([]*EodHistoricalDividendUs, bool) {
	query := url.Values{}
	query.Add("fmt", "json")
	query.Add("api_token", config.EOD_API_KEY)
	path := fmt.Sprintf("%s.US", symbol)
	url := url.URL{
		Scheme:   "https",
		Host:     config.EOD_HOST,
		Path:     fmt.Sprintf("/api/div/%s", path),
		RawQuery: query.Encode(),
	}
	var res []*EodHistoricalDividendUs
	resp, err := http.Get(url.String())
	if err != nil {
		return nil, true
	}
	data, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return nil, true
	}
	json.Unmarshal(data, &res)
	if len(res) == 0 {
		return nil, true
	}
	Reverse(res)
	return res, false

}

func fmpHistoricalDividendUs(symbol string) ([]models.Historical, bool) {
	query := url.Values{}
	query.Add("apikey", config.FMP_API_KEY)
	path := fmt.Sprintf("/historical-price-full/stock_dividend/%s", symbol)
	url := url.URL{
		Scheme:   "https",
		Host:     config.FMP_HOST,
		Path:     fmt.Sprintf("/api/v3/%s", path),
		RawQuery: query.Encode(),
	}
	var original_data *models.StockDividend
	resp, err := http.Get(url.String())
	if err != nil {
		return nil, true
	}
	data, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return nil, true
	}
	json.Unmarshal(data, &original_data)
	return original_data.Historical, true
}

func HistoricalDividendUs() {
	db := db.Client()
	defer db.Close()

	symbols := db.LinstockStocksSymbol.
		Query().
		Where(linstockstockssymbol.Domicile("us"),
			linstockstockssymbol.DeletedAtIsNil(),
			linstockstockssymbol.Or(linstockstockssymbol.SecType("EF"), linstockstockssymbol.SecType("EN"), linstockstockssymbol.SecType("SSEF"))).
		AllX(context.Background())

	for _, symbol := range symbols {
		eod, _ := eodRequest(symbol.Symbol)
		fmp, _ := fmpHistoricalDividendUs(symbol.Symbol)
		var per string
		if len(eod) != 0 && len(fmp) != 0 {
			l := len(fmp)
			if l < len(eod) {
				l = len(eod)
			}
			todays := make(map[string][]EodHistoricalDividendUs)
			for i := 0; i < l; i++ {
				if i < len(fmp) {
					todays[fmp[i].Date] = append(todays[fmp[i].Date], EodHistoricalDividendUs{
						Date:            fmp[i].Date,
						DeclarationDate: fmp[i].DeclarationDate,
						RecordDate:      fmp[i].RecordDate,
						PaymentDate:     fmp[i].PaymentDate,
						Value:           fmp[i].AdjDividend,
						UnadjustedValue: fmp[i].Dividend,
					})
				}
				if i < len(eod) {
					todays[eod[i].Date] = append(todays[eod[i].Date], *eod[i])
					per = eod[i].Period
				}
			}
			for k := range todays {
				if len(todays[k]) == 1 {
					first := todays[k][0]
					date, _ := time.Parse("2006-01-02", first.Date)
					decl_date, _ := time.Parse("2006-01-02", first.DeclarationDate)
					record_date, _ := time.Parse("2006-01-02", first.RecordDate)
					payment_date, _ := time.Parse("2006-01-02", first.PaymentDate)
					period := first.Period
					if period != "" {
						period = per
					}
					db.LinstockStocksHistoricaldividendu.Create().
						SetSymbol(symbol.Symbol).
						SetExDividendDate(date).
						SetDeclarationDate(decl_date).
						SetRecordDate(record_date).
						SetPaymentDate(payment_date).
						SetFrequency(period).
						SetDividend(first.UnadjustedValue).
						SetAdjustedDividend(first.Value).SaveX(context.Background())
					continue
				} else {
					first, second := todays[k][0], todays[k][1]
					date, _ := time.Parse("2006-01-02", first.Date)
					if date.IsZero() == true || (second.Date != "" && first.Date != second.Date) {
						date, _ = time.Parse("2006-01-02", second.Date)
					}
					decl_date, _ := time.Parse("2006-01-02", first.DeclarationDate)
					if decl_date.IsZero() == true || (second.DeclarationDate != "" && first.DeclarationDate != second.DeclarationDate) {
						decl_date, _ = time.Parse("2006-01-02", second.DeclarationDate)
					}
					record_date, _ := time.Parse("2006-01-02", first.RecordDate)
					if record_date.IsZero() == true || (second.RecordDate != "" && first.RecordDate != second.RecordDate) {
						record_date, _ = time.Parse("2006-01-02", second.RecordDate)
					}
					payment_date, _ := time.Parse("2006-01-02", first.PaymentDate)
					if payment_date.IsZero() == true || (second.PaymentDate != "" && first.PaymentDate != second.PaymentDate) {
						payment_date, _ = time.Parse("2006-01-02", second.PaymentDate)
					}
					period := second.Period
					if period == "" {
						period = per
					}
					undadj_val := first.UnadjustedValue
					if undadj_val != second.UnadjustedValue {
						undadj_val = second.UnadjustedValue
					}
					val := first.Value
					if val != second.Value {
						val = second.Value
					}
					db.LinstockStocksHistoricaldividendu.Create().
						SetSymbol(symbol.Symbol).
						SetExDividendDate(date).
						SetDeclarationDate(decl_date).
						SetRecordDate(record_date).
						SetPaymentDate(payment_date).
						SetFrequency(period).
						SetDividend(undadj_val).
						SetAdjustedDividend(val).SaveX(context.Background())
				}
			}
			continue
		}
		if len(eod) == 0 && len(fmp) == 0 {
			continue
		}
		if len(eod) != 0 {
			for _, v := range eod {
				date, _ := time.Parse("2006-01-02", v.Date)
				decl_date, _ := time.Parse("2006-01-02", v.DeclarationDate)
				record_date, _ := time.Parse("2006-01-02", v.RecordDate)
				payment_date, _ := time.Parse("2006-01-02", v.PaymentDate)
				db.LinstockStocksHistoricaldividendu.Create().
					SetSymbol(symbol.Symbol).
					SetExDividendDate(date).
					SetDeclarationDate(decl_date).
					SetRecordDate(record_date).
					SetPaymentDate(payment_date).
					SetFrequency(v.Period).
					SetDividend(v.UnadjustedValue).
					SetAdjustedDividend(v.Value).SaveX(context.Background())
			}
		}
	}
}

func FirstTradingDateInsert() {
	queryParams := url.Values{}
	queryParams.Add("token", config.TINGO_TOKEN)
	db := db.Client()
	defer db.Close()
	query := db.LinstockStocksSymbol.
		Query().
		Where(linstockstockssymbol.Domicile("us"),
			linstockstockssymbol.DeletedAtIsNil(),
			linstockstockssymbol.Or(linstockstockssymbol.SecType("EF"), linstockstockssymbol.SecType("EN"), linstockstockssymbol.SecType("SSEF"))).WithLinstockStocksEtfdetail().
		AllX(context.Background())

	for _, v := range query {
		if v.Edges.LinstockStocksEtfdetail == nil || v.Edges.LinstockStocksEtfdetail.FirstTradingDate.IsZero() == false {
			continue
		}
		url := url.URL{
			Scheme:   "https",
			Host:     config.TINGO_HOST,
			Path:     fmt.Sprintf("tiingo/daily/%s", v.Symbol),
			RawQuery: queryParams.Encode(),
		}
		resp, err := http.Get(url.String())
		if err != nil {
			log.Println("ERROR")
			continue
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println("ERROR")
			continue
		}
		var tingo_resp interface{}
		json.Unmarshal(data, &tingo_resp)
		tingo_m := tingo_resp.(map[string]interface{})
		tingo_first_date := tingo_m["startDate"]
		if tingo_first_date == nil {
			log.Println("ERROR")
			continue
		}
		tingo_date, err := time.Parse("2006-01-02", tingo_first_date.(string))
		if err != nil {
			log.Println("ERROR")
			continue
		}
		log.Println(v.Symbol, tingo_date)
		v.Edges.LinstockStocksEtfdetail.Update().SetFirstTradingDate(tingo_date).SaveX(context.Background())
	}

}
