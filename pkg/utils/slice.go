package utils

import (
	"database/sql/driver"
)

func Map[T any, V any](vs []T, f func(T) V) []V {
	vsm := make([]V, len(vs))
	for i, v := range vs {
		vsm[i] = f(v)
	}
	return vsm
}

func AnyAllDrivVal(vs []string) []driver.Value {
	return Map(vs, func(t string) driver.Value {
		new, _ := driver.String.ConvertValue(t)
		return new
	})
}

func Reverse[T any](numbers []*T) {
	for i, j := 0, len(numbers)-1; i < j; i, j = i+1, j-1 {
		numbers[i], numbers[j] = numbers[j], numbers[i]
	}
}

func FindMaxAndMin[T any](arr []T, f func(T) float64) (min T, max T) {
	min = arr[0]
	max = arr[0]
	for _, el := range arr {
		if f(el) > f(max) {
			max = el
		}
		if f(el) < f(min) {
			min = el
		}
	}
	return min, max
}

func MapTo[T any, V any](arr []T, f func(T) V) []V {
	var rst []V
	for _, v := range arr {
		rst = append(rst, f(v))
	}
	return rst
}

func AnyAll[T any](vs []T) []any {
	return Map(vs, func(t T) any { return t })
}

func Filter[T any](vs []T, f func(T) bool) []T {
	vsf := make([]T, 0)
	for _, v := range vs {
		if f(v) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}

func Contains[T comparable](s []T, e T) bool {
	for _, v := range s {
		if v == e {
			return true
		}
	}
	return false
}
