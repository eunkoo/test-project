package utils

import (
	"ezar-b2b-api/pkg/core/rdbclient"
	"ezar-b2b-api/pkg/models"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func Response[T any](c *gin.Context, res T, apiName string) {
	format, err := c.Get("response_format")
	if !err {
		c.Error(fmt.Errorf("error: response_format is not set"))
		format = "json"
	}

	if format == "json" {
		c.JSON(http.StatusOK, &res)
	} else {
		var result []string
		remotePath := "shinhan/"
		switch apiName {
		case "category_id":
			category := any(res).(models.Category)
			result = category.ToString()
			remotePath += "category_id.txt"
		case "category":
			arr := any(res).([]*models.Category)
			for _, each := range arr {
				result = append(result, each.ToString()...)
			}
			remotePath += "category.txt"
		case "stock_dividend":
			stock_div := any(res).([]*models.StockDividend_b2b)
			for _, val := range stock_div {
				for _, dividend := range val.Historical {
					result = append(result, fmt.Sprintf("%s|%s", val.Symbol, dividend.String()))
				}
			}
			remotePath += "stock-dividend.txt"
		case "etf_details":
			arr := any(res).([]interface{})
			for _, val := range arr {
				detail := val.(*models.EtfDetail_b2b)
				result = append(result, detail.String())
			}
			remotePath += "etf_detail.txt"
		case "symbols":
			typeCond := c.Request.URL.Query().Get("type")
			arr := any(res).([]interface{})
			switch typeCond {
			case "simple":
				for _, val := range arr {
					symb := val.(*models.SymbolSimple)
					result = append(result, symb.String())
				}
			case "etf_detail":
				for _, val := range arr {
					symb := val.(*models.SymbolWithEtfDetail)
					result = append(result, symb.String())
				}
			default:
				for _, val := range arr {
					symb := val.(*models.Symbol)
					result = append(result, symb.String())
				}
			}
			remotePath += "symbols.txt"
		case "MDD":
			symbol, _ := c.Params.Get("symbol")
			mdd := any(res).(*models.DrawDown).String()
			result = append(result, fmt.Sprintf("%s|%s", symbol, mdd))
			remotePath += fmt.Sprintf("MDD/%s.txt", symbol)
		case "sector_exposure":
			arr := any(res).([]*models.StockExposure_b2b)
			for _, exposure := range arr {
				result = append(result, exposure.String())
			}
			remotePath += "etf-stock-exposure.txt"
		case "sector_weighting":
			arr := any(res).([]*models.SectorWeightings)
			for _, val := range arr {
				for _, weight := range val.Weightings {
					result = append(result, fmt.Sprintf("%s|%s", val.Symbol, weight.String()))
				}
			}
			remotePath += "etf-sector-weighting.txt"
		case "country_weighting":
			arr := any(res).([]*models.CountryWeightings)
			for _, val := range arr {
				for _, weight := range val.Weightings {
					result = append(result, fmt.Sprintf("%s|%s", val.Symbol, weight.String()))
				}
			}
			remotePath += "etf-country-weighting.txt"
		case "LeverageInverseMap":
			arr := any(res).([]models.LeverageInverseMap)
			for _, v := range arr {
				result = append(result, v.ToString()...)
			}
			remotePath += "leverage-inverse-map.txt"
		case "etf_holdings":
			arr := any(res).([]*models.EtfHoldings)
			for _, val := range arr {
				for _, holding := range val.Holdings {
					result = append(result, fmt.Sprintf("%s|%s", val.Symbol, holding.String()))
				}
			}
			remotePath += "etf-holdings.txt"
		case "ssef":
			arr := any(res).([]*models.SSEF)
			for _, each := range arr {
				result = append(result, each.String())
			}
			remotePath += "single-stock-etp.txt"
		}
		if remotePath == "" {
			c.AbortWithError(http.StatusInternalServerError, fmt.Errorf("error: No remote file name is provided"))
			return
		}
		err := rdbclient.Write(remotePath, strings.Join(result, "\n"))
		if err != nil {
			c.AbortWithError(http.StatusInternalServerError, err)
			return
		}
		c.String(http.StatusOK, "File is written to "+remotePath)
	}
}
