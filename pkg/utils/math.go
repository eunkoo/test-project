package utils

import (
	"math"
)

func KeepDecimal(x float64, unit int) float64 {
	multiples := []float64{0, 10, 100, 1000, 10000, 100000, 1000000}
	var number float64
	if unit > len(multiples) {
		number = math.Pow(10, float64(unit))
	} else {
		number = multiples[unit]
	}
	if x >= 0 {
		return math.Floor(x*number) / number
	} else {
		return math.Ceil(x*number) / number
	}
}
