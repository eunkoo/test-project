package utils

import (
	config "ezar-b2b-api/pkg/core/configs"
	"strings"
)

func AddImageStorage(src string) string {
	return strings.Join([]string{config.IMAGE_STORAGE, src}, "/")
}
