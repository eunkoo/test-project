package utils

import (
	"context"
	"ezar-b2b-api/ent"
	"ezar-b2b-api/ent/linstockcommonusstockearlyclose"
	"ezar-b2b-api/ent/linstockcommonusstockholiday"
	"ezar-b2b-api/pkg/core/db"
	"ezar-b2b-api/pkg/core/logger"
	"log"
	"strings"
	"time"
)

func GetNewYorkTime(value *time.Time) (*time.Time, error) {
	loc, err := time.LoadLocation("America/New_York")
	if err != nil {
		logger.SentryLogger(err)
		return nil, err
	}
	rst := value.In(loc)
	return &rst, nil
}

func GetDateString(value *time.Time) string {
	return value.Format("2006-01-02")
}

func GetTimeString(value *time.Time) string {
	return value.Format("15:04:05")
}

func ParseMarketOpenTime(value string) (string, error) {
	timeString := strings.ReplaceAll(strings.ReplaceAll(value, ".", ""), "ET", "UTC")
	time, err := time.Parse("03:04 pm MST", timeString)
	if err != nil {
		logger.SentryLogger(err)
		return "", err
	}

	return time.Format("15:04:05"), nil
}

func CheckHoliday(holidays []*ent.LinstockCommonUsstockholiday) (bool, error) {
	now := time.Now()
	today, err := GetNewYorkTime(&now)
	if err != nil {
		logger.SentryLogger(err)
		return true, err
	}
	todayStr := GetDateString(today)
	for _, v := range holidays {
		if GetDateString(&v.Holiday) == todayStr {
			return true, nil
		}
	}
	weekDay := int(today.Weekday())
	if weekDay == 0 || weekDay == 6 {
		return true, nil
	}
	return false, nil
}

func Diff(a time.Time, b time.Time) (year, month, day, hour, min, sec int) {
	if a.Location() != b.Location() {
		b = b.In(a.Location())
	}
	if a.After(b) {
		a, b = b, a
	}
	y1, M1, d1 := a.Date()
	y2, M2, d2 := b.Date()

	h1, m1, s1 := a.Clock()
	h2, m2, s2 := b.Clock()

	year = int(y2 - y1)
	month = int(M2 - M1)
	day = int(d2 - d1)
	hour = int(h2 - h1)
	min = int(m2 - m1)
	sec = int(s2 - s1)

	// Normalize negative values
	if sec < 0 {
		sec += 60
		min--
	}
	if min < 0 {
		min += 60
		hour--
	}
	if hour < 0 {
		hour += 24
		day--
	}
	if day < 0 {
		// days in month:
		t := time.Date(y1, M1, 32, 0, 0, 0, 0, time.UTC)
		day += 32 - t.Day()
		month--
	}
	if month < 0 {
		month += 12
		year--
	}

	return
}

// func ParseMarketOpenTime(value string) (string, error) {
// 	timeString := strings.ReplaceAll(strings.ReplaceAll(value, ".", ""), "ET", "UTC")
// 	time, err := time.Parse("03:04 pm MST", timeString)
// 	if err != nil {
// 		return "", err
// 	}

// 	return time.Format("15:04:05"), nil
// }

func GetSessionTimeStatus() {
	loc, _ := time.LoadLocation("America/New_York")
	newYorkTime := time.Now().In(loc)
	newYorkNow := newYorkTime.Format("15:04:05")

	client := db.Client()
	defer client.Close()

	status := "off"
	hoursLeft := 0
	minutesLeft := 0
	secondsLeft := 0

	holiday := client.LinstockCommonUsstockholiday.Query().Where(linstockcommonusstockholiday.Holiday(newYorkTime)).FirstX(context.Background())
	if holiday != nil {
		early_close := client.LinstockCommonUsstockearlyclose.
			Query().
			Where(linstockcommonusstockearlyclose.Day(newYorkTime)).
			Select(linstockcommonusstockearlyclose.FieldOpen, linstockcommonusstockearlyclose.FieldClose, linstockcommonusstockearlyclose.FieldPost, linstockcommonusstockearlyclose.FieldPre).
			FirstX(context.Background())

		if early_close != nil {
			times := make(map[string]string)

			time_parse, err := ParseMarketOpenTime(early_close.Open)
			if err != nil {
				log.Println(err)
				return
			}
			times["open"] = time_parse

			time_parse, err = ParseMarketOpenTime(early_close.Close)
			if err != nil {
				log.Println(err)
				return
			}
			times["close"] = time_parse

			time_parse, err = ParseMarketOpenTime(early_close.Pre)
			if err != nil {
				log.Println(err)
				return
			}
			times["pre"] = time_parse

			time_parse, err = ParseMarketOpenTime(early_close.Post)
			if err != nil {
				log.Println(err)
				return
			}
			times["post"] = time_parse
			switch {
			case newYorkNow < times["pre"] || newYorkNow > times["post"]:
				f, _ := time.Parse("15:04:05", newYorkNow)
				s, _ := time.Parse("15:04:05", times["pre"])
				_, _, _, hoursLeft, minutesLeft, secondsLeft = Diff(f, s)
				status = "off"
				break
			case newYorkNow < times["post"] && newYorkNow > times["close"]:
				f, _ := time.Parse("15:04:05", newYorkNow)
				s, _ := time.Parse("15:04:05", times["post"])
				_, _, _, hoursLeft, minutesLeft, secondsLeft = Diff(f, s)
				status = "post"
				break
			case newYorkNow < times["close"] && newYorkNow > times["open"]:
				f, _ := time.Parse("15:04:05", newYorkNow)
				s, _ := time.Parse("15:04:05", times["close"])
				_, _, _, hoursLeft, minutesLeft, secondsLeft = Diff(f, s)
				status = "regular"
				break
			case newYorkNow < times["open"] && newYorkNow > times["pre"]:
				f, _ := time.Parse("15:04:05", newYorkNow)
				s, _ := time.Parse("15:04:05", times["open"])
				_, _, _, hoursLeft, minutesLeft, secondsLeft = Diff(f, s)
				status = "pre"
				break
			}
			log.Println(status, hoursLeft, minutesLeft, secondsLeft)
			return
		} else {
			log.Println("Holiday Today")
			return
		}
	}
	query := client.LinstockCommonUsstocktime.Query().AllX(context.Background())
	times := make(map[string]string)
	for _, v := range query {
		time, err := ParseMarketOpenTime(v.Time)
		if err != nil {
			log.Println(err)
			return
		}
		times[v.Title] = time
	}
	switch {
	case newYorkNow < times["pre"] || newYorkNow > times["post"]:
		f, _ := time.Parse("15:04:05", newYorkNow)
		s, _ := time.Parse("15:04:05", times["pre"])
		_, _, _, hoursLeft, minutesLeft, secondsLeft = Diff(f, s)
		status = "off"
		break
	case newYorkNow < times["post"] && newYorkNow > times["close"]:
		f, _ := time.Parse("15:04:05", newYorkNow)
		s, _ := time.Parse("15:04:05", times["post"])
		_, _, _, hoursLeft, minutesLeft, secondsLeft = Diff(f, s)
		status = "post"
		break
	case newYorkNow < times["close"] && newYorkNow > times["open"]:
		f, _ := time.Parse("15:04:05", newYorkNow)
		s, _ := time.Parse("15:04:05", times["close"])
		_, _, _, hoursLeft, minutesLeft, secondsLeft = Diff(f, s)
		status = "regular"
		break
	case newYorkNow < times["open"] && newYorkNow > times["pre"]:
		f, _ := time.Parse("15:04:05", newYorkNow)
		s, _ := time.Parse("15:04:05", times["open"])
		_, _, _, hoursLeft, minutesLeft, secondsLeft = Diff(f, s)
		status = "pre"
		break
	}
	log.Println(status, hoursLeft, minutesLeft, secondsLeft)
}
