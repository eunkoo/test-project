package utils

import (
	"ezar-b2b-api/pkg/models"
)

func TransformHolder(v *models.Holder, nameKo string) *models.Holder_b2b {

	return &models.Holder_b2b{
		Asset:            v.Asset,
		Cusip:            v.Cusip,
		Isin:             v.Isin,
		MarketValue:      v.MarketValue,
		Name:             v.Name,
		Name_KO:          nameKo,
		SharesNumber:     v.SharesNumber,
		Updated:          v.Updated,
		WeightPercentage: v.WeightPercentage,
	}
}

func TransformEtfSectorExposure(
	asset_exposure string,
	origin_etf string,
	nameKo string,
	market_value float64,
	shares_number int64,
	leverage_power float64,
	weight_percent float64) *models.StockExposure_b2b {
	return &models.StockExposure_b2b{
		AssetExposure:    asset_exposure,
		EtfSymbol:        origin_etf,
		EtfNameKo:        nameKo,
		MarketValue:      market_value,
		SharesNumber:     shares_number,
		WeightPercentage: weight_percent,
		LeveragePower:    leverage_power,
	}
}

func TransformHistoricalStockDividend(v *models.Historical, payment *float64) *models.Historical_b2b {
	rst := &models.Historical_b2b{
		AdjDividend:     v.AdjDividend,
		Date:            v.Date,
		DeclarationDate: v.DeclarationDate,
		Dividend:        v.Dividend,
		PaymentDate:     v.PaymentDate,
		RecordDate:      v.RecordDate,
	}
	if payment != nil && v.PaymentDate != "" {
		rst.PayoutPriceRatio = KeepDecimal(v.AdjDividend/(*payment)*100, 3)
	}
	// need fix, but not in use i think
	// rst.PayoutCloseRatio = KeepDecimal((v.AdjDividend/(*payment)[0].Close)*100, 3)
	return rst
}

func TransformHistoricalPrice(v *models.HistoricalPrice) *models.HistoricalPrice_b2b {
	return &models.HistoricalPrice_b2b{
		AdjustedClose: v.AdjustedClose,
		Close:         v.Close,
		Date:          v.Date,
	}
}

func TransformStockQuote(v *models.StockQuote) *models.StockQuote_b2b {
	return &models.StockQuote_b2b{
		Symbol:         v.Symbol,
		Name:           v.Name,
		Price:          v.Price,
		ChangesPercent: v.ChangesPercentage,
		Change:         v.Change,
		DayLow:         v.DayLow,
		DayHigh:        v.DayHigh,
		YearHigh:       v.YearHigh,
		YearLow:        v.YearLow,
		MarketCap:      v.MarketCap,
		PriceAvg50:     v.PriceAvg50,
		PriceAvg200:    v.PriceAvg200,
		Volume:         v.Volume,
		AvgVolume:      v.AvgVolume,
		Exchange:       v.Exchange,
		Open:           v.Open,
		PreviousClose:  v.PreviousClose,
		Timestamp:      v.Timestamp,
	}
}

func TransformEtfBasicStats(v *models.DetailStats) *models.Stats_b2b {
	return &models.Stats_b2b{
		CompanyName:               v.Stats.CompanyName,
		Marketcap:                 v.Stats.Marketcap,
		Week52High:                v.Stats.Week52High,
		Week52Low:                 v.Stats.Week52Low,
		Week52HighSplitAdjustOnly: v.Stats.Week52HighSplitAdjustOnly,
		Week52LowSplitAdjustOnly:  v.Stats.Week52LowSplitAdjustOnly,
		Week52Change:              v.Stats.Week52Change,
		Avg10DVolume:              v.Stats.Avg10Volume,
		Avg30DVolume:              v.Stats.Avg30Volume,
		Day200MovingAvg:           v.Stats.Day200MovingAvg,
		Day50MovingAvg:            v.Stats.Day50MovingAvg,
		TtmDividendRate:           v.Stats.TtmDividendRate,
		DividendYield:             v.Stats.DividendYield,
		NextDividendDate:          v.Stats.NextDividendDate,
		ExDividendDate:            v.Stats.ExDividendDate,
		Beta:                      v.Stats.Beta,
		Year5ChangePercent:        KeepDecimal(100*v.Stats.Year5ChangePercent, 2),
		Year2ChangePercent:        KeepDecimal(100*v.Stats.Year2ChangePercent, 2),
		Year1ChangePercent:        KeepDecimal(100*v.Stats.Year1ChangePercent, 2),
		YtdChangePercent:          KeepDecimal(100*v.Stats.YtdChangePercent, 2),
		Month6ChangePercent:       KeepDecimal(100*v.Stats.Month6ChangePercent, 2),
		Month3ChangePercent:       KeepDecimal(100*v.Stats.Month3ChangePercent, 2),
		Month1ChangePercent:       KeepDecimal(100*v.Stats.Month1ChangePercent, 2),
		Day30ChangePercent:        KeepDecimal(100*v.Stats.Day30ChangePercent, 2),
		Day5ChangePercent:         KeepDecimal(100*v.Stats.Day5ChangePercent, 2),
	}
}
