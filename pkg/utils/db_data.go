package utils

import (
	"context"
	"ezar-b2b-api/ent/linstockstockssymbol"
	"ezar-b2b-api/pkg/core/db"
	"fmt"
	"math"
	"sync"
)

var HotCatIds []int32 = []int32{676, 251}
var ColdCatIds []int32 = []int32{677, 252}

func GetRedisPricePoolKey(symbol string) string {
	return fmt.Sprintf("PP:%s", symbol)
}

func GetCp(prev float64, price float64) float64 {
	if prev == 0 {
		return 0
	}
	cp := (price - prev) / prev
	return math.Round(cp*10000) / 10000
}

func IsHotColdRank(id int32) bool {
	target := []int32{}
	target = append(target, ColdCatIds...)
	target = append(target, HotCatIds...)
	return Contains(target, id)
}

var NameKoByIsinCusip map[string]string = map[string]string{}

func CheckValidETFSymbols(symbols []string) bool {
	db := db.Client()
	defer db.Close()
	ctx := context.Background()
	rst, err := db.LinstockStocksSymbol.Query().Where(
		linstockstockssymbol.And(
			linstockstockssymbol.Status("active"),
			linstockstockssymbol.SymbolIn(symbols...),
			linstockstockssymbol.Not(linstockstockssymbol.SecType("ST")),
			linstockstockssymbol.Domicile("us"),
		),
	).Select(
		linstockstockssymbol.FieldSymbol,
	).Strings(ctx)

	if err != nil {
		return false
	}

	if len(rst) == 0 {
		return false
	}

	return true
}

func GetValidETFSymbols() []string {
	db := db.Client()
	defer db.Close()
	ctx := context.Background()
	rst, err := db.LinstockStocksSymbol.Query().Where(
		linstockstockssymbol.And(
			linstockstockssymbol.Status("active"),
			linstockstockssymbol.Not(linstockstockssymbol.SecType("ST")),
			linstockstockssymbol.Domicile("us"),
		),
	).Select(
		linstockstockssymbol.FieldSymbol,
	).Strings(ctx)

	if err != nil {
		return make([]string, 0)
	}

	return rst
}

type NameKoWithIsinCusip struct {
	NameKo string `json:"name_ko"`
	Cusip  string `json:"cusip"`
	Isin   string `json:"isin"`
}

func GetMapNameKoByCusipIsin() {
	db := db.Client()
	defer db.Close()
	ctx := context.Background()
	var data []*NameKoWithIsinCusip

	db.LinstockStocksSymbol.Query().
		Where(
			linstockstockssymbol.Status("active"),
			linstockstockssymbol.Domicile("us"),
		).
		Select(
			linstockstockssymbol.FieldNameKo,
			linstockstockssymbol.FieldIsin,
			linstockstockssymbol.FieldCusip,
		).
		Scan(ctx, &data)

	rst := make(map[string]string)
	var wg sync.WaitGroup
	wg.Add(len(data))
	for _, v := range data {
		go func(v *NameKoWithIsinCusip) {
			defer wg.Done()
			rst[v.Cusip] = v.NameKo
			rst[v.Isin] = v.NameKo
		}(v)
	}
	wg.Wait()
	rst[""] = ""
	NameKoByIsinCusip = rst
}
