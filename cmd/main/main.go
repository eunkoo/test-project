package main

import (
	"context"
	"ezar-b2b-api/docs"
	_ "ezar-b2b-api/docs"
	config "ezar-b2b-api/pkg/core/configs"
	"ezar-b2b-api/pkg/core/db"
	"ezar-b2b-api/pkg/routes"
	"ezar-b2b-api/pkg/utils"
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime"
	"time"

	"github.com/getsentry/sentry-go"
	sentrygin "github.com/getsentry/sentry-go/gin"
	"github.com/gin-gonic/gin"
	"github.com/robfig/cron"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title           eZar B2B API
// @version         1.0
// @description     Client data request serving server api
// @termsOfService  http://swagger.io/terms/

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html
// @securityDefinitions.apikey ApiKeyAuth
// @in 				header
// @name 			X-API-Key

// @host       public-api-us.ezar.co.kr
// @BasePath  /api/v1/pub/b2b/

func main() {

	log.Print("This is the ", config.Mode, " mode.")
	runtime.GOMAXPROCS(1)
	docs.SwaggerInfo.Title = os.Getenv("SWAG_TITLE")
	docs.SwaggerInfo.Description = os.Getenv("SWAG_DESCRPTION")
	docs.SwaggerInfo.Version = os.Getenv("SWAG_API_VERSION")
	docs.SwaggerInfo.Host = os.Getenv("SWAG_API_HOST")
	docs.SwaggerInfo.BasePath = os.Getenv("SWAG_API_BASE_HREF")
	utils.GetMapNameKoByCusipIsin()
	initScheduler()
	if config.Mode == "Production" {
		setupSentry()
		gin.SetMode(gin.ReleaseMode)
	}
	go func() {
		for {
			SetMarketOpen()
			time.Sleep(time.Second)
		}
	}()
	server := gin.Default()

	server.Use(sentrygin.New(sentrygin.Options{
		Repanic: true,
	}))

	server = routes.SetupRoutes(server)
	server.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	server.NoRoute(func(c *gin.Context) {
		// Return a 404 error response
		c.JSON(http.StatusNotFound, gin.H{"error": "not jin  found"})
	})
	server.Run(":8080")
}

func initScheduler() {
	l, _ := time.LoadLocation("Asia/Seoul")
	c := cron.NewWithLocation(l)

	// Daily tasks AM 08:00 TZ=Asia/Seoul
	c.AddFunc("0 0 8 * * *", func() { utils.GetMapNameKoByCusipIsin() })
	c.Start()
}

func setupSentry() {
	if err := sentry.Init(sentry.ClientOptions{
		Dsn: "https://9387af33e97f44238081dcd5077a9b2a@o1222483.ingest.sentry.io/4504678818054144",
		// Set TracesSampleRate to 1.0 to capture 100%
		// of transactions for performance monitoring.
		// We recommend adjusting this value in production,
		TracesSampleRate: 1.0,
	}); err != nil {
		fmt.Printf("Sentry initialization failed: %v\n", err)
	}
}

func SetMarketOpen() {
	db := db.Client()
	defer db.Close()
	ctx := context.Background()
	now := time.Now()
	newYorkNow, err := utils.GetNewYorkTime(&now)
	if err != nil {
		log.Println(err)
		return
	}
	newYorkTime := utils.GetTimeString(newYorkNow)

	holidays, err := db.LinstockCommonUsstockholiday.Query().All(ctx)
	if err != nil {
		fmt.Println(err)
		return
	}
	isHoliday, err := utils.CheckHoliday(holidays)
	if err != nil {
		fmt.Println(err)
		return
	}
	if isHoliday {
		config.MarketOpenStatus = 0
		return
	}

	openTimes, err := db.LinstockCommonUsstocktime.Query().All(ctx)
	if err != nil {
		fmt.Println(err)
		return
	}
	times := make(map[string]string)
	for _, v := range openTimes {
		time, err := utils.ParseMarketOpenTime(v.Time)
		if err != nil {
			log.Println(err)
			return
		}
		times[v.Title] = time
	}

	switch {
	case newYorkTime > times["post"] || newYorkTime < times["pre"]:
		config.MarketOpenStatus = 0
		break
	case newYorkTime < times["post"] && newYorkTime > times["close"]:

		config.MarketOpenStatus = 3
		break
	case newYorkTime < times["close"] && newYorkTime > times["open"]:
		config.MarketOpenStatus = 2
		break
	case newYorkTime < times["open"] && newYorkTime > times["pre"]:
		config.MarketOpenStatus = 1
		break
	}
}
