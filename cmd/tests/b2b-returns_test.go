package tests

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetAnnualizedReturns(t *testing.T) {
	testPath := "returns/annualized/QQQ,SPY"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetCumulativeReturns(t *testing.T) {
	testPath := "returns/cumulative/QQQ,SPY"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetReturnsByYear(t *testing.T) {
	testPath := "returns/byyear/QQQ,SPY"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetRegularReturn(t *testing.T) {
	testPath := "returns/regular/QQQ"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	q := req.URL.Query()
	q.Add("from", "2020-01-02")
	q.Add("to", "2023-01-02")

	frequncies := []string{"daily", "weekly", "monthly"}

	for _, freq := range frequncies {
		q.Del("frequency")
		q.Add("frequency", freq)
		req.URL.RawQuery = q.Encode()
		BasicTest(t, w, req)
	}
}

func TestGetPeriodReturns(t *testing.T) {
	testPath := "returns/byyear/QQQ"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	q := req.URL.Query()
	q.Add("from", "2020-01-02")
	q.Add("to", "2023-01-02")

	BasicTest(t, w, req)
}

func TestGetReturnsRank(t *testing.T) {
	testPath := "returns/rank"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	q := req.URL.Query()

	cases := map[string][5]string{
		"EF":   {"1d", "1m", "3m", "1y", "5y"},
		"EN":   {"1d", "1m", "3m", "1y", "5y"},
		"SSEF": {"1d", "1m", "3m", "1y", "5y"},
	}
	for secType, periods := range cases {
		q.Add("sec_type", secType)
		for _, period := range periods {
			q.Del("period")
			q.Add("period", period)
			req.URL.RawQuery = q.Encode()
			BasicTest(t, w, req)
		}
		q.Del("sec_type")
	}
}
