package tests

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetSnp500list(t *testing.T) {
	testPath := "snp500list"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetNasdaq100list(t *testing.T) {
	testPath := "nasdaq100list"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetDowJoneslist(t *testing.T) {
	testPath := "dowjoneslist"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}
