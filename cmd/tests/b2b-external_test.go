package tests

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetEtfHoldings(t *testing.T) {
	testPath := "etf-holdings"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetEtfHoldingsBySymbol(t *testing.T) {
	testPath := "etf-holdings/QQQ"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetEtfCountryWeightings(t *testing.T) {
	testPath := "etf-country-weightings"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetEtfCountryWeightingsBySymbol(t *testing.T) {
	testPath := "etf-country-weightings/QQQ"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetEtfSectorWeightings(t *testing.T) {
	testPath := "etf-sector-weightings"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetEtfSectorWeightingsBySymbol(t *testing.T) {
	testPath := "etf-sector-weightings/QQQ"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetEtfSectorExposure(t *testing.T) {
	testPath := "etf-stock-exposure/QQQ"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetHistoricalStockDividend(t *testing.T) {
	testPath := "historical-price-full/stock_dividend"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetHistoricalStockDividendBySymbol(t *testing.T) {
	testPath := "historical-price-full/stock_dividend/QQQ"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetHistoricalStockDividendRank(t *testing.T) {
	testPath := "historical-price-full/stock_dividend/rank"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetEtfHistoricalPriceFull(t *testing.T) {
	testPath := "historical-price-full/QQQ"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetEtfQuote(t *testing.T) {
	testPath := "etf-quote/QQQ,SPY"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetEtfPriceOnly(t *testing.T) {
	testPath := "etf-price-only/QQQ,SPY"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetEtfBasicStat(t *testing.T) {
	testPath := "etf-basic-stat/QQQ,SPY"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestMDD(t *testing.T) {
	testPath := "MDD/QQQ"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	q := req.URL.Query()
	q.Add("from", "2015-08-20")
	q.Add("to", "2022-08-20")

	req.URL.RawQuery = q.Encode()

	BasicTest(t, w, req)
}

func TestGetLeverageInverseMap(t *testing.T) {
	testPath := "leverage-inverse-map"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}
