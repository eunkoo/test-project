package tests

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetCategoryList(t *testing.T) {
	testPath := "categories"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	q := req.URL.Query()
	q.Add("domicile", "us")
	q.Add("end_user", "shinhan")
	req.URL.RawQuery = q.Encode()
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}

func TestGetCategoryDetail(t *testing.T) {
	testPath := "categories/1"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}
