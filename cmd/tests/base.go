package tests

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func BasicTest(t *testing.T, w *httptest.ResponseRecorder, req *http.Request) {
	Router.ServeHTTP(w, req)
	data, _ := ioutil.ReadAll(w.Body)
	assert.Equal(t, http.StatusOK, w.Code)
	assert.NotEmpty(t, data)
	assert.NotNil(t, data)
	assert.NotEqual(t, string(data), "null")
}
