package tests

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetSymbolImage(t *testing.T) {
	testPath := "symbols/QQQ/images"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	Router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusFound, w.Code)
	assert.NotEmpty(t, http.StatusOK, w.Body)
	assert.NotNil(t, http.StatusOK, w.Body)
}

func TestGetSymbolList(t *testing.T) {
	testPath := "symbols"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()
	q := req.URL.Query()

	cases := []map[string]string{
		{
			"spec": "all",
		},
		{
			"spec": "QQQ,SPY",
		},
		{
			"isin": "US46090E1038,US78462F1030",
		},
		{
			"cusip": "46090E103,78462F103",
		},
		{
			"spec":     "all",
			"sec_type": "EF",
		},
		{
			"spec":     "all",
			"sec_type": "EN",
		},
		{
			"spec":     "all",
			"sec_type": "SSEF",
		},
		{
			"spec":       "all",
			"legal_type": "PTP",
		},
		{
			"spec":       "all",
			"legal_type": "GNR",
		},
	}

	for _, caseMap := range cases {
		q.Del("spec")
		q.Del("sec_type")
		q.Del("legal_type")
		q.Del("isin")
		q.Del("cusip")
		for k, v := range caseMap {
			q.Add(k, v)
		}
		req.URL.RawQuery = q.Encode()
		BasicTest(t, w, req)
	}
}

func TestGetEtfDetails(t *testing.T) {
	testPath := "etf_details"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()
	q := req.URL.Query()

	cases := []map[string]string{
		{
			"spec": "all",
		},
		{
			"spec": "QQQ,SPY",
		},
		{
			"isin": "US46090E1038,US78462F1030",
		},
		{
			"cusip": "46090E103,78462F103",
		},
	}

	for _, caseMap := range cases {
		q.Del("spec")
		q.Del("isin")
		q.Del("cusip")
		for k, v := range caseMap {
			q.Add(k, v)
		}
		req.URL.RawQuery = q.Encode()
		BasicTest(t, w, req)
	}
}

func TestGetEtfRanks(t *testing.T) {
	cases := map[string][]map[string]string{
		"profit": {
			{
				"range":    "1w",
				"domicile": "us",
			},
			{
				"range":    "1m",
				"domicile": "us",
			},
			{
				"range":    "3m",
				"domicile": "us",
			},
			{
				"range":    "6m",
				"domicile": "us",
			},
			{
				"range":    "1y",
				"domicile": "us",
			},
			{
				"range":    "5y",
				"domicile": "us",
			},
		},
		"loss": {
			{
				"range":    "1w",
				"domicile": "us",
			},
			{
				"range":    "1m",
				"domicile": "us",
			},
			{
				"range":    "3m",
				"domicile": "us",
			},
			{
				"range":    "6m",
				"domicile": "us",
			},
			{
				"range":    "1y",
				"domicile": "us",
			},
			{
				"range":    "5y",
				"domicile": "us",
			},
		},
		"volume": {
			{
				"range":    "1d",
				"domicile": "us",
			},
			{
				"range":    "10d",
				"domicile": "us",
			},
			{
				"range":    "1m",
				"domicile": "us",
			},
		},
		"price": {
			{
				"order":    "asc",
				"domicile": "us",
			},
			{
				"order":    "desc",
				"domicile": "us",
			},
		},
		"marketcap": {
			{
				"domicile": "us",
			},
			{
				"domicile": "us",
			},
		},
	}

	for rankType, caseMapList := range cases {
		testPath := fmt.Sprintf("%s/symbols/ranks/%s", BasePath, rankType)
		req, _ := http.NewRequest("GET", testPath, nil)
		req.Header.Add("x-api-key", TestAPIKey)
		w := httptest.NewRecorder()
		q := req.URL.Query()
		for _, item := range caseMapList {
			q.Del("order")
			q.Del("range")
			q.Del("domicile")
			for k, v := range item {
				q.Add(k, v)
			}
			req.URL.RawQuery = q.Encode()
			BasicTest(t, w, req)
		}
	}
}

func TestGetSSEF(t *testing.T) {
	testPath := "symbols/ssef"
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/%s", BasePath, testPath), nil)
	req.Header.Add("x-api-key", TestAPIKey)
	w := httptest.NewRecorder()

	BasicTest(t, w, req)
}
